function hideElement(mobileCurrentAddress, currentAddress, currentZipCode)
{
	document.getElementById(mobileCurrentAddress).style.display = 'none'; 
	document.getElementById(currentAddress).style.display = 'none';
	document.getElementById(currentZipCode).style.display = 'none';
}

function displayElement(mobileCurrentAddress, currentAddress, currentZipCode)
{
	document.getElementById(mobileCurrentAddress).style.display = 'block';
	document.getElementById(currentAddress).style.display = 'block';
	document.getElementById(currentZipCode).style.display = 'block';	
}

function hideAdmissionNotice(admissionNotice)
{
	document.getElementById(admissionNotice).style.display = 'none';
}

function displayAdmissionNotice(admissionNotice)
{
	document.getElementById(admissionNotice).style.display = 'block';
}

function copyPostalAddress(homeAddress, cityProvince, country, zipCode, phoneNumber)
{
	document.getElementById("mobile_admission_notice").value = document.getElementById(phoneNumber).value;
	document.getElementById("city_admission_notice").value = document.getElementById(cityProvince).value;
	document.getElementById("receiver_country").options[document.getElementById(country).options.selectedIndex].selected = true;
	document.getElementById("reciver_address").value = document.getElementById(homeAddress).value;
	document.getElementById("reciver_zip_code").value = document.getElementById(zipCode).value;

}

function copyCurrentAddress(phoneNumber, address ,zipCode)
{
	document.getElementById("mobile_admission_notice").value = document.getElementById(phoneNumber).value;
	document.getElementById("city_admission_notice").value = "";
	document.getElementById("receiver_country").options[0].selected = true;
	document.getElementById("reciver_address").value = document.getElementById(address).value;
	document.getElementById("reciver_zip_code").value = document.getElementById(zipCode).value;
}