

function start(status, id)
{
	checkStatusForNewApplication(status,id);
	checkStatusForBasicInfo(status, id);
	checkStatusForStudyPlan(status, id);
	checkStatusForEducation(status, id);
	checkStatusForAdditionalInfo(status, id);
	checkStatusForContactInfo(status, id);
	checkStatusForPreview(status, id);

}

function addLink( application_id, url, buttonId, formId)
{
	var link = document.getElementById(formId);
	var sideButton = document.getElementById(buttonId);
	var tickMark = document.createElement('span');
	tickMark.setAttribute("class", "glyphicon glyphicon-ok");
	tickMark.setAttribute("aria-hidden", "true");
	link.setAttribute("action", link.getAttribute("action")+url+application_id);
	sideButton.appendChild(tickMark);
}

function checkStatusForNewApplication(checkStatus,id)
{
	if(checkStatus == 0)
	{

		form = document.getElementById("basicInfoCol");
		form.style.backgroundColor = '#172D44';

	}
}

function checkStatusForBasicInfo(checkStatus,id)
{

	if(checkStatus == 1)
	{
		addLink(id, "ApplicationProcess/editApplicationStep5/", "submitStep1", "basicInfo");
		form = document.getElementById("basicInfoCol");
		form.style.backgroundColor = '#172D44';

		form2 = document.getElementById("studyPlanCol");
		form2.style.backgroundColor = '#172D44';

	}


}

function checkStatusForStudyPlan(checkStatus, id)
{
	if(checkStatus == 2)
	{
		addLink(id, "ApplicationProcess/editApplicationStep5/", "submitStep1", "basicInfo");
		form = document.getElementById("basicInfoCol");
		form.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep6/", "submitStep2", "studyPlan");
		form2 = document.getElementById("studyPlanCol");
		form2.style.backgroundColor = '#172D44';
		form3 = document.getElementById("educationCol");
		form3.style.backgroundColor = '#172D44';
	}


}



function checkStatusForEducation(checkStatus, id)
{
	if(checkStatus == 3)
	{
		addLink(id, "ApplicationProcess/editApplicationStep5/", "submitStep1", "basicInfo");
		form = document.getElementById("basicInfoCol");
		form.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep6/", "submitStep2", "studyPlan");
		form2 = document.getElementById("studyPlanCol");
		form2.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep7/", "submitStep3", "education");
		form3 = document.getElementById("educationCol");
		form3.style.backgroundColor = '#172D44';
		form4 = document.getElementById("additionalInfoCol");
		form4.style.backgroundColor = '#172D44';
				alert("wtf");

	}

}

function checkStatusForAdditionalInfo(checkStatus, id)
{
	if(checkStatus == 4)
	{
		addLink(id, "ApplicationProcess/editApplicationStep5/", "submitStep1", "basicInfo");
		form = document.getElementById("basicInfoCol");
		form.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep6/", "submitStep2", "studyPlan");
		form2 = document.getElementById("studyPlanCol");
		form2.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep7/", "submitStep3", "education");
		form3 = document.getElementById("educationCol");
		form3.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep8/", "submitStep4", "additionalInfo");
		form4 = document.getElementById("additionalInfoCol");
		form4.style.backgroundColor = '#172D44';
		form5 = document.getElementById("contactInfoCol");
		form5.style.backgroundColor = '#172D44';
		


	}

}

function checkStatusForContactInfo(checkStatus, id)
{

	if(checkStatus == 5)
	{
		addLink(id, "ApplicationProcess/editApplicationStep5/", "submitStep1", "basicInfo");
		form = document.getElementById("basicInfoCol");
		form.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep6/", "submitStep2", "studyPlan");
		form2 = document.getElementById("studyPlanCol");
		form2.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep7/", "submitStep3", "education");
		form3 = document.getElementById("educationCol");
		form3.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep8/", "submitStep4", "additionalInfo");
		form4 = document.getElementById("additionalInfoCol");
		form4.style.backgroundColor = '#172D44';
		addLink(id, "ApplicationProcess/editApplicationStep9/", "submitStep5", "contactInfo");
		form5 = document.getElementById("contactInfoCol");
		form5.style.backgroundColor = '#172D44';
		form6 = document.getElementById("previewCol");
		form6.style.backgroundColor = '#172D44';

		
	}

}

function checkStatusForPreview(checkStatus, id)
{
	if(checkStatus == 6)
	{
		addLink(id, "ApplicationProcess/editApplicationStep5/", "submitStep1", "basicInfo");
		addLink(id, "ApplicationProcess/editApplicationStep6/", "submitStep2", "studyPlan");
		addLink(id, "ApplicationProcess/editApplicationStep7/", "submitStep3", "education");
		addLink(id, "ApplicationProcess/editApplicationStep8/", "submitStep4", "additionalInfo");
		addLink(id, "ApplicationProcess/editApplicationStep9/", "submitStep5", "contactInfo");
		addLink(id, "ApplicationProcess/applicationStep10/", "submitStep6", "preview");

	}

}

function isFileRequired(filename, inputId)
{

	if(filename == "" || filename == null)
	{

		document.getElementById(inputId).setAttribute("required", "true");

	}

}
