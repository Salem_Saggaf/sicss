var countFamily = 1;
var countEducation = 1;
function addFamilyStatus(){
	var tbody = document.getElementById("family_status");
	var row = document.createElement("tr");
	row.id = "fmaily"+ countFamily;
	var col1 = document.createElement("th");
	setAttributes(col1, {"scope": "row"});
	var col2 = document.createElement("th");
	var col3 = document.createElement("th");
	var col4 = document.createElement("th");
	var col5 = document.createElement("th");
	var col6 = document.createElement("th");
	var col7 = document.createElement("th");
	var familySelect = document.createElement("select");
	addOption(familySelect, ["Father", "Mother", "Spouse", "Uncle", "Brother", "Sister", "Other", "Children"]);
	setAttributes(familySelect, {"class" : "form-control", "name": "family_member"+countFamily});
	var familyName = document.createElement("input");
	setAttributes(familyName, {"class": "form-control", "type": "text", "name": "family_name"+countFamily});
	var phoneNumber = document.createElement("input");
	setAttributes(phoneNumber, {"class": "form-control", "type": "text", "name": "family_phone_number"+countFamily});
	var email = document.createElement("input");
	setAttributes(email, {"class": "form-control", "type": "text", "name": "family_email"+countFamily});
	var position = document.createElement("input");
	setAttributes(position, {"class": "form-control", "type": "text", "name": "family_posistion"+countFamily});
	var workPlace = document.createElement("input");
	setAttributes(workPlace, {"class": "form-control", "type": "text", "name": "family_work_place"+countFamily});
	var deleteButton = document.createElement("button");
	setAttributes(deleteButton, {"class": "btn btn-danger"});
	deleteButton.setAttribute("onclick" , "deleteRow("+row.getAttribute("id")+")");
	deleteButton.innerHTML = "Delete";
	col1.appendChild(familySelect);
	col2.appendChild(familyName);
	col3.appendChild(phoneNumber);
	col4.appendChild(email);
	col5.appendChild(position);
	col6.appendChild(workPlace);
	col7.appendChild(deleteButton);
	row.appendChild(col1);
	row.appendChild(col2);
	row.appendChild(col3);
	row.appendChild(col4);
	row.appendChild(col5);
	row.appendChild(col6);
	row.appendChild(col7);
	tbody.appendChild(row);
	countFamily++;
}	

function addEducationlBackground()
{
	var tbody = document.getElementById("educationl_background");
	var row = document.createElement("tr");
	row.id = "education"+ countEducation;
	var col1 = document.createElement("th");
	setAttributes(col1, {"scope": "row"});
	var col2 = document.createElement("th");
	var col3 = document.createElement("th");
	var col4 = document.createElement("th");
	var col5 = document.createElement("th");
	var yearAttendedFrom = document.createElement("input");
	setAttributes(yearAttendedFrom, {"class": "form-control", "type" : "date", "name" : "education_year_attended_from"+countEducation});
	var yearAttendedTo = document.createElement("input");
	setAttributes(yearAttendedTo, {"class": "form-control", "type" : "date", "name" : "education_year_attended_to"+countEducation});
	var schoolName = document.createElement("input");
	setAttributes(schoolName, {"class": "form-control", "type": "text",  "name" : "education_school_name"+countEducation});
	var fieldStudy = document.createElement("input");
	setAttributes(fieldStudy, {"class": "form-control", "type": "text",  "name" : "education_field_study"+countEducation});
	var deleteButton = document.createElement("button");
	setAttributes(deleteButton, {"class": "btn btn-danger"});
	deleteButton.setAttribute("onclick", "deleteRow("+row.getAttribute("id")+")");
	deleteButton.innerHTML = "Delete";
	col1.appendChild(yearAttendedFrom);
	col2.appendChild(yearAttendedTo);
	col3.appendChild(schoolName);
	col4.appendChild(fieldStudy);
	col5.appendChild(deleteButton);
	row.appendChild(col1);
	row.appendChild(col2);
	row.appendChild(col3);
	row.appendChild(col4);
	row.appendChild(col5);
	tbody.appendChild(row);
	countEducation++;
}

function deleteRow(row){
    var element = document.getElementById(row.id);
    element.parentNode.removeChild(element);
	
}

function setAttributes(el, attrs) {
  for(var key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
}

function addOption (select, options){
	for(var i =0; i<options.length; i++){
		var option = document.createElement("option");
		option.innerHTML = options[i];
		option.value = options[i];
		select.appendChild(option);
	}
}