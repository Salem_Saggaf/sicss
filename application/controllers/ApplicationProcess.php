<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApplicationProcess extends CI_Controller {
	public function index(){
		$this->load->model('public/Application_model');
		$data['application'] = $this->Application_model->getAllApplication();
		$this->load->view('public/application-query', $data);
	}

	public function compress_image($source_url, $destination_url) {

   	 	$quality = 30;

		$info = getimagesize($source_url);

    		if ($info['mime'] == 'image/jpeg')
        			$image = imagecreatefromjpeg($source_url);

    		elseif ($info['mime'] == 'image/gif')
        			$image = imagecreatefromgif($source_url);

   		elseif ($info['mime'] == 'image/png')
        			$image = imagecreatefrompng($source_url);

    imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}



	public function applicationStep1(){
		$this->load->view("public/application");	
	}


	public function applicationStep2()
	{
		$this->load->library('session');
		if($this->input->post("program") != null)
		{
			
			$_SESSION["program"] = $this->input->post("program");
			$this->load->view('public/application-step-1');
		}
	}

	public function applicationStep3()
	{
		
		if($this->input->post())
		{
			$this->load->model('public/Application_model');
			$type = $this->input->post("type");
			$data['study_list'] = $this->Application_model->getDistinctUniversityList();
			$_SESSION["type"] = $type;
			$data["type"] = $this->Application_model->study_plan($type);
			$this->load->view('public/application-step-2' ,$data);
		}
	}

	public function searchApplication()
	{
		$this->load->model('public/Application_model');
		$department = $this->input->post('department');
		$major = $this->input->post('major');
		$university = $this->input->post('university');
		$language = $this->input->post('language');
		$data['study_list'] = $this->Application_model->getDistinctUniversityList();
		$data['type'] = $this->Application_model->getUniversity($department, $major, $university, $language);
		$this->load->view('public/application-step-2', $data);
	}

	public function applicationStep4()
	{
		$this->load->library('session');
		$this->load->model('public/Application_model');

		if($this->input->post() != null)
		{
			$study_plan_name = $this->input->post("study_plan_name");
			$university_name = $this->input->post("university_name");
			$department = $this->input->post("department");
			$major = $this->input->post("major");
			$duration_from = $this->input->post("duration_from");
			$duration_to = $this->input->post("duration_to");
			$years = $this->input->post("years");
			$teaching_language = $this->input->post("teaching_language");
			$apply_deadline = $this->input->post("apply_deadline");
			$notes = $this->input->post("notes");
			$user = $this->session->userdata('user');
			
			$result = $this->Application_model->insert_new_application($study_plan_name,$university_name, $department, $major, $duration_from, $duration_to, $years, $teaching_language, $apply_deadline, $notes, $user[0]['user_id']);
			if($result == false)
			{
				$this->load->model('public/Application_model');
				$data["message"] = "You can not apply to the same college";		 
				$data["type"] = $this->Application_model->study_plan($_SESSION["type"]);
				$this->load->view('public/application-step-2', $data);
			}
			else
				redirect('ApplicationProcess/editApplicationStep5/'.$result);
		}

	}


	public function editApplicationStep5($id)
	{
		$this->load->model('public/Application_model');
		
		$data['application'] = $this->Application_model->viewApplication($id);
		$this->load->view('public/application-step-3', $data);
	}

	public function applicationStep5()
	{
		$this->load->model('public/Application_model');
		$image = "";
		if($_FILES['image']['name'] != "" || $_FILES['image']['name'] != null)
		{
			$image = basename($_FILES['image']['name']);
			$targetfolder = 'assets/sicss/images/';
	      	$targetfolder_image = $targetfolder . basename( $_FILES['image']['name']) ;
	      	$filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
	      	move_uploaded_file($filename, $targetfolder_image);
      	}

		if ($this->input->post()){
			$family_name = $this->input->post("family_name");
			$given_name = $this->input->post("given_name");
			$chinese_name = $this->input->post("chinese_name");
			$nationality = $this->input->post("nationality");
			$marital_status = $this->input->post("marital_status");
			$gender = $this->input->post("gender");
			$birth_date = $this->input->post("birth_date");
			$birth_place = $this->input->post("birth_place");
			$native_language = $this->input->post("native_language");
			$education_level = $this->input->post("education_level");
			$religion = $this->input->post("religion");
			$institution_affilliated = $this->input->post("institution_affilliated");
			$occupation = $this->input->post("occupation");
			$health_status = $this->input->post("health_status");
			$emigrant = $this->input->post("emigrant");
			$hobby = $this->input->post("hobby");
			$passport_no = $this->input->post("passport_no");
			$passport_expiry_date = $this->input->post("passport_expiry_date");
			$user = $this->session->userdata('user');
			$status = $this->input->post("status");
			$result = $this->Application_model->insert_basic_info($image, $family_name, $given_name, $chinese_name, $nationality, 
					$marital_status, 
					$gender, $birth_date, $birth_place, $native_language, $education_level, $religion, $institution_affilliated, 
					$occupation, 
					$health_status, $emigrant, $hobby, $passport_no, $passport_expiry_date, $user[0]['user_id'], $status);
			if($result == 1)
				redirect('ApplicationProcess/editApplicationStep6/'.$this->input->post('id'));
		}
	}

	public function editApplicationStep6($id)
	{
		$this->load->model('public/Application_model');
		
		$data['application'] = $this->Application_model->viewApplication($id);
		$this->load->view('public/application-step-4', $data);
	}

	public function applicationStep6()
	{
		$this->load->model('public/Application_model');
		if($this->input->post())
		{
			$language_proficiency = $this->input->post("language_proficiency");
			$toefl = $this->input->post("toefl");
			$gre = $this->input->post("gre");
			$gmat = $this->input->post("gmat");
			$ielts = $this->input->post("ielts");
			$other_language_proficiency = $this->input->post("other_language_proficiency");

			$adjacments = $this->input->post("adjacments");
			$alt_college = $this->input->post("alt_college");
			$alt_major = $this->input->post("alt_major");
			$alt_teaching_laguage = $this->input->post("alt_teaching_laguage");
			$alt_study_years = $this->input->post("alt_study_years");
			$rec_name = $this->input->post("rec_name");
			$rec_relation = $this->input->post("rec_relation");
			$rec_orgnaization = $this->input->post("rec_orgnaization");
			$rec_mobile = $this->input->post("rec_mobile");
			$rec_phone = $this->input->post("rec_phone");
			$rec_email = $this->input->post("rec_email");
			$rec_nationality = $this->input->post("rec_nationality");
			$job = $this->input->post("job");
			$rec_address = $this->input->post("rec_address");
			$fax_number = $this->input->post("fax_number");
			$status = $this->input->post("status");

			$result = $this->Application_model->insert_study_plan($language_proficiency, $toefl, $gre, $gmat, $ielts, $other_language_proficiency, $adjacments, $alt_college, $alt_major, $alt_teaching_laguage, $alt_study_years, $rec_name, $rec_relation, $rec_orgnaization, $rec_mobile, $rec_phone, $rec_email, $rec_nationality, $job, $rec_address, $fax_number, $status);
			if($result == 1)
				redirect('ApplicationProcess/editApplicationStep7/'.$this->input->post('id'));

		}
	}

	public function editApplicationStep7($id)
	{
		$this->load->model('public/Application_model');
		
		$data['application'] = $this->Application_model->viewApplication($id);
		$data['educational_background'] = $this->Application_model->viewEducationalBackground($id);
		$this->load->view('public/application-step-5', $data);
	}

	public function applicationStep7()
	{
		$this->load->model('public/Application_model');

		if($this->input->post())
		{
			$i = 1;
			$edu_background =array();
			$result = 0;
			$passport = "";
			$high_school = "";
			$english_test = "";
			$physical_test = "";
			if($_FILES['passport_copy']['name'] != null || $_FILES['high_school_certified']['name'] != null 
				|| $_FILES['english_test']['name'] != null || $_FILES['physical_test']['name'] != null)
			{
				$passport = basename($_FILES['passport_copy']['name']);
				$high_school = basename($_FILES['high_school_certified']['name']);
				$english_test = basename($_FILES['english_test']['name']);
				$physical_test = basename($_FILES['physical_test']['name']);
				$targetfolder = 'assets/sicss/images/';
	      		$targetfolder_passport = $targetfolder . basename( $_FILES['passport_copy']['name']) ;
	      		$targetfolder_high_school = $targetfolder . basename( $_FILES['high_school_certified']['name']) ;
	      		$targetfolder_english_test = $targetfolder . basename( $_FILES['english_test']['name']) ;
	      		$targetfolder_physical_test = $targetfolder . basename( $_FILES['physical_test']['name']) ;
	    	  	$filename = $this->compress_image($_FILES["passport_copy"]["tmp_name"], $targetfolder_passport);
	    	  	$filename2 = $this->compress_image($_FILES["high_school_certified"]["tmp_name"], $targetfolder_high_school);
	    	  	$filename3 = $this->compress_image($_FILES["english_test"]["tmp_name"], $targetfolder_english_test);
	    	  	$filename4 = $this->compress_image($_FILES["physical_test"]["tmp_name"], $targetfolder_physical_test);
		      	move_uploaded_file($filename, $targetfolder_passport);
		      	move_uploaded_file($filename2, $targetfolder_high_school);
		      	move_uploaded_file($filename3, $targetfolder_english_test);
		      	move_uploaded_file($filename4, $targetfolder_physical_test);
		      	
		   		

	      	}
	      	$status = $this->input->post("status");
	      	$result = $this->Application_model->insert_educational_background($passport, $high_school, $english_test, $physical_test, $status);
			foreach ($_POST as $key => $val) { 

					array_push($edu_background, $val);
					if($i >=5)
					{
						$this->Application_model->insert_educational_background_other($edu_background[0], $edu_background[1], $edu_background[2], $edu_background[3], $edu_background[4], $this->input->post('id'));
						$edu_background = array();
						$i = 0;
					}
					$i++;					


			}

			
				redirect('ApplicationProcess/editApplicationStep8/'.$this->input->post('id'));
		}
	}

	public function editApplicationStep8($id)
	{
		$this->load->model('public/Application_model');
		$data['application'] = $this->Application_model->viewApplication($id);
		$data['family_member'] = $this->Application_model->viewFamilyMember($id);
		$this->load->view('public/application-step-6', $data);
	}

	public function applicationStep8()
	{
		$this->load->model('public/Application_model');

		if($this->input->post())
		{
			$i = 1;
			$family_member =array();

			$guarantor_name = $this->input->post("guarantor_name");
			$guarantor_tel = $this->input->post("guarantor_tel");
			$guarantor_address = $this->input->post("guarantor_address");
			$guarantor_org = $this->input->post("organization");
			$guarantor_email = $this->input->post("gur_email");
			$em_name = $this->input->post("emergency_name");
			$em_mobile = $this->input->post("emergency_mobile");
			$em_phone_number = $this->input->post("emergency_phone_number");
			$em_email = $this->input->post("emergency_email");
			$em_org = $this->input->post("emergency_organization");
			$em_address = $this->input->post("emergency_address");
			$status = $this->input->post("status");

			$result = $this->Application_model->insert_addtional_info($guarantor_name, $guarantor_tel, $guarantor_address, $guarantor_org, $guarantor_email, $em_name, $em_mobile, $em_phone_number, $em_email, $em_org, $em_address, $status);
			foreach ($this->input->post() as $key => $val) { 
				if(strcmp(substr($key, 0, 5), "family")==0)
				{
					array_push($family_member, $val);
					if($i >=6)
					{
						$this->Application_model->insert_family_member($family_member[0], $family_member[1], $family_member[2], $family_member[3], $family_member[4], $family_member[5]);
						$family_member = array();
						$i = 1;
					}
					$i++;					
				}

			}

			if($result == 1)
				redirect('ApplicationProcess/editApplicationStep9/'.$this->input->post('id'));
		}
	}

	public function editApplicationStep9($id)
	{
		$this->load->model('public/Application_model');
		
		$data['application'] = $this->Application_model->viewApplication($id);
		$this->load->view('public/application-step-7', $data);
	}

	public function applicationStep9()
	{

		$this->load->model('public/Application_model');
		if ($this->input->post()){
			$adress_home_address = $this->input->post("adress_home_address");
			$city_province = $this->input->post("city_province");
			$country_home_address = $this->input->post("country_home_address");
			$phone_number_home_address = $this->input->post("phone_number_home_address");
			$mobile_home_address = $this->input->post("mobile_home_address");
			$zip_code_home_address = $this->input->post("zip_code_home_address");
			$same_country_home = $this->input->post("same_country_home");
			$email_current_address = $this->input->post("email_current_address");
			$mobile_current_address_input = $this->input->post("mobile_current_address_input");
			$address_current_address_input = $this->input->post("address_current_address_input");
			$zip_code_current_address_input = $this->input->post("zip_code_current_address_input");
			$radio_admission_notice = $this->input->post("radio_admission_notice");
			$reciver_name = $this->input->post("reciver_name");
			$mobile_admission_notice = $this->input->post("mobile_admission_notice");
			$city_admission_notice = $this->input->post("city_admission_notice");
			$receiver_country = $this->input->post("receiver_country");
			$reciver_address = $this->input->post("reciver_address");
			$reciver_zip_code = $this->input->post("reciver_zip_code");
			$user = $this->session->userdata('user');
			$status = $this->input->post("status");
			$applicationStatus = $this->input->post('application_status');
			$result = $this->Application_model->insert_contact_info($adress_home_address, $city_province, $country_home_address, $phone_number_home_address, $mobile_home_address, 
					$zip_code_home_address, 
					$same_country_home, $email_current_address, $mobile_current_address_input, $address_current_address_input, $zip_code_current_address_input, $radio_admission_notice, $reciver_name, 
					$mobile_admission_notice, 
					$city_admission_notice, $receiver_country, $reciver_address, $reciver_zip_code, $status);
			if($result == 1)
				redirect('ApplicationProcess/applicationStep10/'.$this->input->post('id'));
		}
	}

	public function applicationStep10($id)
	{
		$this->load->model('public/Application_model');
		$data['application'] = $this->Application_model->viewApplication($id);
		$data['educational_background'] = $this->Application_model->viewEducationalBackground($id);
		$data['family_member'] = $this->Application_model->viewFamilyMember($id);
		$this->load->view('public/application-step-8', $data);
	}

	public function viewApplicationById()
	{
		$this->load->model('public/Application_model');
		$id = $this->input->post('id');
		$data['application'] = $this->Application_model->getApplicationById($id);
		$data['educational_background'] = $this->Application_model->viewEducationalBackground($this->Application_model->getApplicationById($id)[0]->application_id);
		$data['family_member'] = $this->Application_model->viewFamilyMember($this->Application_model->getApplicationById($id)[0]->application_id);
		$this->load->view('public/application-step-8', $data);
			
	}

	public function editApplication()
	{
		$this->load->model('public/Application_model');
		 $id = $this->input->post('id');
		switch($this->Application_model->getUserApplicationStatus($id)[0]->status)
		{
			case 0 :
			{
				$this->load->model('public/Application_model');
				
				$data['application'] = $this->Application_model->getApplicationById($id);
				redirect('ApplicationProcess/editApplicationStep5/'.$data['application'][0]->application_id, $data);
				break;				
			}
			case 1 : 
			{
				$this->load->model('public/Application_model');
				
				$data['application'] = $this->Application_model->getApplicationById($id);
				redirect('ApplicationProcess/editApplicationStep5/'.$data['application'][0]->application_id, $data);
				break;
			}
				
			case 2 :
				$this->load->model('public/Application_model');
				
				$data['application'] = $this->Application_model->getApplicationById($id);
				redirect('ApplicationProcess/editApplicationStep6/'.$data['application'][0]->application_id, $data);
				break;
			case 3 :
				$this->load->model('public/Application_model');
				
				$data['application'] = $this->Application_model->getApplicationById($id);
				redirect('ApplicationProcess/editApplicationStep7/'.$data['application'][0]->application_id, $data);
				break;
			case 4 :
				$this->load->model('public/Application_model');
				
				$data['application'] = $this->Application_model->getApplicationById($id);
				redirect('ApplicationProcess/editApplicationStep8/'.$data['application'][0]->application_id, $data);
				break;

			case 5 :
				$this->load->model('public/Application_model');
				
				$data['application'] = $this->Application_model->getApplicationById($id);
				redirect('ApplicationProcess/editApplicationStep9/'.$data['application'][0]->application_id, $data);
				break;
		
		}
	}

	public function deleteApplication()
	{
		$this->load->model('public/Application_model');
		$id = $this->input->post('id');
		$result = $this->Application_model->deleteApplication($id);
		if($result == 1)
			redirect('ApplicationProcess','refresh');
	}

}

