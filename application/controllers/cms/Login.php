<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Login extends CI_Controller
  {

    public function index()
    {
      $this->load->view('cms/login');
    }

    public function signup()
    {
      $email    = $this->input->post('email');
      $password = $this->input->post('password');
      $username = $this->input->post('username');
      $password = md5($password);
      $token = sha1(mt_rand(1, 90000) . 'SALT');

      $this->load->model('cms/home_model');
      $is_exist = $this->home_model->is_exist($email);

      $message['error'] = 'false';

      if ($is_exist == 'true') {
        $message['error'] = 'true';
        $this->load->view('cms/login',$message);
      }else {
        $result           = $this->home_model->signup($email, $password, $username, $token);

        $directoryName = './users/uploads/applications/'.$email;

        //Check if the directory already exists.
        if(!is_dir($directoryName)){
            //Directory does not exist, so lets create it.
            mkdir($directoryName, 0755, true);
        }

        // $email_body  = '<html>';
        // $email_body .= '<body>';
        // $email_body = 'Dear '.$username.' Please click this link '.base_url().'cms/login/new_signup_account_confirmation?token='.$token.'&email='.$email.' to activate your account';
        // $email_body .= 'Dear '.$username.' Please click this <a href = "'.base_url().'cms/login/reset_current_password?token='.$token.'"> link </a> to activate your account';
        // $email_body .= '</body>';
        // $email_body .= '</html>';

        $message['user']  = $this->home_model->user_data($email);
        $this->session->set_userdata($message['user']);

        // $this->load->library('email');
        //
        // $this->email->from('noreply@mesamall.pavertech.my', 'mesamall.pavertech.my');
        // $this->email->to($email);
        //
        // $this->email->subject('Welcome to mesamall.pavertech.my');
        // $this->email->message($email_body);
        //
        // $this->email->send();

        $this->load->view('public/login',$message);
      }
    }

    public function logindata()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata != NULL) {
        redirect('Home','refresh');
      }

      $email    = $this->input->post('email');
      $password = $this->input->post('password');
      $password = md5($password);

      $this->load->model('cms/home_model');
      $is_exist = $this->home_model->login($email, $password);

      $message['login'] = 'true';

      if ($is_exist == 'true') {
        $user_data = $this->home_model->user($email, $password);
        $this->session->set_userdata('user',$user_data);
        $userdata = $this->session->userdata('user');



        $this->load->view('cms/index');


      }else {
        $message['login'] = 'false';

        $this->load->view('cms/login',$message);
      }
    }

    public function change_password()
    {
      $this->load->view('cms/change_password');
    }

    public function update_password()
    {
      $userdata = $this->session->userdata('user');

      $new_password = $this->input->post('password');
      $old_password = $this->input->post('old-password');

      $new_password = md5($new_password);
      $old_password = md5($old_password);

      $this->load->model('cms/home_model');
      $result = $this->home_model->update_password($userdata[0]['email'], $old_password, $new_password);
      if ($result == 1) {
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect('cms/home','refresh');
      }
    }

    public function reset_password_email()
    {
      $this->load->view('cms/reset-password');
    }

    public function reset_password()
    {
      if ($this->input->post()) {
        $email = $this->input->post('email');

        $this->load->model('cms/home_model');
        $result['email'] = $this->home_model->reset_password_check_email($email);
        $result['result'] = 'success';

        if ($result['email'] == NULL) {
          $result['result'] = 'negative';
          $this->load->view('cms/reset-password', $result);
        }else {

          // $email_body  = '<html>';
          // $email_body .= '<body>';
          // $email_body .= 'dear '.$result['email'][0]['username'].' Please click this <a href = "'.base_url().'cms/login/reset_current_password?token='.$result['email'][0]['key_update'].'"> link </a> to activate your account';
          $email_body = 'dear '.$result['email'][0]['username'].' Please click this link to update new password '.base_url().'cms/login/reset_current_password?token='.$result['email'][0]['key_update'].'&id='.$result['email'][0]['user_id'].'';
          // $email_body .= '</body>';
          // $email_body .= '</html>';

          $this->load->library('email');

          $this->email->from('gostudy@gostudy.my', 'GoStudy.My');
          $this->email->to($result['email'][0]['email']);
          // $this->email->cc('mafadotaiu@gmail.com');
          // $this->email->cc('another@another-example.com');
          // $this->email->bcc('them@their-example.com');

          $this->email->subject('Reset Password');
          $this->email->message($email_body);

          $this->email->send();

          $this->load->view('cms/reset-password', $result);

         }
      }
    }


    public function reset_current_password()
    {
      if (isset($_GET['token'])) {
        $token = $_GET['token'];
        $id    = $_GET['id'];

        $this->load->model('cms/home_model');
        $key_status['userdata'] = $this->home_model->key_status($id, $token);

        $this->load->view('cms/reset-password-email',$key_status);

       }
    }

    public function update_new_password()
    {

      if ($this->input->post()) {
        $email        = $this->input->post('email');
        $new_password = $this->input->post('password');

        $new_password = md5($new_password);

        $this->load->model('cms/home_model');
        $result = $this->home_model->update_new_password($email, $new_password);

         if ($result == 1) {
          redirect('cms/home','refresh');
        }
      }
    }

    public function new_signup_account_confirmation()
    {
      if (isset($_GET['token'])) {
        $token = $_GET['token'];
        $email = $_GET['email'];

        $this->load->model('cms/home_model');
        $key_status = $this->home_model->new_key_status($email, $token);

        if ($key_status[0]['key_status'] == 'Active') {
           $this->home_model->confirmed_account($email);
           $account['status'] = "Activated";
        }else {
          $account['status'] = "Error";
        }

        // print_r($account['status']);

        $this->load->view('cms/welcome',$account);

       }
    }

    public function test2()
    {
      $this->load->view('cms/welcome');
    }


  }

?>
