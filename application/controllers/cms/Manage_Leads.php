<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_Leads extends CI_Controller {

    public function index()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata == null) {
        $this->load->view('cms/login');
      }
      else {
        $this->load->model('cms/manage_leads_model');
        $data['lead'] = $this->manage_leads_model->get_available_leads();
        $data['month'] = $this->manage_leads_model->get_available_months();
        $data['from'] = 0;
        $data['till'] = 0;
        $this->load->view('cms/manage-leads',$data);
      }
    }

    public function search_leads()
    {
      if ($this->input->post()) {
        $date_from = $this->input->post('from')." 00:00:00";
        $date_till = $this->input->post('till')." 00:00:00";

        $this->load->model('cms/manage_leads_model');
        $data['lead'] = $this->manage_leads_model->get_selected_leads($date_from, $date_till);
        $data['month'] = $this->manage_leads_model->get_available_months();
        $data['from'] = $date_from;
        $data['till'] = $date_till;

        $this->load->view('cms/manage-leads',$data);

      }
    }

    public function download_leads()
    {
      if ($this->input->post()) {
        $month = $this->input->post('month');

        $this->load->model('cms/manage_leads_model');
        $query = $this->manage_leads_model->get_selected_month_leads($month);
        $this->generate_excel_file($query);

        // redirect('cms/manage_leads');

      }
    }

    public function download_selected_leads()
    {
      if ($this->input->post()) {
        $this->load->model('cms/manage_leads_model');

        if ($this->input->post('from') == 0) {
          $query = $this->manage_leads_model->get_available_leads();
        }else {
          $date_from = $this->input->post('from')." 00:00:00";
          $date_till = $this->input->post('till')." 00:00:00";
          $query = $this->manage_leads_model->get_selected_leads($date_from, $date_till);
        }

        $this->generate_excel_file($query);

      }
    }

    public function generate_excel_file($query)
    {

        // Starting the PHPExcel library
        $this->load->library('excel');


        $filename = $query[0]->month_name;
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle($query[0]->month);
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', "Leads Month ".$query[0]->month);
        //change the font size
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        $this->excel->getActiveSheet()->mergeCells('A1:E1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // $this->excel->getActiveSheet()->getStyle('A1:E2')->get

        // Field names in the first row
        $titles = ["Title","Name","Email","Contact","Message", "Submitted at"];
        $fields = ["sirName","name","email","phone","content" ,"submitted"];

        // $fields = $query->list_fields();
        $col = 0;
        foreach ($titles as $title)
        {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, $title);
            $col++;
        }
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->setSize(14);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);


        // Fetching the table data
        $row = 3;
        foreach($query as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $filename= $query[0]->month_name.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function delete_lead()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata == null) {
        $this->load->view('cms/login');
      }
      else {
        if ( isset($_GET['id'])){
          $application_id = $_GET['id'];

          $this->load->model('cms/manage_leads_model');
          $result = $this->manage_leads_model->delete_lead($application_id);

          if ($result == 1) {
            redirect('cms/Manage_Leads');
          }
        }
      }
    }

    public function check_status()
    {
       $passport_no     = $this->input->post('passport-no');
       $university_name = $this->input->post('university');


       $this->load->model('cms/application_status_model');
       $data_result['stage'] = $this->application_status_model->check_status($passport_no,$university_name);

       $this->load->view('cms/result',$data_result);
    }





}
