<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_Product extends CI_Controller {

  public function index()
  {
    $userdata = $this->session->userdata('user');

    if ($userdata == null) {
      $this->load->view('cms/login');
    }
    else {
      $this->load->model('cms/product/manage_product_model');
      $data['tag'] = $this->manage_product_model->available_tags();

      $this->load->view('cms/product/manage-product',$data);
    }

  }
}
