<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_Product extends CI_Controller {

  public function compress_image($source_url, $destination_url) {

    $quality = 30;

    $info = getimagesize($source_url);

        if ($info['mime'] == 'image/jpeg')
              $image = imagecreatefromjpeg($source_url);

        elseif ($info['mime'] == 'image/gif')
              $image = imagecreatefromgif($source_url);

      elseif ($info['mime'] == 'image/png')
              $image = imagecreatefrompng($source_url);

        imagejpeg($image, $destination_url, $quality);
    return $destination_url;
  }

  public function index()
  {
    $userdata = $this->session->userdata('user');

    if ($userdata == null) {
      $this->load->view('cms/login');
    }
    else {
      if ( isset($_GET['tag'])){
        $tag = $_GET['tag'];
        $data['tag'] = $tag;

        $this->load->model('cms/product/update_product_model');
        $data['product_banner']  = $this->update_product_model->product_banner($tag);
        $data['product_feature'] = $this->update_product_model->product_feature($tag);
        $data['product_description'] = $this->update_product_model->product_description($tag);
        $data['testimonial'] = $this->update_product_model->product_testimonial($tag);
        $data['product_imagelist'] = $this->update_product_model->product_imagelist($tag);
        $data['titleimage'] = $this->update_product_model->titleimage($tag);

        $this->load->view('cms/product/update-product',$data);

      }else {
        redirect('cms/product/Update_Product');
      }
    }
  }

  public function update_banner()
  {
    if ($this->input->post()) {
      $id                       = $this->input->post('id');
      $tag                      = $this->input->post('tag');
      $title                    = $this->input->post('title');
      $description              = $this->input->post('description');
      $image                    = basename( $_FILES['image']['name']);

      $targetfolder = 'assets/product/img/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

      // $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($_FILES["image"]["tmp_name"], $targetfolder_image);

      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->update_banner($id, $title, $description, $image);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$tag);
      }
    }
  }

  public function update_product_feature()
  {
    if ($this->input->post()) {
      $id                       = $this->input->post('id');
      $tag                      = $this->input->post('tag');
      $title                    = $this->input->post('title');
      $description              = $this->input->post('description');
      $icon                     = $this->input->post('icon');

      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->update_product_feature($id, $title, $description, $icon);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$tag);
      }
    }
  }

  public function update_product_description()
  {
    if ($this->input->post()) {
      $targetfolder = 'assets/product/img/product/';


      $data['id']                        = $this->input->post('id');
      $data['$tag']                      = $this->input->post('tag');
      $data['title']                     = $this->input->post('title');

      $data['title1']                    = $this->input->post('title1');
      $data['image1']                    = basename( $_FILES['image1']['name']);
      $data['description1']              = $this->input->post('description1');
      $data['sec1_keypoint1']            = $this->input->post('sec1-keypoint1');
      $data['sec1_keypoint2']            = $this->input->post('sec1-keypoint2');
      $data['sec1_keypoint3']            = $this->input->post('sec1-keypoint3');
      $data['sec1_icon1']                = $this->input->post('sec1-icon1');
      $data['sec1_icon2']                = $this->input->post('sec1-icon2');
      $data['sec1_icon3']                = $this->input->post('sec1-icon3');

      $data['title2']                    = $this->input->post('title2');
      $data['image2']                    = basename( $_FILES['image2']['name']);
      $data['description2']              = $this->input->post('description2');
      $data['sec2_keypoint1']            = $this->input->post('sec2-keypoint1');
      $data['sec2_keypoint2']            = $this->input->post('sec2-keypoint2');
      $data['sec2_keypoint3']            = $this->input->post('sec2-keypoint3');
      $data['sec2_icon1']                = $this->input->post('sec2-icon1');
      $data['sec2_icon2']                = $this->input->post('sec2-icon2');
      $data['sec2_icon3']                = $this->input->post('sec2-icon3');

      $targetfolder_image1       = $targetfolder . basename( $_FILES['image1']['name']) ;
      // $filename = $this->compress_image($_FILES["image1"]["tmp_name"], $targetfolder_image1);
      move_uploaded_file($_FILES["image1"]["tmp_name"], $targetfolder_image1);

      $targetfolder_image2       = $targetfolder . basename( $_FILES['image2']['name']) ;
      // $filename = $this->compress_image($_FILES["image2"]["tmp_name"], $targetfolder_image2);
      move_uploaded_file($_FILES["image2"]["tmp_name"], $targetfolder_image2);

      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->update_product_description($data);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$data['$tag']);
      }
    }
  }

  public function update_product_testimonial()
  {
    if ($this->input->post()) {
      $id                   = $this->input->post('id');
      $tag                  = $this->input->post('tag');
      $title                = $this->input->post('title');
      $description          = $this->input->post('description');
      $video1               = $this->input->post('video1');
      $video2               = $this->input->post('video2');


      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->update_product_testimonial($id, $title, $description, $video1, $video2);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$tag);
      }
    }
  }

  public function create_product_list()
  {
    if ($this->input->post()) {
      $title                    = $this->input->post('title');
      $tag                      = $this->input->post('tag');
      $image                    = basename( $_FILES['image']['name']);

      $targetfolder = 'assets/product/img/overlays/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

      $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->create_product_list($title, $image, $tag);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$tag);
      }
    }
  }

  public function update_product_list()
  {
    if ($this->input->post()) {
      $id                       = $this->input->post('id');
      $tag                      = $this->input->post('tag');
      $title                    = $this->input->post('title');
      $image                    = basename( $_FILES['image']['name']);

      $targetfolder = 'assets/product/img/overlays/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

      $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->update_product_list($id, $title, $image);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$tag);
      }
    }
  }

  public function delete_product_list()
  {
    $userdata = $this->session->userdata('user');

    if ($userdata == null) {
      $this->load->view('cms/login');
    }
    else {
      if ( isset($_GET['id'])){
        $application_id = $_GET['id'];
        $tag            = $_GET['tag'];

        $this->load->model('cms/product/update_product_model');
        $result = $this->update_product_model->delete_product_list($application_id);

        if ($result == 1) {
          redirect('cms/product/Update_Product/index?tag='.$tag);
        }
      }
    }
  }

  public function update_titleimage()
  {
    if ($this->input->post()) {
      $id                       = $this->input->post('id');
      $tag                      = $this->input->post('tag');
      $title                    = $this->input->post('title');
      $description              = $this->input->post('description');
      if (basename( $_FILES['image']['name']) != NULL) {
        $image                    = basename( $_FILES['image']['name']);

        $targetfolder = 'assets/product/img/';
        $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

        $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
        move_uploaded_file($filename, $targetfolder_image);

      }else {
        $image = "";
      }



      $this->load->model('cms/product/update_product_model');
      $result = $this->update_product_model->update_titleimage($id, $title, $description, $image);

      if ($result == 1) {
        redirect('cms/product/Update_Product/index?tag='.$tag);
      }
    }
  }
}
