<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_Content extends CI_Controller {

  public function compress_image($source_url, $destination_url) {

    $quality = 30;

		$info = getimagesize($source_url);

    		if ($info['mime'] == 'image/jpeg')
        			$image = imagecreatefromjpeg($source_url);

    		elseif ($info['mime'] == 'image/gif')
        			$image = imagecreatefromgif($source_url);

   		elseif ($info['mime'] == 'image/png')
        			$image = imagecreatefrompng($source_url);

    imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}

  public function index()
  {
    $userdata = $this->session->userdata('user');

    if ($userdata == null) {
      $this->load->view('cms/login');
    }
    else {
      $this->load->model('cms/manage_content_model');
    //   $data['product_list'] = $this->manage_content_model->product_list();
  		// $data['quote'] = $this->manage_content_model->quote();
  		// $data['portfolio'] = $this->manage_content_model->portfolio();
  		// $data['testimonial'] = $this->manage_content_model->testimonial();
  		// $data['key_point'] = $this->manage_content_model->key_point();

      $data['banner'] = $this->manage_content_model->banner();
      $data['property1'] = $this->manage_content_model->property1();
      $data['property2'] = $this->manage_content_model->property2();  
       $this->load->view('cms/manage-content',$data);
    }

  }

  public function add_image_banner(){
    if($this->input->post()){
      $id = $this->input->post('id');
      $image = basename($_FILES['Image']['name']);
      $title = $this->input->post('Title');
      $sub = $this->input->post('SubTitle');
      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';

      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->add_image_banner($id,$image,$title,$sub);
      if ($result == 1) {
        redirect('cms/Manage_Content');
      }
    }
  }

  public function update_banner()
  {
    if ($this->input->post()) {
      $id         = $this->input->post('id');
      $image      = basename( $_FILES['Image']['name']);
      $title      = $this->input->post('Title');
      $sub        = $this->input->post('SubTitle');


      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->update_banner($id, $image, $title, $sub);

      if ($result == 1) {
        redirect('cms/Manage_Content');
      }
    }
  }

  public function update_property1()
  {
    if ($this->input->post()) {
      $id         = $this->input->post('id');
      $image      = basename( $_FILES['Image']['name']);



      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->update_property1($id, $image);

      if ($result == 1) {
        redirect('cms/Manage_Content');
      }
    }
  }

  public function update_property2()
  {
    if ($this->input->post()) {
      $id         = $this->input->post('id');
      $image      = basename( $_FILES['Image']['name']);

      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->update_property2($id, $image);

      if ($result == 1) {
        redirect('cms/Manage_Content');
      }
    }
  }

    public function add_property1(){
    if($this->input->post()){
      $id = $this->input->post('id');
      $image = basename($_FILES['Image']['name']);
      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';

      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->add_image_property1($id,$image);
      if ($result == 1) {
        redirect('cms/Manage_Content');
      }
    }
  }

  public function add_property2(){
    if($this->input->post()){
      $id = $this->input->post('id');
      $image = basename($_FILES['Image']['name']);
      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';

      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
      move_uploaded_file($filename, $targetfolder_image);

      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->add_image_property2($id,$image);
      if ($result == 1) {
        redirect('cms/Manage_Content');
      }
    }
  }

  public function delete_banner()
  {
    $userdata = $this->session->userdata();

    if(isset($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->delete_banner($id);
      if($result == 1)
        redirect('cms/Manage_Content');
    }
  }

  public function delete_property1()
  {
    $userdata = $this->session->userdata();

    if(isset($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->delete_property1($id);
      if($result == 1)
        redirect('cms/Manage_Content');
    }
  }

  public function delete_property2()
  {
    $userdata = $this->session->userdata();

    if(isset($_GET['id'])){
      $id = $_GET['id'];
      $this->load->model('cms/manage_content_model');
      $result = $this->manage_content_model->delete_property2($id);
      if($result == 1)
        redirect('cms/Manage_Content');
    }
  }

    public function delete_portfolio()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata == null) {
        $this->load->view('cms/login');
      }
      else {
        if ( isset($_GET['id'])){
          $application_id = $_GET['id'];

          $this->load->model('cms/manage_content_model');
          $result = $this->manage_content_model->delete_portfolio($application_id);

          if ($result == 1) {
            redirect('cms/Manage_Content');
          }
        }
      }
    }

    public function update_key_points()
    {
      if ($this->input->post()) {
        $id                       = $this->input->post('id');
        $icon                     = $this->input->post('icon');
        $description              = $this->input->post('description');
        $position                 = $this->input->post('position');

        $this->load->model('cms/manage_content_model');
        $result = $this->manage_content_model->update_key_points($id, $icon, $description, $position);

        if ($result == 1) {
          redirect('cms/Manage_Content');
        }
      }
    }


    public function create_product_list()
    {
      if ($this->input->post()) {
        $title                    = $this->input->post('title');
        $image                    = basename( $_FILES['image']['name']);

        $targetfolder = 'assets/qingfeng/images/overlays/';
        $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

        $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
        move_uploaded_file($filename, $targetfolder_image);

        $this->load->model('cms/manage_content_model');
        $result = $this->manage_content_model->create_product_list($title, $image);

        if ($result == 1) {
          redirect('cms/Manage_Content');
        }
      }
    }

    public function update_product_list()
    {
      if ($this->input->post()) {
        $id                       = $this->input->post('id');
        $title                    = $this->input->post('title');
        $image                    = basename( $_FILES['image']['name']);

        $targetfolder = 'assets/qingfeng/images/overlays/';
        $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

        $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
        move_uploaded_file($filename, $targetfolder_image);

        $this->load->model('cms/manage_content_model');
        $result = $this->manage_content_model->update_product_list($id, $title, $image);

        if ($result == 1) {
          redirect('cms/Manage_Content');
        }
      }
    }

    public function delete_product_list()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata == null) {
        $this->load->view('cms/login');
      }
      else {
        if ( isset($_GET['id'])){
          $application_id = $_GET['id'];

          $this->load->model('cms/manage_content_model');
          $result = $this->manage_content_model->delete_product_list($application_id);

          if ($result == 1) {
            redirect('cms/Manage_Content');
          }
        }
      }
    }

        public function update_quote()
        {
          if ($this->input->post()) {
            $id                       = $this->input->post('id');
            $name                     = $this->input->post('name');
            $description              = $this->input->post('description');
            $image                    = basename( $_FILES['image']['name']);

            $targetfolder = 'assets/qingfeng/images/overlays/';
            $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

            $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
            move_uploaded_file($filename, $targetfolder_image);

            $this->load->model('cms/manage_content_model');
            $result = $this->manage_content_model->update_quote($id, $name, $description, $image);
            if ($result == 1) {
              redirect('cms/Manage_Content');
            }
          }
        }

        public function update_portfolio()
        {
          if ($this->input->post()) {
            $id                       = $this->input->post('id');
            $description              = $this->input->post('description');
            $tag                      = $this->input->post('tag');
            $image                    = basename( $_FILES['image']['name']);

            $targetfolder = 'assets/qingfeng/images/overlays/';
            $targetfolder_image       = $targetfolder . basename( $_FILES['image']['name']) ;

            $filename = $this->compress_image($_FILES["image"]["tmp_name"], $targetfolder_image);
            move_uploaded_file($filename, $targetfolder_image);

            $this->load->model('cms/manage_content_model');
            $result = $this->manage_content_model->update_portfolio($id, $description, $tag, $image);

            if ($result == 1) {
              redirect('cms/Manage_Content');
            }
          }
        }

        public function update_testimonial()
        {
          if ($this->input->post()) {
            $id                   = $this->input->post('id');
            $title                = $this->input->post('title');
            $description          = $this->input->post('description');
            $video                = $this->input->post('video');


            $this->load->model('cms/manage_content_model');
            $result = $this->manage_content_model->update_testimonial($id, $title, $description, $video);

            if ($result == 1) {
              redirect('cms/Manage_Content');
            }
          }
        }




}
