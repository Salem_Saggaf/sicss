<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download_Report extends CI_Controller {

    public function index()
    {
       $userdata = $this->session->userdata('user');

       if ($userdata == null) {
         $this->load->view('cms/login');
       }
       else {
         $this->load->model('cms/download_report_model');
         $data['report'] = $this->download_report_model->get_available_reports();

         $this->load->view('cms/download-report',$data);
       }
    }

    public function update_download()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata == null) {
        $this->load->view('cms/login');
      }
      else {
        if ( isset($_GET['id'])){
          $application_id = $_GET['id'];

          $this->load->model('cms/download_report_model');
          $result = $this->download_report_model->update_download($application_id);
 
        }

      }
    }
}
