<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Manage_Zeta extends CI_Controller{
		 public function compress_image($source_url, $destination_url) {
	   		$quality = 30;
			$info = getimagesize($source_url);

	    		if ($info['mime'] == 'image/jpeg')
	        			$image = imagecreatefromjpeg($source_url);

	    		elseif ($info['mime'] == 'image/gif')
	        			$image = imagecreatefromgif($source_url);

	   		elseif ($info['mime'] == 'image/png')
	        			$image = imagecreatefrompng($source_url);

	    imagejpeg($image, $destination_url, $quality);
			return $destination_url;
		}

	  public function index()
	  {
	    $userdata = $this->session->userdata('user');

	    if ($userdata == null) {
	      $this->load->view('cms/login');
	    }
	    else {
	      $this->load->model('cms/manage_property_model');
	    //   $data['product_list'] = $this->manage_content_model->product_list();
	  		// $data['quote'] = $this->manage_content_model->quote();
	  		// $data['portfolio'] = $this->manage_content_model->portfolio();
	  		// $data['testimonial'] = $this->manage_content_model->testimonial();
	  		// $data['key_point'] = $this->manage_content_model->key_point();

	      $data['banner'] = $this->manage_property_model->zetaBanner();
	      $data['sectionInfo'] = $this->manage_property_model->zetaSectionInfo();
	      $data['sectionImage'] = $this->manage_property_model->zetaSectionImage();
	        
	       $this->load->view('cms/manage-zeta',$data);
	    }

	  }

	  public function add_image_banner(){
	    if($this->input->post()){
	      $id = $this->input->post('id');
	      $image = basename($_FILES['Image']['name']);
	      $title = $this->input->post('Title');
	      $sub = $this->input->post('SubTitle');
	      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';

	      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
	      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

	      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
	      move_uploaded_file($filename, $targetfolder_image);

	      $this->load->model('cms/manage_property_model');
	      $result = $this->manage_property_model->addZetaBanner($id,$image,$title,$sub);
	      if ($result == 1) {
	        redirect('cms/Manage_Zeta/index');
	      }
	      else
	      	redirect('index');
	    }
	  }

	  public function add_zeta_section_image()
	  {
	  	if($this->input->post()){
	      $id = $this->input->post('id');
	      $section = $this->input->post('section');
	      $image = basename($_FILES['Image']['name']);

	      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';

	      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
	      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

	      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
	      move_uploaded_file($filename, $targetfolder_image);

	      $this->load->model('cms/manage_property_model');
	      $result = $this->manage_property_model->add_zeta_section_image($id,$image,$section);
	      if ($result == 1) {
	        redirect('cms/Manage_Zeta/index');
	      }
	      else
	      	redirect('index');
	    }
	  }

	  public function update_zeta_banner()
	  {
	    if ($this->input->post()) {
	      $id         = $this->input->post('id');
	      $image      = basename( $_FILES['Image']['name']);
	      $title      = $this->input->post('Title');
	      $sub        = $this->input->post('SubTitle');


	      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
	      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

	      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
	      move_uploaded_file($filename, $targetfolder_image);

	      $this->load->model('cms/manage_property_model');
	      $result = $this->manage_property_model->update_zeta_banner($id, $image, $title, $sub);

	      if ($result == 1) {
	        redirect('cms/Manage_Zeta/index');
	      }
	    }
	  }

		public function update_zeta_section_info()
		{
			if($this->input->post())
			{
		      $id         = $this->input->post('id');
		      $title      = $this->input->post('Title');
		      $sub        = $this->input->post('SubTitle');
		      $desc 	  =	$this->input->post("desc");

		      $this->load->model('cms/manage_property_model');
		      $result = $this->manage_property_model->update_zeta_section_info($id, $title, $sub, $desc);

		      if ($result == 1) {
		        redirect('cms/Manage_Zeta/index');
		      }
			}
		}

	  public function update_zeta_section_image()
	  {
	    if ($this->input->post()) {
	      $id         = $this->input->post('id');
	      $image      = basename( $_FILES['Image']['name']);



	      $targetfolder = 'assets/propertygallery/images/Property-Gallery/astetica-residences/';
	      $targetfolder_image       = $targetfolder . basename( $_FILES['Image']['name']) ;

	      $filename = $this->compress_image($_FILES["Image"]["tmp_name"], $targetfolder_image);
	      move_uploaded_file($filename, $targetfolder_image);

	      $this->load->model('cms/manage_property_model');
	      $result = $this->manage_property_model->update_zeta_section_image($id, $image);

	      if ($result == 1) {
	        redirect('cms/Manage_Zeta/index');
	      }
	    }
	  }

		public function delete_zeta_banner()
		{
			$userdata = $this->session->userdata();

			if(isset($_GET['id'])){
			  $id = $_GET['id'];
			  $this->load->model('cms/manage_property_model');
			  $result = $this->manage_property_model->delete_zeta_banner($id);
			  if($result == 1)
			    redirect('cms/Manage_Zeta/index');
			}
		}

		public function delete_zeta_section_image()
		{
			$userdata = $this->session->userdata();

			if(isset($_GET['id'])){
			  $id = $_GET['id'];
			  $this->load->model('cms/manage_property_model');
			  $result = $this->manage_property_model->delete_zeta_section_Image($id);
			  if($result == 1)
			    redirect('cms/Manage_Zeta/index');
			}
		}





	}
 ?>