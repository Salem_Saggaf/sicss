<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
      $userdata = $this->session->userdata('user');

      if ($userdata == null) {
        $this->load->view('cms/login');
      }
      else {
        $this->load->model('cms/manage_leads_model');
        $data['lead'] = $this->manage_leads_model->get_available_leads();

        $this->load->view('cms/index',$data);

      }
    }


    public function logout()
    {
      $this->session->unset_userdata('user');
      $this->session->sess_destroy();
      redirect('cms/home','refresh');
    }

    public function test()
    {
      $test[] = $this->input->post('test');

      print_r($test);
    }
}
