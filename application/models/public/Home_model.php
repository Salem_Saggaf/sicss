<?php

  class Home_Model extends CI_Model
  {
    public function getOutbox()
    {
      $this->db->select('*');
      $this->db->from('message');
      $query = $this->db->get()->result();
      return $query;
    }


    public function setMessage($name, $email, $title, $comment, $submitted)
    {
      
      $data = array(
          
          'name' => $name,
        
          'email' => $email,
          'title' => $title,
          'content' => $comment,
          
          'submitted' => $submitted,
          
      );
      $result = $this->db->insert('message', $data);
      return $result;
    }

    public function deleteMessage($id)
    {
      $result = $this->db->delete('message', array('id' => $id));
      return $result;
    }



    public function insert_lead($name, $email, $contact, $comment, $type, $submitted, $month)
    {
      $emgs_data = array( 'form_name'           => $name,
                          'form_email'          => $email,
                          'form_contact'        => $contact,
                          'form_comment'        => $comment,
                          'form_type'           => $type,
                          'form_submitted'      => $submitted,
                          'form_month'          => $month
                          );

      $result = $this->db->insert('form_response', $emgs_data);

      return $result;
    }

  }

?>
