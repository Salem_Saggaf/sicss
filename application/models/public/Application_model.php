<?php 
	class Application_model extends CI_Model{

		public function application()
		{
			if($this->session->userdata('user') != null){
				$user = $this->session->userdata('user');

				$this->db->select('*');
				$this->db->from('application');
				$this->db->where(array('user_id'=> $user['user_id'], 'status >' => '1'));
				$query = $this->db->get()->result();
				return $query;
			}
			else
				return;	
		}

		public function study_plan($program)
		{
			$this->db->select('*');
			$this->db->from('study_plan_list');
			$this->db->where('study_plan_name', $program);
			$query = $this->db->get()->result();

			return $query;
		}

		public function insert_new_application($study_plan_name, $university_name, $department, $major, $duration_from, $duration_to, $years, $teaching_language, $apply_deadline, $notes, $user_id)
		{
			
			$this->db->select('*');
			$this->db->from('application');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get()->result();
			foreach ($query as $newQuery) {
				if($newQuery->university_name == $university_name)
				{
					return false;
				}
			}	
			$data = array(
					'type' => $study_plan_name,
					'university_name' => $university_name,
					'department' => $department,
					'major' => $major,
					'duration_from' => $duration_from,
					'duration_to' => $duration_to,
					'years' => $years,
					'teaching_language' => $teaching_language,
					'apply_deadline' => $apply_deadline,
					'notes' => $notes,
					'user_id' => $user_id,
					'status' => 0,
				);

			$result = $this->db->insert('application', $data);
			$id = $this->db->insert_id();
			return $id;
		}

		public function insert_basic_info($profile_picture, $familyName, $givenName, $chineseName, $nationality, $martial, $gender, $birthDate, 
			$birthPlace, $language, $educationLevel, $religion, $employer, $occupation, $health, $emigrant, $hobby, $passportNum, 
			$passportExpiry, $user_id, $status)
		{
				
			$this->load->library('session');
			$user = $this->session->userdata('user');
		    $data = array(
		      	  'profile_picutre' => $profile_picture,
		          'family_name' => $familyName,
		          'given_name' => $givenName,
		          'chinese_name' => $chineseName,
		          'nationality' => $nationality,
		          'martial_status' => $martial,
		          'gender' => $gender,
		          'birth_date' => $birthDate,
		          'place_of_birth' => $birthPlace,
		          'native_language' => $language,
		          'highest_level_of_education' => $educationLevel,
		          'religion' => $religion,
		          'employer_or_institution_affiliated' => $employer,
		          'occupation' => $occupation,
		          'health_status' => $health,
		          'emigrant_from' => $emigrant,
		          'hobby' => $hobby,
		          'passport_no' => $passportNum,
		          'passport_expiry' => $passportExpiry,
		          
		          'user_id' => $user_id,
		      );
		      if($status<=1){
		      	$data +=  array('status' => 1 );
		      }
		     $data = array_filter($data,'strlen');

		     $this->db->where('user_id', $user[0]['user_id']);
		     
		     $query = $this->db->update('application', $data);

		     return $query;
		}

		public function insert_study_plan($pro_language, $toefl, $gre, $gmat, $ielts, $other_language, $adjacment, $sd_college, 
			$sd_major,	$teaching_language, $years, $rec_name, $rec_relation, $rec_orgnaization, $rec_mobile, $rec_phone, $rec_email, 
			$rec_nationality, $rec_job, $rec_address, $rec_fax, $status)
		{
		   	$user = $this->session->userdata('user');	
		   	$data = array(
		                'language_proficiency' => $pro_language,
		                'toefl' => $toefl,
		                'gre' => $gre,
		                'gmat' => $gmat,
		                'ielts' => $ielts,
		                'other_language_proficiency' => $other_language,
		                'major_adjacment' => $adjacment,
		                'second_choice_college' => $sd_college,
		                'second_choice_major' => $sd_major,
		                'second_choice_teaching_language' => $teaching_language,
		                'second_choice_study_years' => $years,
		                'recommended_by_name' => $rec_name,
		                'recommended_by_relationship' => $rec_relation,
		                'recommended_by_orgnaization' => $rec_orgnaization,
		                'recommended_by_mobile' => $rec_mobile,
		                'recommended_by_phone_number' => $rec_phone,
		                'recommended_by_email' => $rec_email,
		                'recommended_by_nationality' => $rec_nationality,
		                'recommended_by_job' => $rec_job,
		                'recommended_by_address' => $rec_address,
		                'recommended_by_fax_number' => $rec_fax,
		              
		              );
			if($status<=2){
		      $data +=  array('status' => 2 );
		    }
		     $data = array_filter($data,'strlen');

		     $this->db->where('user_id', $user[0]['user_id']);
		     
		     $query = $this->db->update('application', $data);

		     return $query;
		}

		public function insert_educational_background($passport, $high_school, $english_test, $physical_test, $status)
		{
		   	$user = $this->session->userdata('user');	
		   	$data = array(
		                'photocopy_of_passport' => $passport,
		                'high_school_certificate' => $high_school,
		                'test_language_report' => $english_test,
		                'physical_exam_test' => $physical_test,
		              );

		   	if($status<=3){
		      $data +=  array('status' => 3 );
		    }
		     $data = array_filter($data,'strlen');

		     $this->db->where('user_id', $user[0]['user_id']);
		    
		     $query = $this->db->update('application', $data);

		     return $query;
		}

		public function insert_educational_background_other($year_attended_from, $year_attended_to, $school_name, $field_of_study, $id)
		{
			
			$data = array(
						'year_attended_from' => $year_attended_from,
						'year_attended_to' => $year_attended_to,
						'school_name' => $school_name,
						'field_of_study' => $field_of_study,
						'application_id' => $id,
					);
			$result = $this->db->insert('application_educational_background', $data);
			return $result;

		}



		public function insert_addtional_info($guarantor_name, $guarantor_tel, $guarantor_address, $guarantor_org, $guarantor_email, $em_name, $em_mobile, $em_phone_number, $em_email, $em_org, $em_address, $status)
		{
		   	$user = $this->session->userdata('user');	
		   	$data = array(
		                'guarantor_name' => $guarantor_name,
		                'guarantor_tel' => $guarantor_tel,
		                'guarantor_address' => $guarantor_address,
		                'guarantor_orgnaization' => $guarantor_org,
		                'guarantor_email' => $guarantor_email,
		                'emergency_contact_name' => $em_name,
		                'emergency_contact_mobile' => $em_mobile,
		                'emergency_contact_phone_number' => $em_phone_number,
		                'emergency_contact_email' => $em_email,
		                'emergency_contact_orgnaization' => $em_org,
		                'emergency_contact_address' => $em_address,
		                
		              );

		   	if($status<=4){
		      $data +=  array('status' => 4 );
		    }

		     $data = array_filter($data,'strlen');

		     $this->db->where('user_id', $user[0]['user_id']);
		     
		     $query = $this->db->update('application', $data);

		     return $query;
		}

		public function insert_family_member($family_member, $name, $phone_number, $email, $position, $work_place)
		{
			
			$data = array(
						'family_member' => $family_member,
						'name' => $name,
						'phone_number' => $phone_number,
						'email' => $email,
						'position' => $position,
						'work_place' => $work_place,
						'application_id' => $_SESSION['insert_id'],
					);
			$result = $this->db->insert('application_family_member', $data);
			return $result;

		}

		public function insert_contact_info($adress_home_address, $city_province, $country_home_address, $phone_number_home_address, $mobile_home_address, $zip_code_home_address, $same_country_home, $email_current_address, $mobile_current_address_input, $address_current_address_input, $zip_code_current_address_input, $radio_admission_notice, $reciver_name, $mobile_admission_notice, $city_admission_notice, $receiver_country, $reciver_address, $reciver_zip_code, $status, $application_payment_status)
		{
		   	$user = $this->session->userdata('user');	
		   	$data = array(
		                'home_address_street' => $adress_home_address,
		                'home_address_city' => $city_province,
		                'home_address_country' => $country_home_address,
		                'home_address_phone_number' => $phone_number_home_address,
		                'home_address_mobile' => $mobile_home_address,
		                'home_address_zip_code' => $zip_code_home_address,
		                'same_country_home' => $same_country_home,
		                'current_address_email' => $email_current_address,
		                'current_address_mobile' => $mobile_current_address_input,
		                'current_address' => $address_current_address_input,
		                'current_address_zip_code' => $zip_code_current_address_input,
		                'radio_admission_notice' => $radio_admission_notice,
		                'receiver_name' => $reciver_name,
		                'receiver_phone' => $mobile_admission_notice,
		                'receiver_city' => $city_admission_notice,
		                'receiver_country' => $receiver_country,
		                'receiver_address' => $reciver_address,
		                'receiver_zip_code' => $reciver_zip_code,

		              );

		   	if($status<=5){
		      $data +=  array('status' => 5 );
		    }

		    if($application_payment_status == null)
		    {
		    	$data += array('application_status' => 'pending');
		    }

		     $data = array_filter($data,'strlen');

		     $this->db->where('user_id', $user[0]['user_id']);
		     
		     $query = $this->db->update('application', $data);

		     return $query;
		}

		public function view_applications()
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application');
			$this->db->where('user_id', $user[0]['user_id']);
			$this->db->where('application_id', $_SESSION['insert_id'] );
			$query = $this->db->get()->result();
			return $query;
		}

		public function viewApplication($id)
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application');
			$this->db->where('user_id', $user[0]['user_id']);
			$this->db->where('application_id', $id);
			$query = $this->db->get()->result();
			return $query;
		}



		public function getUserApplicationStatus($id)
		{
			$user = $this->session->userdata('user');
			$this->db->select('status');
			$this->db->from('application');
			$this->db->where('user_id', $user[0]['user_id']);
			$this->db->where('application_id', $id );
			$query = $this->db->get()->result();
			return $query;
		}

		public function view_educational_background()
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application_educational_background');
			$this->db->where('application_id', $id );
			$query = $this->db->get()->result();
			return $query;
		}

		public function viewEducationalBackground($id)
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application_educational_background');
			$this->db->where('application_id', $id);
			$query = $this->db->get()->result();
			return $query;
		}

		public function view_family_member()
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application_family_member');
			$this->db->where('application_id', $id );
			$query = $this->db->get()->result();
			return $query;
		}

		public function viewFamilyMember($id)
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application_family_member');
			$this->db->where('application_id', $id);
			$query = $this->db->get()->result();
			return $query;
		}

		public function getApplicationById($id)
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application');
			$this->db->where('user_id', $user[0]['user_id']);
			$this->db->where('application_id', $id );
			$query = $this->db->get()->result();
			return $query;
		}

		public function deleteApplication($id)
		{
			$user = $this->session->userdata('user');
			$this->db->where('user_id', $user[0]['user_id']);
			$this->db->where('application_id', $id );
			$query = $this->db->delete('application');
			return $query;
		}



		public function getAllApplication()
		{
			$user = $this->session->userdata('user');
			$this->db->select('*');
			$this->db->from('application');
			$this->db->where('user_id', $user[0]['user_id']);
			$query = $this->db->get()->result();
			return $query;
		}

		public function getDistinctUniversityList()
		{
			$this->db->distinct();
			$this->db->select("*");
			$this->db->from("study_plan_list");
			$query = $this->db->get()->result();
			return $query;
		}

		public function getUniversity($department, $major, $university, $language)
		{
			$this->db->select("*");
			$this->db->from("study_plan_list");
			$this->db->like('department', $department);
			$this->db->like('major', $major);
			$this->db->like('teaching_language', $language);
			$this->db->like('university_name', $university);
			
			$query = $this->db->get()->result();
			return $query;
		}

		public function propertyBannerAstetika()
		{
			$this->db->select('*');
			$this->db->from('background_banner_for_property');
			$this->db->where('page','astetika');

			$query = $this->db->get()->result();
			return $query;

		}

		public function propertyBannerZeta()
		{
			$this->db->select('*');
			$this->db->from('background_banner_for_property');
			$this->db->where('page','zeta');
			$query = $this->db->get()->result();
			return $query;

		}

		public function propertyInfoAstetika()
		{
			$this->db->select('*');
			$this->db->from('property_info_for_property_page');
			$this->db->where(array('page' => 'astetika'));
			$query = $this->db->get()->result();
			return $query;
		}

		public function propertyInfoZeta()
		{
			$this->db->select('*');
			$this->db->from('property_info_for_property_page');
			$this->db->where(array('page' => 'zeta'));
			$query = $this->db->get()->result();
			return $query;
		}

		public function propertyImagesZeta()
		{
			$this->db->select('*');
			$this->db->from('property_image_for_property_page');
			$query = $this->db->get()->result();
			return $query;
		}	


	}
 ?>