<?php
  /**
   *
   */
  class Home_Model extends CI_Model
  {

    public function signup($email, $password, $username,  $token)
    {
      $user = array('email' => $email,'password' => $password, 'username' => $username, 'key_update' => $token );
      $result = $this->db->insert('user', $user);

      return $result;
    }

    public function is_exist($email)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->from('user');

      $is_exist = '';
      $query = $this->db->get()->result_array();
      if ($query == null) {
        $is_exist = 'false';
      }else {
        $is_exist = 'true';
      }
      return $is_exist;
    }

    public function user_data($email)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      return $query;
    }

    public function user($email, $password)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->where('password',$password);
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      return $query;
    }

    public function agent($email, $password)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->where('password',$password);
      $this->db->where('status','Active');
      $this->db->from('agent_data');
      $query = $this->db->get()->result_array();

      return $query;
    }

    public function login($email, $password)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->where('password',$password);
      $this->db->where('status','Active');
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      $is_exist = '';
      if ($query == null) {
        $is_exist = 'false';
      }else {
        $is_exist = 'true';
      }

      return $is_exist;
    }

    public function available_event()
    {
      $this->db->select('*');
      $this->db->from('event');
      $query = $this->db->get()->result();

      return $query;
    }

    
    public function update_password($email, $old_password, $new_password)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->where('password',$old_password);
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      if ($query != NULL) {
        $data = array( 'password' => $new_password);


        $this->db->where('password', $old_password);
        $this->db->where('email', $email);
        $query = $this->db->update('user', $data);

        return $query;
      }
      // print_r($query);
    }

    public function reset_password_check_email($email)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      $token = sha1(mt_rand(1, 90000) . 'SALT');

      if ($query != NULL) {
        $data = array( 'key_update' => $token,
                       'key_status' => 'Active');

        $this->db->where('email', $query[0]['email']);
        $this->db->update('user', $data);

        $this->db->select('*');
        $this->db->where('email',$email);
        $this->db->from('user');
        $query = $this->db->get()->result_array();

      }


      return $query;
    }

    public function key_status($userid, $token)
    {
      $this->db->select('*');
      $this->db->where('user_id',$userid);
      $this->db->where('key_update',$token);
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      return $query;
    }

    public function update_new_password($email, $new_password)
    {
        $data = array( 'password'   => $new_password,
                       'key_status' => 'Expired');

        $this->db->where('email', $email);
        $this->db->update('user', $data);
        $query = $this->db->affected_rows();

        return $query;
    }

    public function new_key_status($email, $token)
    {
      $this->db->select('*');
      $this->db->where('email',$email);
      $this->db->where('key_update',$token);
      $this->db->from('user');
      $query = $this->db->get()->result_array();

      return $query;
    }

    public function confirmed_account($email)
    {
        $data = array( 'status'     => 'Active',
                       'key_status' => 'Expired');

        $this->db->where('email', $email);
        $this->db->update('user', $data);
    }

  }

?>
