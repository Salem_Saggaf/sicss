<?php
  /**
   *
   */
  class Manage_Leads_Model extends CI_Model
  {

    public function get_available_leads()
    {
      $this->db->select('*');
      $this->db->from('message');
      $this->db->join('all_month','all_month.month_id = message.month');

      // $this->db->order_by("form_submitted", "desc");
      $query = $this->db->get()->result();

      return $query;
    }

    public function get_selected_leads($date_from, $date_till)
    {
      $this->db->select('*');
      $this->db->from('message');
      $this->db->where('submitted >=', $date_from);
      $this->db->where('submitted <=', $date_till);
      $this->db->join('all_month','all_month.month_id = message.month');

      $query = $this->db->get()->result();

      return $query;
    }

    public function get_selected_month_leads($month)
    {
      $this->db->select('*');
      $this->db->from('message');
      $this->db->where('month', $month);
      $this->db->join('all_month','all_month.month_id = message.month');
      $query = $this->db->get()->result();

      return $query;
    }

    public function get_available_months()
    {
      $this->db->distinct();
      $this->db->select('month,all_month.*');
      $this->db->from('message');
      $this->db->order_by("month", "asc");
      $this->db->join('all_month','all_month.month_id = message.month');
      $query = $this->db->get()->result();

      return $query;
    }

    public function delete_lead($application_id)
    {
      $this->db->where('id', $application_id);
      $query = $this->db->delete('message');

      return $query;
    }





  }

?>
