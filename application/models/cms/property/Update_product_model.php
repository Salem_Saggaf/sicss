<?php

  class Update_Product_Model extends CI_Model
  {
    public function product_banner($tag)
    {
      $this->db->select('*');
      $this->db->from('main_banner_for_main_page');
      //$this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function update_banner($id, $title, $subTitle, $image)
    {
      $data = array(
                      'title'             => $title,
                      'SubTitle'       => $subTitle,
                      'image'             => $image
                    );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('main_banner_for_main_page', $data);

      return $query;
    }

    public function property1_banner($tag)
    {
      $this->db->select('*');
      $this->db->from('property1_images_for_main_page');
      //$this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function property1_banner($id, $image)
    {
      $data = array(
                      'image'             => $image
                    );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property1_images__for_main_page', $data);

      return $query;
    }

    public function property2_banner($tag)
    {
      $this->db->select('*');
      $this->db->from('property2_images_for_main_page');
      //$this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function property2_banner($id, $image)
    {
      $data = array(
                      'image'             => $image
                    );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property2_images_for_main_page', $data);

      return $query;
    }

    public function product_feature($tag)
    {
      $this->db->select('*');
      $this->db->from('product_feature');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function update_product_feature($id, $title, $description, $icon)
    {
      $data = array(
                      'title'             => $title,
                      'description'       => $description,
                      'icon'             => $icon
                    );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('product_feature', $data);

      return $query;
    }

    public function product_description($tag)
    {
      $this->db->select('*');
      $this->db->from('product_description');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function update_product_description($desc)
    {
      $data = array(
                      'title'                     => $desc['title'],
                      'section1_title'            => $desc['title1'],
                      'section1_image'            => $desc['image1'],
                      'section1_desc'             => $desc['description1'],
                      'section1_keypoint1'        => $desc['sec1_keypoint1'],
                      'section1_keypoint2'        => $desc['sec1_keypoint2'],
                      'section1_keypoint3'        => $desc['sec1_keypoint3'],
                      'section1_icon1'            => $desc['sec1_icon1'],
                      'section1_icon2'            => $desc['sec1_icon2'],
                      'section1_icon3'            => $desc['sec1_icon3'],
                      'section2_title'            => $desc['title2'],
                      'section2_image'            => $desc['image2'],
                      'section2_desc'             => $desc['description2'],
                      'section2_keypoint1'        => $desc['sec2_keypoint1'],
                      'section2_keypoint2'        => $desc['sec2_keypoint2'],
                      'section2_keypoint3'        => $desc['sec2_keypoint3'],
                      'section2_icon1'            => $desc['sec2_icon1'],
                      'section2_icon2'            => $desc['sec2_icon2'],
                      'section2_icon3'            => $desc['sec2_icon3']
                    );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $desc['id']);
      $query = $this->db->update('product_description', $data);

      return $query;
    }

    public function product_testimonial($tag)
    {
      $this->db->select('*');
      $this->db->from('product_testimonial');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function update_product_testimonial($id, $title, $description, $video1, $video2)
    {
      $data = array('title'               => $title,
                    'description'         => $description,
                    'video1'               => $video1,
                    'video2'               => $video2
                  );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('product_testimonial', $data);

      return $query;
    }

    public function product_imagelist($tag)
    {
      $this->db->select('*');
      $this->db->from('product_imagelist');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function create_product_list($title, $image, $tag)
    {
      $data = array('title'             => $title,
                    'image'             => $image,
                    'tag'               => $tag
                  );

      $result = $this->db->insert('product_imagelist', $data);

      return $result;
    }

    public function update_product_list($id, $title, $image)
    {
      $data = array('title'             => $title,
                    'image'             => $image);

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('product_imagelist', $data);

      return $query;
    }

    public function delete_product_list($application_id)
    {
      $this->db->where('id', $application_id);
      $query = $this->db->delete('product_imagelist');

      return $query;
    }

    public function titleimage($tag)
    {
      $this->db->select('*');
      $this->db->from('product_titleimage');
      $this->db->where('tag', $tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function update_titleimage($id, $title, $description, $image)
    {
      $data = array(
                      'title'             => $title,
                      'description'       => $description,
                      'image'             => $image
                    );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('product_titleimage', $data);

      return $query;
    }

  }
?>
