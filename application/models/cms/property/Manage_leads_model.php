<?php
  /**
   *
   */
  class Manage_Leads_Model extends CI_Model
  {

    public function get_available_leads()
    {
      $this->db->select('*');
      $this->db->from('com_message_pump()');
      // $this->db->where('product_form_response.tag','9dvr');
      // $this->db->join('month','month.id = message.month');

      // $this->db->order_by("form_submitted", "desc");
      $query = $this->db->get()->result();

      return $query;
    }

    public function get_selected_leads($date_from, $date_till)
    {
      $this->db->select('*');
      $this->db->from('product_form_response');
      $this->db->where('form_submitted >=', $date_from);
      $this->db->where('form_submitted <=', $date_till);
      // $this->db->where('product_form_response.tag','9dvr');
      $this->db->join('month','month.id = product_form_response.form_month');

      $query = $this->db->get()->result();

      return $query;
    }

    public function get_selected_month_leads($month)
    {
      $this->db->select('*');
      $this->db->from('product_form_response');
      $this->db->where('form_month', $month);
      // $this->db->where('product_form_response.tag','9dvr');
      $this->db->join('month','month.id = product_form_response.form_month');
      $query = $this->db->get()->result();

      return $query;
    }

    public function get_available_months()
    {
      $this->db->distinct();
      $this->db->select('form_month,month.*');
      $this->db->from('product_form_response');
      // $this->db->where('product_form_response.tag','9dvr');
      $this->db->order_by("form_month", "asc");
      $this->db->join('month','month.id = product_form_response.form_month');
      $query = $this->db->get()->result();

      return $query;
    }

    public function get_available_tag()
    {
      $this->db->distinct();
      $this->db->select('tag');
      $this->db->from('product_form_response');
      $query = $this->db->get()->result();

      return $query;
    }

    public function get_selected_product_leads($tag)
    {
      $this->db->select('*');
      $this->db->from('product_form_response');
      $this->db->where('product_form_response.tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function delete_lead($application_id)
    {
      $this->db->where('form_id', $application_id);
      $query = $this->db->delete('product_form_response');

      return $query;
    }





  }

?>
