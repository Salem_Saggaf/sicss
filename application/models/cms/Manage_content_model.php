<?php

  class Manage_Content_Model extends CI_Model
  {


    public function banner()
    {
      $this->db->select('*');
      $this->db->from('main_banner_for_main_page');
      $query = $this->db->get()->result();

      return $query;
    }

    public function add_image_banner($id, $image, $title, $sub)
    {
      $data = array(
          'Image'             => $image,
          'Title'             => $title,
          'SubTitle'          => $sub,
        );
      $query = $this->db->insert('main_banner_for_main_page', $data);

      return $query;
    }

    public function update_banner($id, $image, $title, $sub)
    {
      $data = array(
                'Image'             => $image,
                'Title'             => $title,
                'SubTitle'          => $sub,
              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('main_banner_for_main_page', $data);

      return $query;
    }

    public function delete_banner($id)
    {
     
      $query = $this->db->delete('main_banner_for_main_page', array('id' => $id));

      return $query;
    }

    public function property1()
    {
      $this->db->select('*');
      $this->db->from('property1_images_for_main_page');
      $query = $this->db->get()->result();

      return $query;
    }

    public function add_image_property1($id, $image)
    {
      $data = array(
          'Image'             => $image,

        );
      $query = $this->db->insert('property1_images_for_main_page', $data);

      return $query;
    }


    public function update_property1($id, $image)
    {
      $data = array(
                'Image'             => $image,

              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property1_images_for_main_page', $data);

      return $query;
    }
    public function delete_property1($id)
    {
     
      $query = $this->db->delete('property1_images_for_main_page', array('id' => $id));

      return $query;
    }

    public function property2()
    {
      $this->db->select('*');
      $this->db->from('property2_images_for_main_page');
      $query = $this->db->get()->result();

      return $query;
    }

    public function add_image_property2($id, $image)
    {
      $data = array(
          'Image'             => $image,

        );
      $query = $this->db->insert('property2_images_for_main_page', $data);

      return $query;
    }


    public function update_property2($id, $image)
    {
      $data = array(
                'Image'             => $image,

              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property2_images_for_main_page', $data);

      return $query;
    }

    public function delete_property2($id)
    {

      $query = $this->db->delete('property2_images_for_main_page', array('id' => $id));

      return $query;
    }


    public function update_key_points($id, $icon, $description, $position)
    {
      $data = array(
        'icon'             => $icon,
        'description'      => $description,
        'position'         => $position,
      );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('key_point', $data);

      return $query;
    }


    public function delete_portfolio($application_id)
    {
      $this->db->where('id', $application_id);
      $query = $this->db->delete('portfolio');

      return $query;
    }


    public function create_product_list($title, $image)
    {
      $data = array('title'             => $title,
                    'image'             => $image);

      $result = $this->db->insert('product_list', $data);

      return $result;
    }

    public function update_product_list($id, $title, $image)
    {
      $data = array('title'             => $title,
                    'image'             => $image);

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('product_list', $data);

      return $query;
    }

    public function delete_product_list($application_id)
    {
      $this->db->where('id', $application_id);
      $query = $this->db->delete('product_list');

      return $query;
    }


    public function update_quote($id, $name, $description, $image)
    {
      $data = array('name'             => $name,
                    'description'      => $description,
                    'image'            => $image
                  );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('quote', $data);

      return $query;
    }

    public function update_portfolio($id, $description, $tag, $image)
    {
      $data = array('tag'              => $tag,
                    'description'      => $description,
                    'image'            => $image
                  );

      $data = array_filter($data,'strlen');


      $this->db->where('id', $id);
      $query = $this->db->update('portfolio', $data);

      return $query;
    }


    public function update_testimonial($id, $title, $description, $video)
    {
      $data = array('title'               => $title,
                    'description'         => $description,
                    'video'               => $video
                  );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('testimonial', $data);

      return $query;
    }


  }
?>
