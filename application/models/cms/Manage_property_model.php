<?php 
  class Manage_Property_Model extends CI_Model{
    public function astetikaBanner()
    {
      $this->db->select('*');
      $this->db->from('background_banner_for_property');
      $this->db->where('page','astetika');
      $query = $this->db->get()->result();

      return $query;
    }

    public function astetikaSectionInfo()
    {
      $this->db->select('*');
      $this->db->from('property_info_for_property_page');
      $this->db->where('page', 'astetika');
      $query = $this->db->get()->result();

      return $query;
    }

    public function astetikaSectionImage()
    {
      $this->db->select('*');
      $this->db->from('property_image_for_property_page');
      $this->db->where('page', 'astetika');
      $query = $this->db->get()->result();

      return $query;
    }

    public function zetaBanner()
    {
      $this->db->select('*');
      $this->db->from('background_banner_for_property');
      $this->db->where('page','zeta');
      $query = $this->db->get()->result();

      return $query;
    }

    public function zetaSectionInfo()
    {
      $this->db->select('*');
      $this->db->from('property_info_for_property_page');
      $this->db->where('page', 'zeta');
      $query = $this->db->get()->result();

      return $query;
    }

    public function zetaSectionImage()
    {
      $this->db->select('*');
      $this->db->from('property_image_for_property_page');
      $this->db->where('page', 'zeta');
      $query = $this->db->get()->result();

      return $query;
    }

    public function addAstetikaBanner($id, $image, $title, $sub)
    {
      $data = array(
          'Image'             => $image,
          'Title'             => $title,
          'SubTitle'          => $sub,
          'page'              => 'astetika',
        );
      $query = $this->db->insert('background_banner_for_property', $data);

      return $query;
    }

    public function addZetaBanner($id, $image, $title, $sub)
    {
      $data = array(
          'Image'             => $image,
          'Title'             => $title,
          'SubTitle'          => $sub,
          'page'              => 'zeta',
        );
      $query = $this->db->insert('background_banner_for_property', $data);

      return $query;
    }

    public function add_astetika_section_image($id, $image,$section)
    {
      $data = array(
          'Image'             => $image,
          'section'           => $section,
          'page'              => 'astetika',
        );
      $query = $this->db->insert('property_image_for_property_page', $data);

      return $query;
    }

    public function add_zeta_section_image($id, $image,$section)
    {
      $data = array(
          'Image'             => $image,
          'section'           => $section,
          'page'              => 'zeta',
        );
      $query = $this->db->insert('property_image_for_property_page', $data);

      return $query;
    }


    public function update_astetika_banner($id, $image, $title, $sub)
    {
      $data = array(
                'Image'             => $image,
                'Title'             => $title,
                'SubTitle'          => $sub,
              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('background_banner_for_property', $data);

      return $query;
    }

        public function update_zeta_banner($id, $image, $title, $sub)
    {
      $data = array(
                'Image'             => $image,
                'Title'             => $title,
                'SubTitle'          => $sub,
              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('background_banner_for_property', $data);

      return $query;
    }

    public function update_astetika_section_info($id, $title, $sub, $desc)
    {
      $data = array(
                'Image'             => $title,
                'Title'             => $sub,
                'SubTitle'          => $desc,
              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property_info_for_property_page', $data);

      return $query;
    }

    public function update_zeta_section_info($id, $title, $sub, $desc)
    {
      $data = array(
                'Image'             => $title,
                'Title'             => $sub,
                'SubTitle'          => $desc,
              );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property_info_for_property_page', $data);

      return $query;
    }

    public function update_astetika_section_image($id, $image)
    {
      $data = array(
              'Image'           => $image,
      );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property_image_for_property_page', $data);

      return $query;
    }

    public function update_zeta_section_image($id, $image)
    {
      $data = array(
              'Image'           => $image,
      );

      $data = array_filter($data,'strlen');

      $this->db->where('id', $id);
      $query = $this->db->update('property_image_for_property_page', $data);

      return $query;
    }

    public function delete_astetika_banner($id)
    {
     
      $query = $this->db->delete('background_banner_for_property', array('id' => $id));

      return $query;
    }

    public function delete_zeta_banner($id)
    {
     
      $query = $this->db->delete('background_banner_for_property', array('id' => $id));

      return $query;
    }

    public function delete_astetika_section_Image($id)
    {
     
      $query = $this->db->delete('property_image_for_property_page', array('id' => $id));

      return $query;
    }

    public function delete_zeta_section_Image($id)
    {
     
      $query = $this->db->delete('property_image_for_property_page', array('id' => $id));

      return $query;
    }

  }
 ?>