<?php
  /**
   *
   */
  class Download_Report_Model extends CI_Model
  {

    public function get_available_reports()
    {
      $this->db->select('*');
      $this->db->from('report');
      $query=$this->db->get()->result();

      return $query;
    }

    public function check_status($passport_no,$university)
    {
      $this->db->select('*');
      $this->db->where('passport',$passport_no);
      $this->db->where('uni_name',$university);
      $this->db->from('application_status');

      $query=$this->db->get()->result();

      return $query;
    }

    public function update_download($application_id)
    {
      $this->db->where('id', $application_id);
      $this->db->set('download', 'download+1', FALSE);
      $this->db->update('report');
    }



  }

?>
