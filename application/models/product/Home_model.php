<?php

  class Home_Model extends CI_Model
  {

    public function product_banner($tag)
    {
      $this->db->select('*');
      $this->db->from('product_banner');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function feature($tag)
    {
      $this->db->select('*');
      $this->db->from('product_feature');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function description($tag)
    {
      $this->db->select('*');
      $this->db->from('product_description');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function testimonial($tag)
    {
      $this->db->select('*');
      $this->db->from('product_testimonial');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function imagelist($tag)
    {
      $this->db->select('*');
      $this->db->from('product_imagelist');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }

    public function titleimage($tag)
    {
      $this->db->select('*');
      $this->db->from('product_titleimage');
      $this->db->where('tag',$tag);
      $query = $this->db->get()->result();

      return $query;
    }


    public function insert_lead($name, $email, $contact, $message, $tag, $submitted, $month)
    {
      $emgs_data = array( 'form_name'           => $name,
                          'form_email'          => $email,
                          'form_contact'        => $contact,
                          'form_message'        => $message,
                          'form_submitted'      => $submitted,
                          'tag'                 => $tag,
                          'form_month'          => $month
                          );

      $result = $this->db->insert('product_form_response', $emgs_data);

      return $result;
    }

  }

?>
