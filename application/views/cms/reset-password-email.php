<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>
  </head>

  <body class="  footer_fixed" style=" background: #F7F7F7;">
    <div class="container body">


        <!-- page content -->
        <div class="right_col" role="main">

            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12" style="position: absolute; top: 40%;">
                <div class="">
                  <div class=" ">
                    <br><br>
                    <form  method="post" action="<?php echo base_url();?>agent/Login/update_new_password" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" >Email Address <span class="required">*</span><small><br>(as registered)</small></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input value="<?=$userdata[0]['email']?>" class="form-control col-md-7 col-xs-12" name="email" required type="email" readonly >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="password" class="control-label col-md-3">New Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password" type="password" name="password" placeholder="New Password" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password2" type="password" name="password2" data-validate-linked="password" placeholder="Repeat Password" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 padding"><input name="" type="submit" class="btn btn-primary right" value="Submit"></div>
                        <div class="col-md-3"></div>
                      </div>
                    </form>
                  </div>
                </div>
                <br>
              </div>
             </div>
         </div>
        <!-- /page content -->


        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
     </div>

    <?php include('init/footer.php'); ?>

    <?php if ($userdata[0]['key_status'] == 'Expired'): ?>
      <script type="text/javascript">
        alert("This link has Expired");
        window.location.href = "index";
      </script>
    <?php endif; ?>



  </body>
</html>
