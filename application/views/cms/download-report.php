<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>

  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Available Materials <small> Please find all related materials here</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12 text-center"></div>

                      <div class="clearfix"></div>
                      <?php if ($report == NULL): ?>
                        <p style="text-align:center">No reports available in table</p>
                      <?php endif; ?>

                      <?php foreach ($report as $key => $reports): ?>
                        <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                          <div class="well profile_view">
                            <div class="col-sm-12">
                              <h4 class="brief"><i><?=$reports->month?></i></h4>
                              <div class="left col-xs-7">
                                <h2>Monthly Report</h2>
                                <p style="text-align:justify"><?=$reports->description?></p>
                              </div>
                              <div class="right col-xs-5 text-center">
                                <img src="<?php echo base_url();?>assets/build/images/pdf.ico" alt="" class="img-circle img-responsive">
                              </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                              <div class="col-xs-12 col-sm-6 emphasis">
                                <p class="ratings">

                                </p>
                              </div>
                              <div class="col-xs-12 col-sm-6 emphasis">
                                <!-- <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user"> -->
                                <!-- </i> <?=$reports->download?> </button> -->
                                <button type="button" class=" btn btn-primary btn-xs ">
                                  <i class="fa fa-user"> </i> <a style="color: #ffffff" href="../material/<?=$reports->file?>" >Download</a>
                                  <!-- <i class="fa fa-user"> </i> <a style="color: #ffffff"  onclick="myFunction(<?=$reports->id?>)" >Download</a> -->
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endforeach; ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <!-- <script>
      function myFunction(id) {
          window.location = '<?php echo base_url();?>cms/Download_Report/update_download?id='+id;
          window.location = "../material/<?=$reports->file?>";

      }
    </script> -->

  </body>
</html>
