
    <html>

    <head>
      <link rel="stylesheet" href="<?php echo base_url();?>assets/build/css/email.css">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width">

    </head>

        <body class="no-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;">
         <table class="wrapper" style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #fff;" cellpadding="0" cellspacing="0" role="presentation">
          <tbody>
            <tr>
              <td>
                <div role="banner">
                  <div class="preheader" style="Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 167440px);">
                    <div style="border-collapse: collapse;display: table;width: 100%;">
                      <div class="snippet" style="display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #999;font-family: Georgia,serif;">
                        <div style="mso-hide:all;position:fixed;height:0;max-height:0;overflow:hidden;font-size:0;">You need to click inside this email to officially subscribe! It only takes 10
                          seconds.</div>
                      </div>
                      <div class="webversion" style="display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 139px; width: 139px;width: calc(14100% - 78680px);padding: 10px 0 5px 0;text-align: right;color: #999;font-family: Georgia,serif;"></div>
                    </div>
                  </div>
                  <div class="header" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);"
                  id="emb-email-header-container">

                      <div class="logo emb-logo-margin-box" style="font-size: 26px;line-height: 32px;Margin-top: 25px;Margin-bottom: 25px;color: #41637e;font-family: Avenir,sans-serif;Margin-left: 20px;Margin-right: 20px;"
                      align="center">
                        <div class="logo-left" align="left" id="emb-email-header">
                          <a style="text-decoration: none;transition: opacity 0.1s ease-in;color: #41637e;"
                          href="https://www.gostudy.my">
                            <img style="display: block; "
                            src="<?php echo base_url();?>assets/paveredu/images/logo-home.png">
                          </a>
                        </div>
                      </div>

                  </div>
                </div>
                <div role="section">
                  <div class="layout one-col fixed-width" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                    <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;">

                            <div class="column" style="text-align: left;color: #333;font-size: 14px;line-height: 21px;font-family: Georgia,serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">
                              <div style="Margin-left: 20px;Margin-right: 20px;">
                                 <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #333;font-size: 17px;line-height: 26px;"><strong>Thanks for subscribing to the This Week In Cats newsletter! </strong></h2>
                                <h3 style="Margin-top: 16px;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #333;font-size: 16px;line-height: 24px;">But before we can send you a newsletter, we need to confirm that you're a real person who wants emails from us!</h3>
                                <h3 style="Margin-top: 12px;Margin-bottom: 12px;font-style: normal;font-weight: normal;color: #333;font-size: 16px;line-height: 24px;">If you are, click here:</h3>

                              </div>
                              <div style="Margin-left: 20px;Margin-right: 20px;">
                                <div class="btn fullwidth btn--shadow btn--large" style="text-align: center;">
                                     <a style="border-radius: 4px;display: block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px 13px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #ffffff !important;box-shadow: inset 0 -2px 0 0 rgba(0, 0, 0, 0.2);background-color: #ee3322;font-family: Georgia, serif;"
                                    href="#">Yes, I'm a real person!</a>

                                </div>
                              </div>
                            </div>

                    </div>
                  </div>
                  <div style="line-height:20px;font-size:20px;">&nbsp;</div>

                  <div style="line-height:20px;font-size:20px;">&nbsp;</div>
                  <div role="contentinfo">
                    <div class="layout email-footer" style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                      <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">

                              <div class="column wide" style="text-align: left;font-size: 12px;line-height: 19px;color: #999;font-family: Georgia,serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);">
                                <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;">
                                  <table class="email-footer__links emb-web-links" style="border-collapse: collapse;table-layout: fixed;"
                                  role="presentation">
                                    <tbody>
                                      <tr role="navigation">
                                        <td class="emb-web-links" style="padding: 0;width: 26px;">
                                          <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #999;"
                                          href="https://www.facebook.com/BuzzFeedAnimals/">
                                            <img style="border: 0;" src="http://i8.cmail20.com/static/eb/master/13-the-blueprint-3/images/facebook.png"
                                            width="26" height="26">
                                          </a>
                                        </td>
                                        <td class="emb-web-links" style="padding: 0 0 0 3px;width: 26px;">
                                          <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #999;"
                                          href="https://www.instagram.com/BuzzFeedAnimals/">
                                            <img style="border: 0;" src="http://i4.cmail20.com/static/eb/master/13-the-blueprint-3/images/instagram.png"
                                            width="26" height="26">
                                          </a>
                                        </td>
                                        <td class="emb-web-links" style="padding: 0 0 0 3px;width: 26px;">
                                          <a style="text-decoration:
underline;transition: opacity 0.1s ease-in;color: #999;" href="https://www.buzzfeed.com/animals?utm_medium=email&utm_campaign=Confirmation%20Email&utm_content=Confirmation%20Email+&utm_source=BuzzFeed%20Newsletters">
                                            <img style="border: 0;" src="http://i2.cmail20.com/static/eb/master/13-the-blueprint-3/images/website.png"
                                            width="26" height="26">
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <div style="font-size: 12px;line-height: 19px;Margin-top: 20px;">
                                    <div>Unit C-G-18, CoPlace 1,
                                      <br>Jalan Usahawan 2
                                      <br>63000 Cyberjaya, Selangor, Malaysia.</div>
                                  </div>
                                  <div style="font-size: 12px;line-height: 19px;Margin-top: 18px;"></div>
                                </div>
                              </div>

                              <div class="column narrow" style="text-align: left;font-size: 12px;line-height: 19px;color: #999;font-family: Georgia,serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);">
                                <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;"></div>
                              </div>

                      </div>
                    </div>

                  </div>
                  <div style="line-height:40px;font-size:40px;">&nbsp;</div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>

        </body>

    </html>
