<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>

  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <!-- <div class="col-md-6">
              <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
              </div>
            </div> -->

            <div class="row">

              <!-- <?php foreach ($lead as $key => $leads): ?>
                <?php print_r($leads) ?>
              <?php endforeach; ?> -->
              <!-- <div id="chartContainer" style="height: 370px; width: 100%;"></div> -->

            </div>
          </div>
        </div>
        <!-- /page content -->

        <br><br>
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


    <?php include('init/footer.php'); ?>


    <script>
      function myFunction() {
          alert("Your application has approved. Please contact admin to make changes");
      }
    </script>




  </body>
</html>
