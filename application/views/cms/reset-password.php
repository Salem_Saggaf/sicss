<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>User Login</title>
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/paveredu/images/favicon.png" type="image/x-icon">

  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">

  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans'>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/build/css/login.css">


</head>

<body>
  <div class="cont" style="background-image: url(<?php echo base_url();?>assets/build/images/gostudy2.png);">
    <div class="demo">

      <div class="login">
        <div class="login-img"><img  src="<?php echo base_url();?>assets/paveredu/images/logo-home.png" alt="GoStudy.My"></div>
        <div class="login__form">
          <form class="" action="<?php echo base_url();?>application-portal/Login/reset_password" method="post">
            <div class="login__row">
              <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                <path d="M17.388,4.751H2.613c-0.213,0-0.389,0.175-0.389,0.389v9.72c0,0.216,0.175,0.389,0.389,0.389h14.775c0.214,0,0.389-0.173,0.389-0.389v-9.72C17.776,4.926,17.602,4.751,17.388,4.751 M16.448,5.53L10,11.984L3.552,5.53H16.448zM3.002,6.081l3.921,3.925l-3.921,3.925V6.081z M3.56,14.471l3.914-3.916l2.253,2.253c0.153,0.153,0.395,0.153,0.548,0l2.253-2.253l3.913,3.916H3.56z M16.999,13.931l-3.921-3.925l3.921-3.925V13.931z"></path>
              </svg>
              <input type="email" name="email" class="login__input name" placeholder="email" required/>
            </div>
            <br><br><br>
            <input type="submit"  class="login__submit" name="login" value="Send">
          </form>
          <p class="login__signup">Remembered? &nbsp;<a href="<?php echo base_url();?>application-portal/Login/" >Sign In</a></p>
         </div>
      </div>


      </div>
    </div>
   <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script src="<?php echo base_url();?>assets/build/js/login.js"></script>
  <script type="text/javascript">
      var error = '<?=$result?>';
      var email = '<?php if ($email != NULL) {
        echo $email[0]['email'];
      } else {
        echo " ";
      }
      ?>';

      if (error == 'negative') {
        alert('Incorrect Email, Please Check Your Input');
      }
      else {
        alert('email had been sent to '+ email);

      }
   </script>



</body>
</html>
