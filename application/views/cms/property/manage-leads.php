<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>

  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <div >
                  <h3>Manage  <small> Leads</small></h3>
                </div>
                <!-- <form class="" action="<?php echo base_url();?>cms/Manage_Leads/search_leads" method="post">
                  <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                    <br><div class="input-group">
                      <table>
                        <tr>
                            <td>
                                <input class="form-control" style="width:50%" type="date" name="from" class="form-first-name" required />
                                <input class="form-control" style="width:50%; float:right" type="date" name="till" class="form-last-name" required/>
                            </td>
                        </tr>
                      </table>

                      <span class="input-group-btn">
                         <button style="color:#ffffff" type="submit" class="btn btn-primary btn-sm btn-block">Search Leads &emsp;<i class="fa fa-search" aria-hidden="true"></i> </button>
                      </span>
                     </div>
                  </div>
                </form> -->
              </div>
              <div class="title_right">
                <!-- <form class="" action="index.html" method="post">
                  <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                         <select class="form-control" name="month" required>
                          <option value="">Select Month</option>
                          <?php foreach ($month as $key => $months): ?>
                            <option value="<?=$months->form_month?>"><?=$months->month?></option>
                          <?php endforeach; ?>
                        </select>
                        <span class="input-group-btn">
                           <button style="color:#ffffff" type="submit" class="btn btn-primary btn-sm btn-block">Download Leads</button>
                        </span>
                     </div>
                  </div>
                </form> -->
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <form class="" action="<?php echo base_url();?>cms/product/Manage_Leads/search_leads" method="post">
                          <div class=" form-group pull-right top_search">
                            <div class="input-group">
                              <table>
                                <tr>
                                    <td>
                                        <input class="form-control" style="width:50%" type="date" name="from" class="form-first-name" required />
                                        <input class="form-control" style="width:50%; float:right" type="date" name="till" class="form-last-name" required/>
                                    </td>
                                </tr>
                              </table>

                              <span class="input-group-btn">
                                 <button style="color:#ffffff" type="submit" class="btn btn-primary btn-sm btn-block">Search / Filter Leads <i class="fa fa-search" aria-hidden="true"></i> </button>
                              </span>
                             </div>
                          </div>
                        </form>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <form class="" action="<?php echo base_url();?>cms/product/Manage_Leads/download_leads_product" method="post">
                          <div class="  form-group pull-right top_search">
                            <div class="input-group">
                                 <select class="form-control" name="tag" required>
                                  <option value="">Select Product</option>
                                  <?php foreach ($tag as $key => $tag): ?>
                                    <option value="<?=$tag->tag?>"><?=$tag->tag?></option>
                                  <?php endforeach; ?>
                                </select>
                                <span class="input-group-btn">
                                   <button style="color:#ffffff" type="submit" class="btn btn-primary btn-sm btn-block">Select Product</button>
                                </span>
                             </div>
                          </div>
                        </form>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <form class="" action="<?php echo base_url();?>cms/product/Manage_Leads/download_leads" method="post">
                          <div class="  form-group pull-right top_search">
                            <div class="input-group">
                                 <select class="form-control" name="month" required>
                                  <option value="">Select Month</option>
                                  <?php foreach ($month as $key => $months): ?>
                                    <option value="<?=$months->form_month?>"><?=$months->month?></option>
                                  <?php endforeach; ?>
                                </select>
                                <span class="input-group-btn">
                                   <button style="color:#ffffff" type="submit" class="btn btn-primary btn-sm btn-block">Monthly Leads</button>
                                </span>
                             </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Submitted</th>
                          <th>Product</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Contact</th>
                          <th>Message</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($lead as $key => $leads): ?>
                          <tr>
                            <td><?=$leads->form_submitted?></td>
                            <td><?=$leads->tag?></td>
                            <td><?=$leads->form_name?></td>
                            <td><?=$leads->form_email?></td>
                            <td><?=$leads->form_contact?></td>
                            <td><?=$leads->form_message?></td>
                            <td style="text-align:center"><a onclick="myFunction(<?=$leads->form_id?>)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                    <form class="" action="<?php echo base_url();?>cms/product/Manage_Leads/download_selected_leads" method="post">
                      <input type="hidden" name="from" value="<?=$from?>">
                      <input type="hidden" name="till" value="<?=$till?>">
                      <button style="float:right" type="submit" class="btn btn-primary">Download This Data <i class="fa fa-download"></i></button>
                    </form>
                  </div>
                </div>
              </div>
            </div>


          </div>
          <br><br><br>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <script>
      function myFunction(id) {
           var r = confirm("Do you want to delete application Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/product/Manage_Leads/delete_lead?id='+id;
          }
      }
    </script>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url();?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/build/js/custom.min.js"></script>

  </body>
</html>
