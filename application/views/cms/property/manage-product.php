<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Update <small>Product Content</small> </h3>
              </div>
              <div class="title_right"></div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <?php foreach ($tag as $key => $tags): ?>
                      <div class="col-md-4 col-sm-6 col-xs-12 profile_details">
                        <a href="<?php echo base_url();?>cms/product/Update_Product/index?tag=<?=$tags->tag?>">
                          <div class="well profile_view">
                              <div class="left col-xs-6">
                                <h2 style="margin-top:2em;"><?=$tags->tag?></h2>
                                <!-- <p style="text-align:justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p> -->
                              </div>
                              <div class="right col-xs-6 text-center">
                                <img src="<?php echo base_url();?>assets/product/img/<?=$tags->image?>" alt="" class="img-circle img-responsive">
                              </div>
                          </div>
                        </a>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
            </div>
          </div> <br><br>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <script type="text/javascript">
      document.getElementById('image').addEventListener('change', checkFile, false);

      function checkFile(e) {

          var file_list = e.target.files;

          for (var i = 0, file; file = file_list[i]; i++) {
              var sFileName = file.name;
              var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
              var iFileSize = file.size;
              var iConvert = (file.size / 10485760).toFixed(2);

              if (!(sFileExtension === "png" || sFileExtension === "jpg" || sFileExtension === "jpeg") || iFileSize > 10485760) {
                  txt = "File type : " + sFileExtension + "\n\n";
                  txt += "Size: " + iConvert + " MB \n\n";
                  txt += "Please make sure your file is in pdf or jpg format and less than 5 MB.\n\n";
                  alert(txt);
                  $("#image").val("");
              }
          }
      }
    </script>

    <script>
      function delete_portfolio(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/Manage_Content/delete_portfolio?id='+id;
          }
      }
    </script>
    <script>
      function delete_architecture(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/Manage_Content/delete_architecture?id='+id;
          }
      }
    </script>
    <script>
      function delete_testimonial(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/Manage_Content/delete_testimonial?id='+id;
          }
      }
    </script>

  </body>
</html>
