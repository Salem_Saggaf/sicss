<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Update <small>Content</small> </h3>
              </div>

              <div class="title_right">

              </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Banner
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <div class="row">
                              <?php foreach ($product_banner as $key => $product_banners): ?>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_banner" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$product_banners->id?>" name="id" type="hidden">
                                    <input value="<?=$product_banners->tag?>" name="tag" type="hidden">

                                    <div class="row">
                                      <div class="col-md-10">

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/product/img/<?=$product_banners->image?>" alt="vs" class="img-responsive">
                                             <p>Image size 765px x 600px</p>
                                           </div>
                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="image" name="image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>
                                           <div class="item form-group">
                                             <label  for="name" >Title: </label>
                                             <input required value="<?=$product_banners->title?>" class="form-control col-md-7 col-xs-12"  name="title" type="text">
                                            </div>
                                            <div class="item form-group">
                                              <label  for="name" >Description: </label>
                                               <input required value="<?=$product_banners->description?>" class="form-control col-md-7 col-xs-12"  name="description" type="text">
                                           </div>

                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Key Information
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">

                            <p>Find the icons here <a target="_blank" style="color:red" href="http://demos.creative-tim.com/now-ui-kit/nucleo-icons.html">Icons</a> </p>
                            <div class="row">

                              <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_titleimage" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?=$titleimage[0]->id?>">
                                <input type="hidden" name="tag" value="<?=$titleimage[0]->tag?>">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="item form-group">
                                      <label  for="name" >Title: </label>
                                         <input required class="form-control col-md-7 col-xs-12" value="<?=$titleimage[0]->title?>" name="title" type="text">
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div>
                                      <div class="item form-group">
                                        <label  for="name" >Image: </label>
                                        <input  id="image" name="image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                      </div>
                                    </div>

                                  </div>
                                </div>

                                <input name="update" type="submit" class="btn btn-primary right" value="Update">
                              </form>

                              <?php foreach ($product_feature as $key => $product_features): ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_product_feature" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$product_features->id?>" name="id" type="hidden">
                                    <input value="<?=$product_features->tag?>" name="tag" type="hidden">

                                    <div class="row">
                                      <div class="col-md-10">
                                        <div class="item form-group">
                                          <label  for="name" >Icon: </label>
                                             <input  class="form-control col-md-7 col-xs-12" value="<?=$product_features->icon?>" name="icon" type="text">
                                         </div>
                                        <div class="item form-group">
                                          <label  for="name" >Title: </label>
                                             <input value="<?=$product_features->title?>" class="form-control col-md-7 col-xs-12"  name="title" type="text">
                                        </div>
                                        <div class="item form-group">
                                          <label  for="name" >Description: </label>
                                             <input value="<?=$product_features->description?>" class="form-control col-md-7 col-xs-12"  name="description" type="text">
                                        </div>


                                         <!-- <button onclick="delete_portfolio(<?=$key_points->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Hot Products
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            <p>Find the icons here <a target="_blank" style="color:red" href="http://demos.creative-tim.com/now-ui-kit/nucleo-icons.html">Icons</a> </p>
                            <div class="row">
                              <?php foreach ($product_description as $key => $product_descriptions): ?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_product_description" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$product_descriptions->id?>" name="id" type="hidden">
                                    <input value="<?=$product_descriptions->tag?>" name="tag" type="hidden">
                                    <div class="item form-group">
                                      <label  for="name" >Title: </label>
                                         <input value="<?=$product_descriptions->title?>" class="form-control col-md-7 col-xs-12"  name="title" type="text">
                                      </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="item form-group">
                                          <label  for="name" >Section 1 Title: </label>
                                             <input value="<?=$product_descriptions->section1_title?>" class="form-control col-md-7 col-xs-12"  name="title1" type="text">
                                         </div>
                                         <div>
                                           <div>
                                             <img style="width:40%" class="view-image" src="<?php echo base_url();?>assets/product/img/product/<?=$product_descriptions->section1_image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1160px x 1040px</p>
                                           </div>
                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="image" name="image1" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>
                                         </div>
                                         <div class="item form-group">
                                           <label  for="name" >Section 1 Description </label>
                                              <textarea value="" class="form-control col-md-7 col-xs-12"  name="description1" type="text"> <?=$product_descriptions->section1_desc?> </textarea>
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 1 Keypoint 1: </label>
                                               <input value="<?=$product_descriptions->section1_keypoint1?>" class="form-control col-md-7 col-xs-12"  name="sec1-keypoint1" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 1 Keypoint 2: </label>
                                               <input value="<?=$product_descriptions->section1_keypoint2?>" class="form-control col-md-7 col-xs-12"  name="sec1-keypoint2" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 1 Keypoint 2: </label>
                                               <input value="<?=$product_descriptions->section1_keypoint3?>" class="form-control col-md-7 col-xs-12"  name="sec1-keypoint3" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 1 Icon 1: </label>
                                               <input value="<?=$product_descriptions->section1_icon1?>" class="form-control col-md-7 col-xs-12"  name="sec1-icon1" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 1 Icon 2: </label>
                                               <input value="<?=$product_descriptions->section1_icon2?>" class="form-control col-md-7 col-xs-12"  name="sec1-icon2" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 1 Icon 3: </label>
                                               <input value="<?=$product_descriptions->section1_icon3?>" class="form-control col-md-7 col-xs-12"  name="sec1-icon3" type="text">
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="item form-group">
                                          <label  for="name" >Section 2 Title: </label>
                                             <input value="<?=$product_descriptions->section1_title?>" class="form-control col-md-7 col-xs-12"  name="title2" type="text">
                                         </div>
                                         <div>
                                           <div>
                                             <img style="width:40%" class="view-image" src="<?php echo base_url();?>assets/product/img/product/<?=$product_descriptions->section2_image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1060px x 950px</p>
                                           </div>
                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="image" name="image2" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>
                                         </div>
                                         <div class="item form-group">
                                           <label  for="name" >Section 2 Description </label>
                                              <textarea value="" class="form-control col-md-7 col-xs-12"  name="description2" type="text"> <?=$product_descriptions->section2_desc?> </textarea>
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 2 Keypoint 1: </label>
                                               <input value="<?=$product_descriptions->section2_keypoint1?>" class="form-control col-md-7 col-xs-12"  name="sec2-keypoint1" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 2 Keypoint 2: </label>
                                               <input value="<?=$product_descriptions->section2_keypoint2?>" class="form-control col-md-7 col-xs-12"  name="sec2-keypoint2" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 2 Keypoint 2: </label>
                                               <input value="<?=$product_descriptions->section2_keypoint3?>" class="form-control col-md-7 col-xs-12"  name="sec2-keypoint3" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 2 Icon 1: </label>
                                               <input value="<?=$product_descriptions->section2_icon1?>" class="form-control col-md-7 col-xs-12"  name="sec2-icon1" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 2 Icon 2: </label>
                                               <input value="<?=$product_descriptions->section2_icon2?>" class="form-control col-md-7 col-xs-12"  name="sec2-icon2" type="text">
                                          </div>
                                          <div class="item form-group">
                                            <label  for="name" >Section 2 Icon 3: </label>
                                               <input value="<?=$product_descriptions->section2_icon3?>" class="form-control col-md-7 col-xs-12"  name="sec2-icon3" type="text">
                                          </div>
                                      </div>
                                      <input name="update" type="submit" class="btn btn-primary right" value="Update">

                                    </div>
                                  </form>
                                </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                              Videos
                            </a>
                          </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                          <div class="panel-body">
                            <div class="row">
                              <?php foreach ($testimonial as $key => $testimonial): ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_product_testimonial" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$testimonial->id?>" name="id" type="hidden">
                                    <input value="<?=$testimonial->tag?>" name="tag" type="hidden">

                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="item form-group">
                                          <label  for="name" >Title: </label>
                                             <input  class="form-control col-md-7 col-xs-12" value="<?=$testimonial->title?>" name="title" type="text">
                                         </div>
                                        <div class="item form-group">
                                          <label  for="name" >Description: </label>
                                             <input value="<?=$testimonial->description?>" class="form-control col-md-7 col-xs-12"  name="description" type="text">
                                         </div>
                                        <div class="item form-group">
                                          <label  for="name" >Video 1: </label>
                                             <input value="<?=$testimonial->video1?>" class="form-control col-md-7 col-xs-12"  name="video1" type="text">
                                         </div>
                                        <div class="item form-group">
                                          <label  for="name" >Video 2: </label>
                                             <input value="<?=$testimonial->video2?>" class="form-control col-md-7 col-xs-12"  name="video2" type="text">
                                         </div>
                                         <div class="">
                                           <p>Get the Youtube link eg:https://www.youtube.com/watch?v=AJtDXIazrMo</p>
                                           <p>Select Video Id: https://www.youtube.com/watch?v=<span class="blink" style="color:red; font-weight:bold">AJtDXIazrMo</span> </p>
                                         </div>
                                         <!-- <button onclick="delete_portfolio(<?=$key_points->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              <?php endforeach; ?>
                              <div class="col-md-4">
                                <hr><hr>
                                <iframe class="img-raised" width="100%" height="300" src="https://www.youtube.com/embed/<?=$testimonial->video1?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></a>
                              </div>
                              <div class="col-md-4">
                                <hr><hr>
                                <iframe class="img-raised" width="100%" height="300" src="https://www.youtube.com/embed/<?=$testimonial->video2?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                              Product List
                            </a>
                          </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                          <div class="panel-body">
                            <div class="row">
                              <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_titleimage" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?=$titleimage[1]->id?>">
                                <input type="hidden" name="tag" value="<?=$titleimage[1]->tag?>">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="item form-group">
                                      <label  for="name" >Title: </label>
                                         <input required class="form-control col-md-7 col-xs-12" value="<?=$titleimage[1]->title?>" name="title" type="text">
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="item form-group">
                                      <label  for="name" >Description: </label>
                                      <input  id="image" name="image" type="hidden" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                       <input required class="form-control col-md-7 col-xs-12" value="<?=$titleimage[1]->description?>" name="description" type="text">
                                     </div>
                                  </div>

                                </div>

                                <input name="update" type="submit" class="btn btn-primary right" value="Update">
                              </form>
                              <br><br>
                              <button data-toggle="modal" data-target=".product_list" class="btn btn-success btn-sm btn-block" type="button"> <i class="fa fa-plus" aria-hidden="true"></i> &ensp; Product List</button>
                              <div class="modal fade product_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                    <br>
                                    <h4 style="text-align:center">Create Product List</h4>
                                    <hr>

                                    <div class="container" style="width:90%">
                                      <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/create_product_list" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                        <input type="hidden" name="tag" value="<?=$product_imagelist[0]->tag?>">
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="item form-group">
                                              <label  for="name" >Title: </label>
                                                 <input required class="form-control col-md-7 col-xs-12"  name="title" type="text">
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div>
                                              <div class="item form-group">
                                                <label  for="name" >Image: </label>
                                                <input required id="image" name="image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                              </div>
                                            </div>

                                          </div>
                                        </div>

                                        <input name="update" type="submit" class="btn btn-primary right" value="Create">
                                      </form>
                                      <br>
                                    </div>

                                  </div>
                                </div>
                              </div>

                              <?php foreach ($product_imagelist as $key => $product_imagelists): ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_product_list" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$product_imagelists->id?>" name="id" type="hidden">
                                    <input value="<?=$product_imagelists->tag?>" name="tag" type="hidden">
                                    <div class="row">
                                      <div class="col-md-10">

                                        <div class="item form-group">
                                          <label  for="name" >Description: </label>
                                             <input value="<?=$product_imagelists->title?>" class="form-control col-md-7 col-xs-12"  name="title" type="text">
                                         </div>

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/product/img/overlays/<?=$product_imagelists->image?>" alt="vs" class="img-responsive">
                                             <p>Image size 270px x 350px</p>

                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="image" name="image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>
                                         </div>

                                         <button onclick="delete_product_list(<?=$product_imagelists->id?>)" type="button" class="btn btn-danger right">Delete</button>
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </div>

                                    </div>
                                  </form>
                                </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSecen">
                              Form
                            </a>
                          </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                          <div class="panel-body">
                            <div class="row">
                              <form method="post" name="slider1" action="<?php echo base_url();?>cms/product/Update_Product/update_titleimage" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?=$titleimage[2]->id?>">
                                <input type="hidden" name="tag" value="<?=$titleimage[2]->tag?>">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="item form-group">
                                      <label  for="name" >Title: </label>
                                         <input required class="form-control col-md-7 col-xs-12" value="<?=$titleimage[2]->title?>" name="title" type="text">
                                     </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="item form-group">
                                      <label  for="name" >Description: </label>
                                      <input  id="image" name="image" type="hidden" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">

                                         <input required class="form-control col-md-7 col-xs-12" value="<?=$titleimage[2]->description?>" name="description" type="text">
                                     </div>
                                  </div>
                                </div>

                                <input name="update" type="submit" class="btn btn-primary right" value="Update">
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div> <br><br>
        </div>
        <!-- /page content -->





        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <script type="text/javascript">
      document.getElementById('image').addEventListener('change', checkFile, false);

      function checkFile(e) {

          var file_list = e.target.files;

          for (var i = 0, file; file = file_list[i]; i++) {
              var sFileName = file.name;
              var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
              var iFileSize = file.size;
              var iConvert = (file.size / 10485760).toFixed(2);

              if (!(sFileExtension === "png" || sFileExtension === "jpg" || sFileExtension === "jpeg") || iFileSize > 10485760) {
                  txt = "File type : " + sFileExtension + "\n\n";
                  txt += "Size: " + iConvert + " MB \n\n";
                  txt += "Please make sure your file is in pdf or jpg format and less than 5 MB.\n\n";
                  alert(txt);
                  $("#image").val("");
              }
          }
      }
    </script>

    <script>
      function delete_product_list(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/product/Update_Product/delete_product_list?id='+id+'&tag=<?=$product_imagelist[0]->tag?>';
          }
      }
    </script>

  </body>
</html>
