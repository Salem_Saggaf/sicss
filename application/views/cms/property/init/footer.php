<!-- jQuery -->
<script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url();?>assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- jQuery custom content scroller -->
<script src="<?php echo base_url();?>assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- validator -->
<script src="<?php echo base_url();?>assets/vendors/validator/validator.js"></script>
<!-- File Uploader -->
<script src="<?php echo base_url();?>assets/vendors/file-uploader/js/fileinput.js" type="text/javascript"></script>

<!-- DateJS -->
<script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url();?>assets/build/js/custom.min.js"></script>
