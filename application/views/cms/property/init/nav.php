<?php $userdata = $this->session->userdata('user'); ?>

<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="index.html" class="site_title"><i class="fa fa-graduatcion-cap"><img src="<?php echo base_url();?>assets/build/images/pavertech-logo.png"  class="img-circle paver-logo"></i>  MCMS System</a>
      <!-- <a href="index.html" class="site_title"><i class="fa fa-graduation-cap"></i> <span><span class="yellow">Go</span><span class="blue">Study.</span><span class="red">My</span></span></a> -->
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
   <div class="profile clearfix">
     <div class="profile_pic">

       <?php if ($userdata != NULL): ?>
         <img src="<?php echo base_url();?>assets/build/images/img.jpg" alt="<?=$userdata[0]['username'] ?>" class="img-circle profile_img">
       <?php endif; ?>

     </div>
     <div class="profile_info">
       <span>Welcome,</span>
       <h2 class="username"><?=$userdata[0]['username'] ?></h2>
     </div>
   </div>
   <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">


          <!-- <li><a href="<?php echo base_url();?>cms/Manage_Content"><i class="fa fa-folder-open-o"></i>Manage Content</a></li> -->

        </ul>

        <ul class="nav side-menu">
          <li><a href="<?php echo base_url();?>cms/Home"><i class="fa fa-home"></i> Home</span></a></li>

          <li><a><i class="fa fa-edit"></i>Company Page<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?php echo base_url();?>cms/Manage_Leads">Manage Leads</a></li>
              <!-- <li><a href="<?php echo base_url();?>cms/Download_Report">Download Report</a></li> -->
              <li><a href="<?php echo base_url();?>cms/Manage_Content">Manage Content</a></li>
            </ul>
          </li>

          <li><a><i class="fa fa-tty"></i>Product Page <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                  <li><a href="<?php echo base_url();?>cms/product/Manage_Leads">Manage Leads</a></li>
                  <!-- <li><a href="<?php echo base_url();?>cms/product/Download_Report">Download Report</a></li> -->
                  <li><a href="<?php echo base_url();?>cms/product/Manage_Product">Manage Product</a></li>
            </ul>
          </li>
          <!-- <li><a href="<?php echo base_url();?>cms/product/Manage_Product"><i class="fa fa-edit"></i> Manage Product </a></li> -->
          <li><a href="<?php echo base_url();?>cms/Download_Report"><i class="fa fa-download"></i> Download Report </a></li>

          <li><a href="<?php echo base_url();?>cms/Login/change_password"><i class="fa fa-key"></i> Change Password </a></li>

          <li><a href="<?php echo base_url();?>cms/Home/logout"><i class="fa fa-sign-out"></i>Logout </a></li>

        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="home">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Application Status">
        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Change Password">
        <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>cms/home/logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>


<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="username">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">


            <?php if ($userdata != NULL): ?>
              <img src="<?php echo base_url();?>assets/build/images/img.jpg" alt=""><?=$userdata[0]['username'].'&nbsp;&nbsp;' ?>
            <?php endif; ?>




            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="<?php echo base_url();?>cms/home"> <i class="fa fa-user-o pull-right"></i> Profile</a></li>
            <li><a href="javascript:;">Help</a></li>
            <li><a href="<?php echo base_url();?>cms/home/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->
