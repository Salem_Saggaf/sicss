<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>User Login</title>
  <link rel="shortcut icon" href="<?=base_url()?>assets/qingfeng/images/logo/qingfeng.png">

  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">

  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans'>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/build/css/login.css">


</head>

<body>
  <div class="cont" style="background-image: url(<?php echo base_url();?>assets/build/images/background.png);">
  <div class="demo">

    <div class="login">
      <div class="login-img"><img  src="<?php echo base_url();?>assets/qingfeng/images/logo/qingfeng.png" style="width:90%; top: -30px; position:relative" alt="QingFeng"></div>
      <div class="login__form">
        <form class="" action="<?php echo base_url();?>cms/Login/logindata" method="post">
          <div class="login__row">
            <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
              <path d="M17.388,4.751H2.613c-0.213,0-0.389,0.175-0.389,0.389v9.72c0,0.216,0.175,0.389,0.389,0.389h14.775c0.214,0,0.389-0.173,0.389-0.389v-9.72C17.776,4.926,17.602,4.751,17.388,4.751 M16.448,5.53L10,11.984L3.552,5.53H16.448zM3.002,6.081l3.921,3.925l-3.921,3.925V6.081z M3.56,14.471l3.914-3.916l2.253,2.253c0.153,0.153,0.395,0.153,0.548,0l2.253-2.253l3.913,3.916H3.56z M16.999,13.931l-3.921-3.925l3.921-3.925V13.931z"></path>
            </svg>
            <input type="email" name="email" class="login__input name" placeholder="email" required/>
          </div>
          <div class="login__row">
            <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
              <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
            </svg>
            <input type="password" name="password" class="login__input pass" placeholder="Password" required/>
          </div>
          <!-- <button type="button" class="login__submit">Sign in</button> -->
          <input type="submit"  class="login__submit" name="login" value="Sign In">
        </form>
        <p class="login__signup">Don't have an account? &nbsp;<a class="sigup-page">Sign Up</a></p>
        <!-- <p class="login__signup"> <a href="<?php echo base_url();?>application-portal/Login/reset_password_email">Forget Password?</a></p> -->
      </div>
    </div>

    <div class="app">
      <div class="app__top">
        <div><img  src="<?php echo base_url();?>assets/qingfeng/images/logo/qingfeng.png" style="width:80%;   position: absolute; left: 10%; top:15%" alt="QingFeng"></div>

        </div>

        <div class="login__form" style="margin-top:-50px;">
          <form class="" action="<?php echo base_url();?>cms/Login/signup" method="post">
            <div class="login__row">
              <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
							<path d="M17.388,4.751H2.613c-0.213,0-0.389,0.175-0.389,0.389v9.72c0,0.216,0.175,0.389,0.389,0.389h14.775c0.214,0,0.389-0.173,0.389-0.389v-9.72C17.776,4.926,17.602,4.751,17.388,4.751 M16.448,5.53L10,11.984L3.552,5.53H16.448zM3.002,6.081l3.921,3.925l-3.921,3.925V6.081z M3.56,14.471l3.914-3.916l2.253,2.253c0.153,0.153,0.395,0.153,0.548,0l2.253-2.253l3.913,3.916H3.56z M16.999,13.931l-3.921-3.925l3.921-3.925V13.931z"></path>
						</svg>
              <input type="email" name="email" class="login__input name" placeholder="email" required/>
            </div>
            <div class="login__row">
              <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8"/>
              </svg>
              <input type="text" name="username" class="login__input name" placeholder="username" required/>
            </div>
            <div class="login__row">
              <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
              </svg>
              <input type="password" name="password" class="login__input pass" placeholder="Password" required/>
            </div>
            <!-- <button type="button" class="login__submit">Sign in</button> -->
            <input type="submit" class="login__submit" value="Create Account">
            <p class="login__signup">Already have an account? &nbsp;<a class="app__logout">Sign In</a></p>
          </form>
        </div>
      </div>
      <!-- <div class="app__logout">
        <svg class="app__logout-icon svg-icon" viewBox="0 0 20 20">
          <path d="M6,3 a8,8 0 1,0 8,0 M10,0 10,12"/>
        </svg>
      </div> -->
    </div>
  </div>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script src="<?php echo base_url();?>assets/build/js/login.js"></script>
  <script type="text/javascript">
  // alert(<?=$error?>);
    var error = <?=$error?>;
    if (error == true) {
      alert('email is exist, please try another email');
    }
  </script>

  <script type="text/javascript">
  // alert(<?=$login?>);
    var login = <?=$login?>;
    if (login == false) {
      alert('wrong credential');
    }
  </script>


</body>
</html>
