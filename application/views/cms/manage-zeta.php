<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Update <small>Zeta</small> </h3>
              </div>

              <div class="title_right">

              </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Background Banner
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-xs-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalBanner">
                                  Add
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="addModalBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Image To Banner</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          
                              
                                            <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/add_image_banner" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                              <input value="" name="id" type="hidden">



                                                   <div>


                                                     <div class="item form-group">
                                                       <label  for="name" >Image: </label>
                                                       <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                                     </div>

                                                      <div class="item form-group">
                                                        <label  for="name" >Title: </label>
                                                           <input  class="form-control col-md-7 col-xs-12" value="" name="Title" type="text">
                                                       </div>

                                                      <div class="item form-group">
                                                        <label  for="name" >Subtitle: </label>
                                                           <input  class="form-control col-md-7 col-xs-12" value="" name="SubTitle" type="text">
                                                       </div>


                                                   </div>

                                                   <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                                   <input name="add" type="submit" class="btn btn-primary right" value="Add">

                                            </form>
                                        
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">

                              <?php foreach ($banner as $key => $banners): ?>

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Astetika/update_astetika_banner" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$banners->id?>" name="id" type="hidden">
                                    <div class="row">
                                      <div class="col-md-10">

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/propertygallery/images/Property-Gallery/astetica-residences/<?=$banners->Image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1800px x 1200px</p>
                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>

                                            <div class="item form-group">
                                              <label  for="name" >Title: </label>
                                                 <input  class="form-control col-md-7 col-xs-12" value="<?=$banners->Title?>" name="Title" type="text">
                                             </div>

                                            <div class="item form-group">
                                              <label  for="name" >Subtitle: </label>
                                                 <input  class="form-control col-md-7 col-xs-12" value="<?=$banners->SubTitle?>" name="SubTitle" type="text">
                                             </div>


                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                  </form>
                                  <div class="col-xs-6"></div>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalBanner<?=$banners->id?>">
                                      Delete
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModalBanner<?=$banners->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <p>Are You Sure You Want To Delete <?=$banners->Title?></p>
                                          </div>
                                          <div class="modal-footer">
                                            <div class="container">
                                              <div class="row">
                                                <div class="col-xs-2">
                                                  <button type="button " class="btn btn-primary" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="col-xs-1">
                                                  <form method="get" name="slider1" action="<?php echo base_url();?>cms/Manage_Astetika/delete_astetika_banner" class="form-horizontal form-label-left">
                                                    <input type="hidden" name="id" value="<?=$banners->id?>">
                                                    <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                                                  </form>
                                                </div>
                                                
                                              </div>
                                            </div>


                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                  
                              </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                              Section 1
                            </a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">

                            <div class="row">
                              <div class="col-xs-12">
                              
                              <?php foreach ($sectionInfo as $key => $sections): ?>
                                <?php if($sections->section == 1): ?>

                                <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/update_zeta_section_info" class="form-horizontal form-label-left">
                                  
                                    <div class="form-group">
                                      <label for="title">Title :</label>
                                      <input type="text" class="form-control" id="title"  value=" <?=$sections->Title ?> ">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="sub">Sub Title :</label>
                                      <input type="text" class="form-control" id="sub"  value=" <?=$sections->SubTitle ?> ">
                                    </div>

                                    <div class="form-group">
                                      <label for="desc">Description :</label>
                                      <textarea  class="form-control" id="desc" rows="5"><?=$sections->Description ?></textarea> 
                                    </div>
                                

                                  <input type="submit" name="submit" value="Upadte" class="btn btn-success">
                                </form>
                                <?php endif; ?>
                              <?php endforeach ?>
                              </div>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-xs-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalSection1">
                                  Add Images
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="addModalSection1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Image To Banner</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          
                              
                                            <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/add_zeta_section_image" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                              <input value="" name="id" type="hidden">
                                              <input type="hidden" name="section" value="1">


                                                   <div>


                                                     <div class="item form-group">
                                                       <label  for="name" >Image: </label>
                                                       <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                                     </div>



                                                   </div>

                                                   <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                                   <input name="add" type="submit" class="btn btn-primary right" value="Add">

                                            </form>
                                        
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                              <hr>
                              <div class="row">
                                
                              
                              <?php foreach ($sectionImage as $key => $sectionImages): ?>
                                <?php if($sectionImages->Section == 1): ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/update_zeta_section_image" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$sectionImages->id?>" name="id" type="hidden">

                                    
                                      

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/propertygallery/images/Property-Gallery/astetica-residences/<?=$sectionImages->Image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1800px x 1200px</p>
                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>



                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </form>
                                      <div class="col-xs-6"></div>
                                      <!-- Button trigger modal -->
                                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalProperty1<?=$sectionImages->id?>">
                                        Delete
                                      </button>

                                      <!-- Modal -->
                                      <div class="modal fade" id="deleteModalProperty1<?=$sectionImages->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                              <p>Are You Sure You Want To Delete <?=$sectionImages->Image?></p>
                                            </div>
                                            <div class="modal-footer">
                                              <div class="container">
                                                <div class="row">
                                                  <div class="col-xs-2">
                                                    <button type="button " class="btn btn-primary" data-dismiss="modal">Close</button>
                                                  </div>
                                                  <div class="col-xs-1">
                                                    <form method="get" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/delete_zeta_section_image" class="form-horizontal form-label-left">
                                                      <input type="hidden" name="id" value="<?=$sectionImages->id?>">
                                                      <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                                                    </form>
                                                  </div>
                                                  
                                                </div>
                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                              <?php endif; ?>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                              Section 2
                            </a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">

                            <div class="row">
                              <div class="col-xs-12">
                              
                              <?php foreach ($sectionInfo as $key => $sections): ?>
                                <?php if($sections->section == 2): ?>

                                <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/update_zeta_section_info" class="form-horizontal form-label-left">
                                  
                                    <div class="form-group">
                                      <label for="title">Title :</label>
                                      <input type="text" class="form-control" id="title"  value=" <?=$sections->Title ?> ">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="sub">Sub Title :</label>
                                      <input type="text" class="form-control" id="sub"  value=" <?=$sections->SubTitle ?> ">
                                    </div>

                                    <div class="form-group">
                                      <label for="desc">Description :</label>
                                      <textarea  class="form-control" id="desc" rows="5"><?=$sections->Description ?></textarea> 
                                    </div>
                                

                                  <input type="submit" name="submit" value="Upadte" class="btn btn-success">
                                </form>
                                <?php endif; ?>
                              <?php endforeach ?>
                              </div>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-xs-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalSection2">
                                  Add Images
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="addModalSection2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Image To Banner</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          
                              
                                            <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/add_zeta_section_image" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                              <input value="" name="id" type="hidden">
                                              <input type="hidden" name="section" value="2">


                                                   <div>


                                                     <div class="item form-group">
                                                       <label  for="name" >Image: </label>
                                                       <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                                     </div>



                                                   </div>

                                                   <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                                   <input name="add" type="submit" class="btn btn-primary right" value="Add">

                                            </form>
                                        
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                              <hr>
                              <div class="row">
                                
                              
                              <?php foreach ($sectionImage as $key => $sectionImages): ?>
                                <?php if($sectionImages->Section == 2): ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/update_zeta_section_image" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$sectionImages->id?>" name="id" type="hidden">

                                    
                                      

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/propertygallery/images/Property-Gallery/astetica-residences/<?=$sectionImages->Image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1800px x 1200px</p>
                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>



                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </form>
                                      <div class="col-xs-6"></div>
                                      <!-- Button trigger modal -->
                                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalProperty1<?=$sectionImages->id?>">
                                        Delete
                                      </button>

                                      <!-- Modal -->
                                      <div class="modal fade" id="deleteModalProperty1<?=$sectionImages->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                              <p>Are You Sure You Want To Delete <?=$sectionImages->Image?></p>
                                            </div>
                                            <div class="modal-footer">
                                              <div class="container">
                                                <div class="row">
                                                  <div class="col-xs-2">
                                                    <button type="button " class="btn btn-primary" data-dismiss="modal">Close</button>
                                                  </div>
                                                  <div class="col-xs-1">
                                                    <form method="get" name="slider1" action="<?php echo base_url();?>cms/Manage_Zeta/delete_zeta_section_image" class="form-horizontal form-label-left">
                                                      <input type="hidden" name="id" value="<?=$sectionImages->id?>">
                                                      <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                                                    </form>
                                                  </div>
                                                  
                                                </div>
                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                              <?php endif; ?>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>

        <!-- /page content -->





        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <script type="text/javascript">
      document.getElementById('Image').addEventListener('change', checkFile, false);

      function checkFile(e) {

          var file_list = e.target.files;

          for (var i = 0, file; file = file_list[i]; i++) {
              var sFileName = file.name;
              var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
              var iFileSize = file.size;
              var iConvert = (file.size / 5242880).toFixed(2);
              // alert(sFileExtension);
              if (!(sFileExtension === "png" || sFileExtension === "jpg" || sFileExtension === "jpeg") || iFileSize <= 5242880) {
                  txt = "File type : " + sFileExtension + "\n\n";
                  txt += "Size: " + iConvert + " MB \n\n";
                  txt += "Please make sure your file is in pdf or jpg format and less than 5 MB.\n\n";
                  alert(txt);
                  $("#Image").val("");
              }
          }
      }
    </script>

    <script>
      function delete_banner(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/product/Manage_Content/delete_portfolio?id='+id;
          }
      }
    </script>
    <script>
      function delete_product_list(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/Manage_Content/delete_product_list?id='+id;
          }
      }
    </script>
    <script>
      function delete_testimonial(id) {
           var r = confirm("Do you want to delete Id: " +id);
          if (r == true) {
              window.location = '<?php echo base_url();?>cms/Manage_Content/delete_testimonial?id='+id;
          }
      }
    </script>

  </body>
</html>
