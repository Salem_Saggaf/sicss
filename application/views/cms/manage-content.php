﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Update <small>Content</small> </h3>
              </div>

              <div class="title_right">

              </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Banner
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-xs-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalBanner">
                                  Add
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="addModalBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Image To Banner</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          
                              
                                            <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/add_image_banner" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                              <input value="" name="id" type="hidden">



                                                   <div>


                                                     <div class="item form-group">
                                                       <label  for="name" >Image: </label>
                                                       <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                                     </div>

                                                      <div class="item form-group">
                                                        <label  for="name" >Title: </label>
                                                           <input  class="form-control col-md-7 col-xs-12" value="" name="Title" type="text">
                                                       </div>

                                                      <div class="item form-group">
                                                        <label  for="name" >Subtitle: </label>
                                                           <input  class="form-control col-md-7 col-xs-12" value="" name="SubTitle" type="text">
                                                       </div>


                                                   </div>

                                                   <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                                   <input name="add" type="submit" class="btn btn-primary right" value="Add">

                                            </form>
                                        
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">

                              <?php foreach ($banner as $key => $banners): ?>

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/update_banner" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$banners->id?>" name="id" type="hidden">
                                    <div class="row">
                                      <div class="col-md-10">

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/propertygallery/images/Property-Gallery/astetica-residences/<?=$banners->Image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1800px x 1200px</p>
                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>

                                            <div class="item form-group">
                                              <label  for="name" >Title: </label>
                                                 <input  class="form-control col-md-7 col-xs-12" value="<?=$banners->Title?>" name="Title" type="text">
                                             </div>

                                            <div class="item form-group">
                                              <label  for="name" >Subtitle: </label>
                                                 <input  class="form-control col-md-7 col-xs-12" value="<?=$banners->SubTitle?>" name="SubTitle" type="text">
                                             </div>


                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                  </form>
                                  <div class="col-xs-6"></div>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalBanner<?=$banners->id?>">
                                      Delete
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModalBanner<?=$banners->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <p>Are You Sure You Want To Delete <?=$banners->Title?></p>
                                          </div>
                                          <div class="modal-footer">
                                            <div class="container">
                                              <div class="row">
                                                <div class="col-xs-2">
                                                  <button type="button " class="btn btn-primary" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="col-xs-1">
                                                  <form method="get" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/delete_banner" class="form-horizontal form-label-left">
                                                    <input type="hidden" name="id" value="<?=$banners->id?>">
                                                    <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                                                  </form>
                                                </div>
                                                
                                              </div>
                                            </div>


                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                  
                              </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                              Property 1
                            </a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-xs-2">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalProperty1">
                                  Add
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="addModalProperty1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Image To Property 1</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          
                              
                                            <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/add_property1" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                              <input value="" name="id" type="hidden">



                                                   <div>


                                                     <div class="item form-group">
                                                       <label  for="name" >Image: </label>
                                                       <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                                     </div>




                                                   </div>

                                                   <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                                   <input name="add" type="submit" class="btn btn-primary right" value="Add">

                                            </form>
                                        
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <?php foreach ($property1 as $key => $property): ?>

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/update_property1" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$property->id?>" name="id" type="hidden">

                                    <div class="row">
                                      <div class="col-md-10">

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/propertygallery/images/Property-Gallery/astetica-residences/<?=$property->Image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1800px x 1200px</p>
                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>



                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                      </form>
                                      <div class="col-xs-6"></div>
                                      <!-- Button trigger modal -->
                                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalProperty1<?=$property->id?>">
                                        Delete
                                      </button>

                                      <!-- Modal -->
                                      <div class="modal fade" id="deleteModalProperty1<?=$property->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                              <p>Are You Sure You Want To Delete <?=$property->Image?></p>
                                            </div>
                                            <div class="modal-footer">
                                              <div class="container">
                                                <div class="row">
                                                  <div class="col-xs-2">
                                                    <button type="button " class="btn btn-primary" data-dismiss="modal">Close</button>
                                                  </div>
                                                  <div class="col-xs-1">
                                                    <form method="get" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/delete_property1" class="form-horizontal form-label-left">
                                                      <input type="hidden" name="id" value="<?=$property->id?>">
                                                      <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                                                    </form>
                                                  </div>
                                                  
                                                </div>
                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  
                                </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                              Property 2
                            </a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-xs-1">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModalProperty2">
                                  Add
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="addModalProperty2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Image To Property 1</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                          
                              
                                            <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/add_property2" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                              <input value="" name="id" type="hidden">



                                                   <div>


                                                     <div class="item form-group">
                                                       <label  for="name" >Image: </label>
                                                       <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                                     </div>




                                                   </div>

                                                   <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                                   <input name="add" type="submit" class="btn btn-primary right" value="Add">

                                            </form>
                                        
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                            <div class="row">
                              <?php foreach ($property2 as $key => $property): ?>

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                   <hr> <hr>
                                  <form method="post" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/update_property2" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">
                                    <input value="<?=$property->id?>" name="id" type="hidden">

                                    <div class="row">
                                      <div class="col-md-10">

                                         <div>
                                           <div>
                                             <img class="view-image" src="<?php echo base_url();?>assets/propertygallery/images/Property-Gallery/astetica-residences/<?=$property->Image?>" alt="vs" class="img-responsive">
                                             <p>Image size 1800px x 1200px</p>
                                           </div>

                                           <div class="item form-group">
                                             <label  for="name" >Image: </label>
                                             <input id="Image" name="Image" type="file" class="file" multiple data-show-upload="false" data-show-caption="true"  onchange="checkFile()">
                                           </div>



                                         </div>

                                         <!-- <button onclick="delete_banner(<?=$banners->id?>)" type="button" class="btn btn-danger right">Delete</button> -->
                                         <input name="update" type="submit" class="btn btn-primary right" value="Update">
                                       </form>
                                        <div class="col-xs-6"></div>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalProperty2<?=$property->id?>">
                                          Delete
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="deleteModalProperty2<?=$property->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <p>Are You Sure You Want To Delete <?=$property->Image?></p>
                                              </div>
                                              <div class="modal-footer">
                                                <div class="container">
                                                  <div class="row">
                                                    <div class="col-xs-2">
                                                      <button type="button " class="btn btn-primary" data-dismiss="modal">Close</button>
                                                    </div>
                                                    <div class="col-xs-1">
                                                      <form method="get" name="slider1" action="<?php echo base_url();?>cms/Manage_Content/delete_property2" class="form-horizontal form-label-left">
                                                        <input type="hidden" name="id" value="<?=$property->id?>">
                                                        <input type="submit" name="delete" class="btn btn-danger" value="Delete">
                                                      </form>
                                                    </div>
                                                    
                                                  </div>
                                                </div>


                                              </div>
                                            </div>
                                          </div>
                                        </div>                                       
                                      </div>
                                    </div>
                                  
                                </div>
                              <?php endforeach; ?>
                            </div>
                          </div>
                        </div>
                      </div>                                            

                      </div>


  
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div> <br><br>
        </div>
        <!-- /page content -->





        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <script type="text/javascript">
      document.getElementById('Image').addEventListener('change', checkFile, false);

      function checkFile(e) {

          var file_list = e.target.files;

          for (var i = 0, file; file = file_list[i]; i++) {
              var sFileName = file.name;
              var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
              var iFileSize = file.size;
              var iConvert = (file.size / 5242880).toFixed(2);
              // alert(sFileExtension);
              if (!(sFileExtension === "png" || sFileExtension === "jpg" || sFileExtension === "jpeg") || iFileSize > 5242880) {
                  txt = "File type : " + sFileExtension + "\n\n";
                  txt += "Size: " + iConvert + " MB \n\n";
                  txt += "Please make sure your file is in pdf or jpg format and less than 5 MB.\n\n";
                  alert(txt);
                  $("#Image").val("");
              }
          }
      }
    </script>

  </body>
</html>
