<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('init/head.php'); ?>
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">

        <?php include('init/nav.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Update <small>Password</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                    <form method="post" action="<?php echo base_url();?>cms/Login/update_password" class="form-horizontal form-label-left" novalidate enctype="multipart/form-data">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" >Old Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" name="old-password" placeholder="Old Password" required="required" type="password">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="password" class="control-label col-md-3">Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password" type="password" name="password" placeholder="New Password" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password2" type="password" name="password2" data-validate-linked="password" placeholder="Repeat Password" class="form-control col-md-7 col-xs-12" required="required">
                        </div>
                      </div>

                      <input name="" type="submit" class="btn btn-primary right" value="Submit">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © Copyright 2017. Paver Tech Sdn. Bhd., All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('init/footer.php'); ?>

    <script type="text/javascript">
      document.getElementById('offer_letter').addEventListener('change', checkFile, false);
      medical_report.addEventListener('change', checkFile, false);
      no_obj_certificate.addEventListener('change', checkFile, false);

      function checkFile(e) {

          var file_list = e.target.files;

          for (var i = 0, file; file = file_list[i]; i++) {
              var sFileName = file.name;
              var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
              var iFileSize = file.size;
              var iConvert = (file.size / 10485760).toFixed(2);

              if (!(sFileExtension === "pdf" || sFileExtension === "jpg" || sFileExtension === "jpeg") || iFileSize > 10485760) {
                  txt = "File type : " + sFileExtension + "\n\n";
                  txt += "Size: " + iConvert + " MB \n\n";
                  txt += "Please make sure your file is in pdf or jpg format and less than 10 MB.\n\n";
                  alert(txt);
                  $("#offer_letter").val("");
                  $("#medical_report").val("");
                  $("#no_obj_certificate").val("");

              }
          }
      }
    </script>

  </body>
</html>
