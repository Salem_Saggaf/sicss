<!DOCTYPE html>
<html lang="en">
<!--
	Bent - Bootstrap Landing Page Template by Dcrazed
	Site: https://dcrazed.com/bent-app-landing-page-template/
	Free for personal and commercial use under GNU GPL 3.0 license.
-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="5;url=<?php echo base_url();?>cms/login" />

    <title>Welcome to  Paver Tech MCMS System</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/build/images/favicon.png" type="image/x-icon">
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/welcome/css/bootstrap.min.css" rel="stylesheet">

    <!-- Owl Carousel Assets -->
    <link href="<?php echo base_url();?>assets/welcome/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/welcome/css/owl.theme.css" rel="stylesheet">


    <!-- Pixeden Icon Font -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/welcome/css/Pe-icon-7-stroke.css">

    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/welcome/css/font-awesome.min.css" rel="stylesheet">


    <!-- PrettyPhoto -->
    <link href="<?php echo base_url();?>assets/welcome/css/prettyPhoto.css" rel="stylesheet">


    <!-- Style -->
    <link href="<?php echo base_url();?>assets/welcome/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/welcome/css/animate.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="<?php echo base_url();?>assets/welcome/css/responsive.css" rel="stylesheet">
    <style type="text/css">
      body {
          overflow:hidden;
      }
    </style>
</head>

<body>
    <!-- PRELOADER -->
    <div class="spn_hol">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

 <!-- END PRELOADER -->

 <!-- =========================
     START ABOUT US SECTION
============================== -->
    <section class="header parallax home-parallax page" id="HOME">
        <h2></h2>
        <div class="section_overlay">

          <div class="">
            <img class="loading" width="100" height="100" src="<?php echo base_url();?>assets/welcome/images/loading.gif" alt="" class="wow fadeIn" data-wow-duration="10s" data-wow-delay=".10s">
          </div>
            <div class="container home-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center">
                                <!-- LOGO -->
                            <img width="300" height="55" src="<?php echo base_url();?>assets/welcome/images/logo.png" alt="" class="wow fadeIn" data-wow-duration="10s" data-wow-delay=".10s">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".10s">
                        <div class="home_text">
                            <!-- TITLE AND DESC -->
                            <h1>Welcome to  Paver Tech MCMS System</h1>
                            <p>You will be auto directed to login page shortly....</p>

                            <div class="download-btn">
                            <!-- BUTTON -->
                            <p>If you are not auto directed... <a class="btn home-btn wow fadeInLeft" href="<?php echo base_url();?>cms/login">Click here  </a></p>


                         </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-6 col-sm-4">
                        <div class="home-iphone">
                            <img src="<?php echo base_url();?>assets/welcome/images/logo.png" alt="" class="wow fadeIn" data-wow-duration="10s" data-wow-delay=".10s">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END HEADER SECTION -->


<!-- =========================
     SCRIPTS
============================== -->



    <script src="<?php echo base_url();?>assets/welcome/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/owl.carousel.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/jquery.fitvids.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/jquery.ajaxchimp.langs.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assets/welcome/js/script.js"></script>


      <?php if ($status == 'Error'): ?>
       <script type="text/javascript">
         alert('You account had been activated already');
         window.location.href = "<?php echo base_url();?>cms/login";
       </script>
       ec
     <?php endif; ?>


</body>
</html>
