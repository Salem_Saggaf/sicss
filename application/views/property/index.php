<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="<?=base_url()?>assets/qingfeng/images/logo/qingfeng.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?=$tag?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="stylesheet" href="<?=base_url()?>assets/product/css/main.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/product/css/custom.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/product/css/responsive.css">
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?=base_url()?>assets/product/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/product/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?=base_url()?>assets/product/css/demo.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/product/css/style.css" rel="stylesheet" />


    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K9B9JSH');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109323197-3"></script>
    	<script>
  	window.dataLayer = window.dataLayer || [];
  	function gtag(){dataLayer.push(arguments);}
  	gtag('js', new Date());

  	gtag('config', 'UA-109323197-3');
	</script>

    <!-- WhatsHelp.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                facebook: "1607420519516232", // Facebook page ID
                whatsapp: "+86(138)26202597", // WhatsApp number
                company_logo_url: "//storage.whatshelp.io/widget/c5/c5ad/c5ad7b8d03fef8626e0728abb64cedcc/11694867_839512182751932_3099601079423646598_n.jpg", // URL of company logo (png, jpg, gif)
                greeting_message: "Hi, wanted to know more about Arcade Machines...", // Text of greeting message
                call_to_action: "Message us", // Call to action
                button_color: "#FF6550", // Color of button
                position: "right", // Position may be 'right' or 'left'
                order: "facebook,whatsapp" // Order of buttons
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /WhatsHelp.io widget -->

</head>

<body class="index-page sidebar-collapse">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9B9JSH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div class="wrapper overflow-x">
        <div class="page-header clear-filter  " filter-color="orsange">
            <!-- <div class="page-header-image" data-parallax="true" style="background-image: url('<?=base_url()?>assets/product/img/headver.jpg');"></div> -->

            <div class="row">
              <div class="col-md-8 col-xs-12">
                <div class="container">
                    <div class="content-center brand">
                      <div class="avatar">
                        <img class="main-image" src="<?=base_url()?>assets/product/img/<?=$banner[0]->image?>" alt="" sty>
                      </div>
                         <!-- <div class="main-image"   style="background-image: url('<?=base_url()?>assets/product/img/main-image.png');">d</div> -->
                    </div>
                </div>
              </div>
              <div class="col-md-4 col-xs-12 title-text">
                <div class="container mobile-logo-container">
                    <div class="content-center brand">
                        <img class="n-logo" src="<?=base_url()?>assets/product/img/qingfeng.png" alt="">
                        <h1 class="h1-seo"><?=$banner[0]->title?> </h1>
                        <h3><?=$banner[0]->description?></h3>
                    </div>
                </div>
              </div>
            </div>
        </div>
        <div class="main">
            <!-- <div class="section section-images">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hero-images-container">
                                <img src="assets/img/hero-image-1.png" alt="">
                            </div>
                            <div class="hero-images-container-1">
                                <img src="assets/img/hero-image-2.png" alt="">
                            </div>
                            <div class="hero-images-container-2">
                                <img src="assets/img/hero-image-3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="container section section-features">
              <h3 class="title"><?=$titleimage[0]->title?></h3>

              <div class="row">

                <div class="col-md-4 col-xs-4">
                  <div class="center">
                     <i class="icon now-ui-icons <?=$feature[0]->icon?>"></i>
                     <h2><?=$feature[0]->title?></h2>
                     <p><?=$feature[0]->description?></p>
                   </div>
                  <div class="center margin-top">
                     <i class="icon now-ui-icons <?=$feature[1]->icon?>"></i>
                     <h2><?=$feature[1]->title?></h2>
                     <p><?=$feature[1]->description?></p>
                   </div>
                  <div class="center margin-top">
                     <i class="icon now-ui-icons <?=$feature[2]->icon?>"></i>
                     <h2><?=$feature[2]->title?></h2>
                     <p><?=$feature[2]->description?></p>
                   </div>
                </div>
                <div class="col-md-4 col-xs-4">
                  <div class="avatar">
                    <img class="product-image" src="<?=base_url()?>assets/product/img/<?=$titleimage[0]->image?>" alt="" sty>
                  </div>
                </div>
                <div class="col-md-4 col-xs-4">
                  <div class="center">
                     <i class="icon now-ui-icons <?=$feature[3]->icon?>"></i>
                     <h2><?=$feature[3]->title?></h2>
                     <p><?=$feature[3]->description?></p>
                   </div>
                  <div class="center margin-top">
                     <i class="icon now-ui-icons <?=$feature[4]->icon?>"></i>
                     <h2><?=$feature[4]->title?></h2>
                     <p><?=$feature[4]->description?></p>
                   </div>
                  <div class="center margin-top">
                     <i class="icon now-ui-icons <?=$feature[5]->icon?>"></i>
                     <h2><?=$feature[5]->title?></h2>
                     <p><?=$feature[5]->description?></p>
                   </div>
                </div>
              </div>
            </div>

            <div class="section-image-desc">
              <h3 class="title"><?=$description[0]->title?></h3>

              <div class="">
                <div class="row">
                  <div class="col-md-6 col-sm-8">
                    <div class="image-1">
                    <div class="avatar">
                      <img src="<?=base_url()?>assets/product/img/product/<?=$description[0]->section1_image?>" alt="">
                   	</div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-4 ">
                    <div class="image-1-desc image-1-desc-cont">
                       <h2><?=$description[0]->section1_title?></h2>
                       <div class="line"></div>
                       <p><?=$description[0]->section1_desc?></p>
                       <br>
                       <p>- <?=$description[0]->section1_keypoint1?></p>
                       <p>- <?=$description[0]->section1_keypoint2?></p>
                       <p>- <?=$description[0]->section1_keypoint3?></p>
                       <ul>
                         <li><i class="icon now-ui-icons <?=$description[0]->section1_icon1?>"></i></li>
                         <li><i class="icon now-ui-icons <?=$description[0]->section1_icon2?>"></i></li>
                         <li><i class="icon now-ui-icons <?=$description[0]->section1_icon3?>"></i></li>
                       </ul>
                     </div>
                  </div>
                </div>

                <div class="row row-2">
                  <div class="col-md-6 col-sm-4">
                    <div class="image-1-desc image-2-desc">
                       <h2><?=$description[0]->section2_title?></h2>
                       <div class="line"></div>
                       <p><?=$description[0]->section2_desc?></p>
                       <br>
                       <p>- <?=$description[0]->section2_keypoint1?></p>
                       <p>- <?=$description[0]->section2_keypoint2?></p>
                       <p>- <?=$description[0]->section2_keypoint3?></p>
                       <ul>
                         <li><i class="icon now-ui-icons <?=$description[0]->section2_icon1?>"></i></li>
                         <li><i class="icon now-ui-icons <?=$description[0]->section2_icon2?>"></i></li>
                         <li><i class="icon now-ui-icons <?=$description[0]->section2_icon3?>"></i></li>
                       </ul>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-8">
                    <div class="image-2">
                      <div class="avatar">
                        <img src="<?=base_url()?>assets/product/img/product/<?=$description[0]->section2_image?>" alt="">
                     	</div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

            <!-- <div class="section section-signup" style="background-image: url('assets/img/bgs11.jpg'); background-size: cover; background-position: top center; min-height: 700px;">
                <div class="container">
                    <div class="row">
                        <div class="card card-signup" data-background-color="orange">
                          <iframe width="100%" height="315" src="https://www.youtube.com/embed/xkPZvGtFzcM?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                         </div>
                    </div>
                    <div class="col text-center">
                        <a href="examples/login-page.html" class="btn btn-simple btn-round btn-white btn-lg" target="_blank">View Login Page</a>
                    </div>
                </div>
            </div> -->
            <div class="section section-examples section-video" id="video">
              <h3 class="title"><?=$testimonial[0]->title?></h3>
              <p><?=$testimonial[0]->description?></p>

                <div class="space-50"></div>
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" target="_blank">
                              <iframe class="img-raised" width="100%" height="300" src="https://www.youtube.com/embed/<?=$testimonial[0]->video1?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" target="_blank">
                              <iframe class="img-raised" width="100%" height="300" src="https://www.youtube.com/embed/<?=$testimonial[0]->video2?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-signup" style="background-image: url('assets/img/bg11.jpg'); background-size: cover; background-position: top center; min-height: 700px;">
              <div class="container">
                <h3 class="title"><?=$titleimage[1]->title?></h3>
                <p><?=$titleimage[1]->description?></p>


                <div class="container-fluid">
                  <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
                      <div class="carousel-inner row w-100 mx-auto" role="listbox">
                        <?php $i = 0; ?>
                        <?php foreach ($imagelist as $key => $imagelists): ?>
                          <div class="carousel-item col-md-3 <?php if($i==0){echo"active";} ?>">
                            <div class="courses-column">
                              <div class="course-img">
                                <img src="<?=base_url()?>assets/product/img/overlays/<?=$imagelists->image?>" alt="">
                              </div>
                              <div class="courses-detail">
                                <div class="teacher-img"><img src="<?=base_url()?>assets/product/img/logo/qingfeng.jpg" alt=""></div>
                              </div>
                              <div class="course-btm">
                                <h5><?=$imagelists->title?></h5>
                              </div>
                            </div>
                          </div>
                          <?php $i++ ?>
                        <?php endforeach; ?>
                    </div>

                  </div>
                </div>
                <div class="next-button">
                   <a id="previous" class="cardousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                      <i class="fa fa-arrow-left" aria-hidden="true"></i>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a id="next" class="carousdel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                      <i class="fa fa-arrow-right" aria-hidden="true"></i>
                      <span class="sr-only">Next</span>
                  </a>
                </div>

              </div>
            </div>

            <div class="section section-contact-us text-center">
                <div class="container">
                    <h2 class="title"><?=$titleimage[2]->title?></h2>
                    <p class="description"><?=$titleimage[2]->description?></p><br><br>
                    <div class="row">
                        <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">
                            <form class="" action="<?=base_url()?>Product/insert_lead" method="post">
                              <input type="hidden" name="tag" value=<?=$tag?>>
                              <input type="hidden" name="page" value=<?=$page?>>
                              <div class="input-group input-lg">
                                <input required name="name" type="text" value="" placeholder="Full Name" class="form-control" />
                              </div>
                              <div class="input-group input-lg">
                                <input required name="email" type="email" value="" placeholder="Email Address" class="form-control" />
                              </div>
                              <div class="input-group input-lg">
                                <input required name="contact" type="text" value="" placeholder="Contact Number" class="form-control" />
                              </div>
                              <div class="textarea-container">
                                  <textarea class="form-control" name="message" rows="4" cols="80" placeholder="Type a message..."></textarea>
                              </div>
                              <div class="send-button">
                                  <input class="btn btn-primary btn-round btn-block btn-lg" type="submit" name="" value="Send Message">
                              </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Sart Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <h4 class="title title-up">Thank you!</h4>
                    </div>
                    <div class="modal-body">
                      <p>Your message has been submitted. Please allow us 24 working hours to revert back to you!.</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--  End Modal -->
        <!-- Mini Modal -->
        <div class="modal fade modal-mini modal-primary" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <div class="modal-profile">
                            <i class="now-ui-icons users_circle-08"></i>
                        </div>
                    </div>
                    <div class="modal-body">
                        <p>Always have an access to your profile</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link btn-neutral">Back</button>
                        <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--  End Modal -->
        <footer class="footer" data-background-color="black">
          <div class="container">
            &copy; 2017 <a target="_blank" href="http://www.gzqingfeng.com/" title="Guangzhou Qingfeng">Guangzhou Qingfeng</a>. All Rights Reserved.
           </div>
        </footer>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<?=base_url()?>assets/product/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/product/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/product/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?=base_url()?>assets/product/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?=base_url()?>assets/product/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="<?=base_url()?>assets/product/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<!-- <script src="<?=base_url()?>assets/product/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script> -->
<script src="<?=base_url()?>assets/product/js/style.js" type="text/javascript"></script>

<script type="text/javascript">
  $('#carouselExample').on('slide.bs.carousel', function (e) {

      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 4;
      var totalItems = $('.carousel-item').length;

      if (idx >= totalItems-(itemsPerSlide-1)) {
          var it = itemsPerSlide - (totalItems - idx);
          for (var i=0; i<it; i++) {
              // append slides to end
              if (e.direction=="left") {
                  $('.carousel-item').eq(i).appendTo('.carousel-inner');
              }
              else {
                  $('.carousel-item').eq(0).appendTo('.carousel-inner');
              }
          }
      }
  });

</script>
<?php if ($form_sub == "true"): ?>
      <script type="text/javascript">
          $(window).on('load',function(){
               $('#myModal').modal('show');
          });
       </script>
<?php endif; ?>


</html>
