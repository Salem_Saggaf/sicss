<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Index</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">
    
    <script type="text/javascript">
      jQuery(document).ready(function($){
        'use strict';
          jQuery('body').backstretch([
            "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
        ], {duration: 5000, fade: 500});
      });
    </script>

    <script type="text/javascript">
      $( document ).ready(function() {
        
        start(<?=$application[0]->status?>, <?=$application[0]->application_id?>);
      });
    </script>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
            <?php include('init/nav.php');?>
              
              <?php include('init/sideMenuBar.php'); ?>
            <div class="col-xs-9" >
            <form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep8" >
              <table class="table">
                <thead>
                  <tr>
                    <th class="col">*Family Members</th>
                    <th class="col">*Name</th>
                    <th class="col">*Phone Number</th>
                    <th class="col">Email</th>
                    <th class="col">Position</th>
                    <th class="col">Work Place</th>
                    <th class="col">Operation</th>
                  </tr>
                </thead>
                <tbody id="family_status">
                  <?php foreach($family_member as $newFamilyMember): ?>
                    <tr id="family_member<?=$newFamilyMember->family_member_id?>">
                      <th>
                        <select scope="row" class="form-control">
                          <option value="<?=$newFamilyMember->family_member_id?>"><?=$newFamilyMember->family_member_id?></option>
                          <option value="Father">Father</option>
                          <option value="Mother">Mother</option>
                          <option value="Spouse">Spouse</option>
                          <option value="Uncle">Uncle</option>
                          <option value="Brother">Brother</option>
                          <option value="Sister">Sister</option>
                          <option value="Other">Other</option>
                          <option value="Children">Children</option>
                        </select>                        
                      </th>
                      <th  id="family_field1"><?=$newFamilyMember->name?></th>
                      <th id="family_field2"><?=$newFamilyMember->phone_number?></th>
                      <th  id="family_field3"><?=$newFamilyMember->email?></th>
                      <th  id="family_field4"><?=$newFamilyMember->position?></th>
                      <th  id="family_field5"><?=$newFamilyMember->work_place?></th>
                      <th  id="family_field6"><button class="btn btn-danger" onclick="deleteRow('family_member<?=$newFamilyMember->family_member_id?>')">Delete</button></th>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
                
              </table>
                <button type="button" onclick="addFamilyStatus()" class="btn btn-default">Add</button>

              
              
                    <div class="col-xs-12">
                        <hr>
                          <h3>Financial Supporter</h3>
                        <hr>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="guarantor_name">*Guarantor Name </label>
                        <input type="text" name="guarantor_name" id="guarantor_name" class="form-control" value="<?php if($application != null): ?><?=$application[0]->guarantor_name?> <?php endif; ?>">
                      </div>     
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="guarantor_tel">The Guarantor Tel</label>
                        <input type="text" name="guarantor_tel" id="guarantor_tel" class="form-control" value="<?php if($application != null): ?><?=$application[0]->guarantor_tel?> <?php endif; ?>">
                      </div>                      
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="guarantor_address">The Guarantor Address</label>
                        <input type="text" name="guarantor_address" id="guarantor_address" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->guarantor_address?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="organization">Organization</label>
                        <input type="text" name="organization" id="organization" class="form-control" value="<?php if($application != null): ?><?=$application[0]->guarantor_orgnaization?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="gur_email">Email</label>
                        <input type="text" name="gur_email" id="gur_email" class="form-control" value="<?php if($application != null): ?><?=$application[0]->guarantor_email?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-12">
                        <hr>
                          <h3>Emergency Contact</h3>
                        <hr>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="emergency_name">Name</label>
                        <input type="text" name="emergency_name" id="emergency_name" class="form-control" value="<?php if($application != null): ?><?=$application[0]->emergency_contact_name?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="emergency_mobile">Mobile</label>
                        <input type="text" name="emergency_mobile" id="emergency_mobile" class="form-control" value="<?php if($application != null): ?><?=$application[0]->emergency_contact_mobile?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="emergency_phone_number">Phone Number</label>
                        <input type="text" name="emergency_phone_number" id="emergency_phone_number" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->emergency_contact_phone_number?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="emergency_email">Email</label>
                        <input type="text" name="emergency_email" id="emergency_email" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->emergency_contact_email?><?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="emergency_organization">Oragnization</label>
                        <input type="text" name="emergency_organization" id="emergency_organization" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->emergency_contact_orgnaization?><?php endif; ?>">
                      </div>     
                    </div> 

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="emergency_address">Address</label>
                        <input type="text" name="emergency_address" id="emergency_address" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->emergency_contact_address?><?php endif; ?>">
                      </div>     
                    </div>                     
                    <input type="hidden" name="id" value="<?=$application[0]->application_id?>">
                    <input type="hidden" name="status" value="<?=$application[0]->status?>">

                      <div class="col-xs-12">
                        <input type="submit" name="submit" value="Save and Next" class="btn btn-primary">
                      </div>

                    </div>     
              </form>
            </div>
          </div>
  




          </div>
        </div>
      </div>

      </section>
      <div class="footer-login center">
        <p>&copy; 2017 Study in China. All right reserved.</p>
      </div>
    </div>
    <script type="text/javascript">
           function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/tempData.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/userStatus.js"></script>

</body>
</html>
