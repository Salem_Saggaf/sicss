<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Index</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
  	 'use strict';
        	jQuery('body').backstretch([
            "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
  	        "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
  	        "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
  	    ], {duration: 5000, fade: 500});
      });
    </script>
    <script type="text/javascript">
      
      $( document ).ready(function() {
        
        start(<?=$application[0]->status?>,<?=$application[0]->application_id?>);
        isFileRequired("<?=$application[0]->photocopy_of_passport?>", "passport_copy");
        isFileRequired("<?=$application[0]->high_school_certificate?>", "high_school_certified");
        isFileRequired("<?=$application[0]->test_language_report?>", "english_test");
        isFileRequired("<?=$application[0]->physical_exam_test?>", "physical_test");
      });
    </script>    
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
          <?php include('init/nav.php');?>
              
              <?php include('init/sideMenuBar.php'); ?>
                <div class="col-xs-9" >
                  <form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep7" enctype="multipart/form-data">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="col">Year Attended <br> (From)</th>
                        <th class="col">Year Attended <br> (To)</th>
                        <th class="col">School Name </th>
                        <th class="col">Field of Study &amp; <br> Diploma received</th>
                        <th class="col">Operation</th>
                      </tr>
                    </thead>
                    <tbody id="educationl_background">
                      <?php foreach($educational_background as $newEducationalBackground): ?>
                        <tr id="educationl_field<?=$newEducationalBackground->educational_background_id?>">
                          <th scope="row"><?=$newEducationalBackground->year_attended_from?></th>
                          <th id="education_field2"><?=$newEducationalBackground->year_attended_to?></th>
                          <th id="education_field3"><?=$newEducationalBackground->school_name?></th>
                          <th id="education_field4"><?=$newEducationalBackground->field_of_study?></th>
                          <th id="education_field5"><button class="btn btn-danger" onclick="deleteRow('educationl_field<?=$newEducationalBackground->educational_background_id?>')">Delete</button></th>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                    
                </table>
                <button type="button" onclick="addEducationlBackground()" class="btn btn-default">Add</button>

                  <div class="panel panel-default" >
                    <div class="panel-heading">
                      <strong>Study Plan List</strong>  
                    </div>
                    <div class="panel-body" style="background-color: white;">
                      <table class="table" >
                        <thead>
                          <tr>
                            <th class="col-xs-6">Documents List</th>
                            <th class="col-xs-6">Operation</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">                   
                              <div class="form-group">
                      
                      <img id="img" src="<?php if($application != null): ?> <?=base_url()?>assets/sicss/images/<?=$application[0]->photocopy_of_passport?> <?php endif; ?>" class="img-responsive" width="160" height="100">
                      <br>
                      <label class="custom-file">
                        <input type="file" id="passport_copy"  name="passport_copy" class="custom-file-input" onchange="readURL(this);" >
                        <span class="custom-file-control"></span>                        
                      </label>
                             </div>
                           </th>

                            <th>The Photocopy of Passport (Front page and blank visa page)<small>(*.jpg,*.jpeg,*.png)：</small> </th>
                          </tr>
                          <tr>
                            <th scope="row">
                              <div class="form-group">
                      
                      <img id="img2" src="<?php if($application != null): ?> <?=base_url()?>assets/sicss/images/<?=$application[0]->high_school_certificate?> <?php endif; ?>" class="img-responsive" width="160" height="100">
                      <br>
                      <label class="custom-file">
                        <input type="file" id="high_school_certified" name="high_school_certified" class="custom-file-input" onchange="readURL2(this);" >
                        <span class="custom-file-control"></span>                        
                      </label>
                             </div>
                           </th>

                            <th>CThe Certified Copy of High School Transcripts<small>(*.pdf,*.jpg,*.jpeg,*.png)：</small></th>
                            
                          </tr>
                           <tr>
                            <th scope="row">
                              <div class="form-group">
                      
                      <img id="img3" src="<?php if($application != null): ?> <?=base_url()?>assets/sicss/images/<?=$application[0]->test_language_report?> <?php endif; ?>" class="img-responsive" width="160" height="100">
                      <br>
                      <label class="custom-file">
                        <input type="file" id="english_test" name="english_test"  class="custom-file-input" onchange="readURL3(this);" >
                        <span class="custom-file-control"></span>                        
                      </label>
                             </div>
                           </th>

                            <th>The Valid Report of Chinese or English Language Proficiency Tests<small>(*.pdf,*.jpg,*.jpeg,*.png)：</small></th>
                        
                          </tr>
                           <tr>
                            <th scope="row">
                              <div class="form-group">
                      
                      <img id="img4" src="<?php if($application != null): ?><?=base_url()?>assets/sicss/images/<?=$application[0]->physical_exam_test?><?php endif; ?>" class="img-responsive" width="160" height="100">
                      <br>
                      <label class="custom-file">
                        <input type="file" id="physical_test" name="physical_test" class="custom-file-input" onchange="readURL4(this);" >
                        <span class="custom-file-control"></span>                        
                      </label>
                             </div>
                           </th>

                            <th>The Physical Examination Report<small>(*.jpg,*.jpeg,*.png)：</small></th>
                           
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                      <input type="hidden" name="id" value="<?=$application[0]->application_id?>">
                      <input type="hidden" name="status" value="<?=$application[0]->status?>">
                      <div class="col-xs-12">
                        <input type="submit" name="submit" value="Save and Next" class="btn btn-primary">
                      </div>
                  </form>
                </div>
             


                    </div>     
            </div>
          </div>
  




          </div>
        </div>
      </div>

      </section>
      <div class="footer-login center">
        <p>&copy; 2017 Study in China. All right reserved.</p>
      </div>
    </div>

    <script type="text/javascript">
           function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();


                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }


        }

        function readURL2(input)
        {
            if (input.files && input.files[0]) {
                var reader = new FileReader();


                reader.onload = function (e) {
                    $('#img2')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }          
        }

        function readURL3(input)
        {
            if (input.files && input.files[0]) {
                var reader = new FileReader();


                reader.onload = function (e) {
                    $('#img3')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }          
        }        

        function readURL4(input)
        {
            if (input.files && input.files[0]) {
                var reader = new FileReader();


                reader.onload = function (e) {
                    $('#img4')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }          
        }
    </script>
    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/tempData.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/userStatus.js"></script>
</body>
</html>
