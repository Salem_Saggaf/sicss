<!DOCTYPE html>
<html lang="en">
<head>

    <title>SICSS | Index</title>
    <?php include('init/header.php');?>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
            <?php include('init/nav.php');?>
              <div class="row">
                <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3>Outbox</h3>
                    </div>
                    <div class="panel-body" align="center">                                    
                      <table class="table">
                        <thead>
                          <tr>
                            <th class="col">Title</th>
                            <th class="col">Content</th>
                            <th class="col">Send Time</th>
                            <th class="col">Operation</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($message as $newMessage) : ?>
                            <tr>
                              <th scope="row">
                                
                                                              <!-- Button trigger modal -->
                                  <a type="button"  data-toggle="modal" data-target="#exampleModalLong" style="color: blue;">
                                    <?=$newMessage->title?>
                                  </a>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
                                  <div class="modal-dialog" role="document" >
                                    <div class="modal-content" >
                                      <div class="modal-header" >
                                        <h5 class="modal-title" id="exampleModalLongTitle" ><?=$newMessage->title?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body" >
                                        <?=$newMessage->content?>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </th>
                              <th><?=$newMessage->content?></th>
                              <th><?=$newMessage->submitted?></th>
                              <form method="post" action="<?=base_url()?>Home/deleteMessage">
                                <input type="hidden" name="id" value="<?=$newMessage->id?>">
                                <th><button class="btn btn-danger">Delete</button></th>  
                              </form>
                              
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                      <nav aria-label="application navigation">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">

                </div>
              </div>
          </div>
        </div>
          

      </section>
    </div>  
      <div class="footer-login center">
      <?php include('init/footer.php'); ?>
</body>
</html>
