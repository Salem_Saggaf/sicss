<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Index</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
     'use strict';
          jQuery('body').backstretch([
            "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
        ], {duration: 5000, fade: 500});
      });
    </script>

    <script type="text/javascript">
      
      $( document ).ready(function() {
        
        start(<?=$application[0]->status?>, <?=$application[0]->application_id?>);
        for(var i =0; i<document.getElementById("adjacments").options.length;i++)
        {
          if(document.getElementById("adjacments").options[i].value == "<?=$application[0]->major_adjacment?>")
          {
           document.getElementById("adjacments").selectedIndex = i; 
          }
        }        

        for(var i =0; i<document.getElementById("rec_nationality").options.length;i++)
        {
          if(document.getElementById("rec_nationality").options[i].value == "<?=$application[0]->recommended_by_nationality?>")
          {
           document.getElementById("rec_nationality").selectedIndex = i; 
          }
        }
      });
    </script>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
          <?php include('init/nav.php');?>

            <?php include('init/sideMenuBar.php'); ?>
            <div class="col-xs-9" >
              <form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep6" >
                    <div class="col-xs-8">
                        <hr>
                          <h3>Language Proficiency</h3>
                        <hr>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="language_proficiency">*Language Proficiency</label>
                        <input type="text" name="language_proficiency" id="language_proficiency" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->language_proficiency?> <?php endif; ?>">
                      </div>     
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="toefl">TOEFL</label>
                        <input type="text" name="toefl" id="toefl" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->toefl?> <?php endif; ?>">
                      </div>                      
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="gre">GRE</label>
                        <input type="text" name="gre" id="gre" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->gre?> <?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="gmat">GMAT</label>
                        <input type="text" name="gmat" id="gmat" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->gmat?> <?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="ielts">IELTS</label>
                        <input type="text" name="ielts" id="ielts" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->ielts?> <?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="other_language_proficiency">Other Language Proficiency </label>
                        <input type="text" name="other_language_proficiency" id="other_language_proficiency" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->other_language_proficiency?> <?php endif; ?>">
                      </div>     
                    </div>

                    <div class="col-xs-12">
                      <hr>
                        <h3>Study Plan</h3>
                      <hr>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="input-group">
                        <span class="input-group-addon" id="program">Program</span>
                         <input type="text" class="form-control"   aria-describedby="program" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->type?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>

                    <div class="col-xs-6">
                      <div class="input-group">
                        <span class="input-group-addon" id="department">Department</span>
                         <input type="text" class="form-control"   aria-describedby="department" value="<?php if($application != null): ?> <?=$application[0]->department?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>

                    <div class="col-xs-6">
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="major">Major</span>
                         <input type="text" class="form-control"   aria-describedby="major" value="<?php if($application != null): ?> <?=$application[0]->major?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>

                    <div class="col-xs-6">
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="teaching_language">Teaching Language</span>
                         <input type="text" class="form-control"   aria-describedby="teaching_language" value="<?php if($application != null): ?> <?=$application[0]->teaching_language?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>

                    <div class="col-xs-6">
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="study_years">Study Years</span>
                         <input type="text" class="form-control"   aria-describedby="study_years" value="<?php if($application != null): ?> <?=$application[0]->years?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>

                    <div class="col-xs-6">
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="study_duration">Study Duration From</span>
                         <input type="text" class="form-control"   aria-describedby="study_duration" value="<?php if($application != null): ?> <?=$application[0]->duration_from?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>

                    <div class="col-xs-6">
                      <br>
                      <div class="input-group">
                        <span class="input-group-addon" id="study_duration">Study Duration To</span>
                         <input type="text" class="form-control"   aria-describedby="study_duration" value="<?php if($application != null): ?> <?=$application[0]->duration_to?> <?php endif; ?>" readonly>
                      </div>
                        
                    </div>
                    

                    <div class="col-xs-12">
                      <br>
                      <select class="form-control" name="adjacments" id="adjacments">
                        <option value="Reject">Reject Major Adjacments</option>
                        <option value="Accept" checked>Accept Major Adjacments</option>
                      </select>
                          
                    </div>

                    <div class="col-xs-12">
                      <hr>
                        <h3>Second Choice</h3>
                      <hr>                      
                    </div>


                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="alt_college">College</label>
                        <input type="text" name="" id="alt_college" name="alt_college" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->second_choice_college?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="alt_major">Major</label>
                        <input type="text" name="" id="alt_major" name="alt_major" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->second_choice_major?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="alt_teaching_laguage">Teaching Language</label>
                        <input type="text" name="" id="alt_teaching_laguage" name="alt_teaching_laguage" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->second_choice_teaching_language?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="alt_study_years">Study Years</label>
                        <input type="text" name="" id="alt_study_years" name="alt_study_years" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->second_choice_study_years?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-12">
                      <hr>
                        <h3>Recommended by 1</h3>
                      <hr>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="rec_name">Name</label>
                        <input type="text" name="rec_name" id="rec_name" name="rec_name" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_name?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="relation_with_the_applicant">Relationship With The Applicant</label>
                        <input type="text" name="rec_relation" id="relation_with_the_applicant" name="rec_relation" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_relationship?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="orgnaization">Orgnaization</label>
                        <input type="text" name="rec_orgnaization" id="orgnaization" name="rec_orgnaization" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_orgnaization?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="mobile">Mobile</label>
                        <input type="text" name="rec_mobile" id="mobile" name="rec_mobile" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_mobile?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="phone_number">*Phone Number</label>
                        <input type="text" name="rec_phone" id="phone_number" name="rec_phone" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_mobile?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="rec_email" id="email" name="rec_email" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_email?> <?php endif; ?>">
                      </div>                      
                    </div>                                                                                

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="rec_nationality">*Nationality</label>
                        <select name="rec_nationality" id="rec_nationality" class="form-control">
                          <option value="">-- select one --</option>
                          <option value="afghan">Afghan</option>
                          <option value="albanian">Albanian</option>
                          <option value="algerian">Algerian</option>
                          <option value="american">American</option>
                          <option value="andorran">Andorran</option>
                          <option value="angolan">Angolan</option>
                          <option value="antiguans">Antiguans</option>
                          <option value="argentinean">Argentinean</option>
                          <option value="armenian">Armenian</option>
                          <option value="australian">Australian</option>
                          <option value="austrian">Austrian</option>
                          <option value="azerbaijani">Azerbaijani</option>
                          <option value="bahamian">Bahamian</option>
                          <option value="bahraini">Bahraini</option>
                          <option value="bangladeshi">Bangladeshi</option>
                          <option value="barbadian">Barbadian</option>
                          <option value="barbudans">Barbudans</option>
                          <option value="batswana">Batswana</option>
                          <option value="belarusian">Belarusian</option>
                          <option value="belgian">Belgian</option>
                          <option value="belizean">Belizean</option>
                          <option value="beninese">Beninese</option>
                          <option value="bhutanese">Bhutanese</option>
                          <option value="bolivian">Bolivian</option>
                          <option value="bosnian">Bosnian</option>
                          <option value="brazilian">Brazilian</option>
                          <option value="british">British</option>
                          <option value="bruneian">Bruneian</option>
                          <option value="bulgarian">Bulgarian</option>
                          <option value="burkinabe">Burkinabe</option>
                          <option value="burmese">Burmese</option>
                          <option value="burundian">Burundian</option>
                          <option value="cambodian">Cambodian</option>
                          <option value="cameroonian">Cameroonian</option>
                          <option value="canadian">Canadian</option>
                          <option value="cape verdean">Cape Verdean</option>
                          <option value="central african">Central African</option>
                          <option value="chadian">Chadian</option>
                          <option value="chilean">Chilean</option>
                          <option value="chinese">Chinese</option>
                          <option value="colombian">Colombian</option>
                          <option value="comoran">Comoran</option>
                          <option value="congolese">Congolese</option>
                          <option value="costa rican">Costa Rican</option>
                          <option value="croatian">Croatian</option>
                          <option value="cuban">Cuban</option>
                          <option value="cypriot">Cypriot</option>
                          <option value="czech">Czech</option>
                          <option value="danish">Danish</option>
                          <option value="djibouti">Djibouti</option>
                          <option value="dominican">Dominican</option>
                          <option value="dutch">Dutch</option>
                          <option value="east timorese">East Timorese</option>
                          <option value="ecuadorean">Ecuadorean</option>
                          <option value="egyptian">Egyptian</option>
                          <option value="emirian">Emirian</option>
                          <option value="equatorial guinean">Equatorial Guinean</option>
                          <option value="eritrean">Eritrean</option>
                          <option value="estonian">Estonian</option>
                          <option value="ethiopian">Ethiopian</option>
                          <option value="fijian">Fijian</option>
                          <option value="filipino">Filipino</option>
                          <option value="finnish">Finnish</option>
                          <option value="french">French</option>
                          <option value="gabonese">Gabonese</option>
                          <option value="gambian">Gambian</option>
                          <option value="georgian">Georgian</option>
                          <option value="german">German</option>
                          <option value="ghanaian">Ghanaian</option>
                          <option value="greek">Greek</option>
                          <option value="grenadian">Grenadian</option>
                          <option value="guatemalan">Guatemalan</option>
                          <option value="guinea-bissauan">Guinea-Bissauan</option>
                          <option value="guinean">Guinean</option>
                          <option value="guyanese">Guyanese</option>
                          <option value="haitian">Haitian</option>
                          <option value="herzegovinian">Herzegovinian</option>
                          <option value="honduran">Honduran</option>
                          <option value="hungarian">Hungarian</option>
                          <option value="icelander">Icelander</option>
                          <option value="indian">Indian</option>
                          <option value="indonesian">Indonesian</option>
                          <option value="iranian">Iranian</option>
                          <option value="iraqi">Iraqi</option>
                          <option value="irish">Irish</option>
                          <option value="israeli">Israeli</option>
                          <option value="italian">Italian</option>
                          <option value="ivorian">Ivorian</option>
                          <option value="jamaican">Jamaican</option>
                          <option value="japanese">Japanese</option>
                          <option value="jordanian">Jordanian</option>
                          <option value="kazakhstani">Kazakhstani</option>
                          <option value="kenyan">Kenyan</option>
                          <option value="kittian and nevisian">Kittian and Nevisian</option>
                          <option value="kuwaiti">Kuwaiti</option>
                          <option value="kyrgyz">Kyrgyz</option>
                          <option value="laotian">Laotian</option>
                          <option value="latvian">Latvian</option>
                          <option value="lebanese">Lebanese</option>
                          <option value="liberian">Liberian</option>
                          <option value="libyan">Libyan</option>
                          <option value="liechtensteiner">Liechtensteiner</option>
                          <option value="lithuanian">Lithuanian</option>
                          <option value="luxembourger">Luxembourger</option>
                          <option value="macedonian">Macedonian</option>
                          <option value="malagasy">Malagasy</option>
                          <option value="malawian">Malawian</option>
                          <option value="malaysian">Malaysian</option>
                          <option value="maldivan">Maldivan</option>
                          <option value="malian">Malian</option>
                          <option value="maltese">Maltese</option>
                          <option value="marshallese">Marshallese</option>
                          <option value="mauritanian">Mauritanian</option>
                          <option value="mauritian">Mauritian</option>
                          <option value="mexican">Mexican</option>
                          <option value="micronesian">Micronesian</option>
                          <option value="moldovan">Moldovan</option>
                          <option value="monacan">Monacan</option>
                          <option value="mongolian">Mongolian</option>
                          <option value="moroccan">Moroccan</option>
                          <option value="mosotho">Mosotho</option>
                          <option value="motswana">Motswana</option>
                          <option value="mozambican">Mozambican</option>
                          <option value="namibian">Namibian</option>
                          <option value="nauruan">Nauruan</option>
                          <option value="nepalese">Nepalese</option>
                          <option value="new zealander">New Zealander</option>
                          <option value="ni-vanuatu">Ni-Vanuatu</option>
                          <option value="nicaraguan">Nicaraguan</option>
                          <option value="nigerien">Nigerien</option>
                          <option value="north korean">North Korean</option>
                          <option value="northern irish">Northern Irish</option>
                          <option value="norwegian">Norwegian</option>
                          <option value="omani">Omani</option>
                          <option value="pakistani">Pakistani</option>
                          <option value="palauan">Palauan</option>
                          <option value="panamanian">Panamanian</option>
                          <option value="papua new guinean">Papua New Guinean</option>
                          <option value="paraguayan">Paraguayan</option>
                          <option value="peruvian">Peruvian</option>
                          <option value="polish">Polish</option>
                          <option value="portuguese">Portuguese</option>
                          <option value="qatari">Qatari</option>
                          <option value="romanian">Romanian</option>
                          <option value="russian">Russian</option>
                          <option value="rwandan">Rwandan</option>
                          <option value="saint lucian">Saint Lucian</option>
                          <option value="salvadoran">Salvadoran</option>
                          <option value="samoan">Samoan</option>
                          <option value="san marinese">San Marinese</option>
                          <option value="sao tomean">Sao Tomean</option>
                          <option value="saudi">Saudi</option>
                          <option value="scottish">Scottish</option>
                          <option value="senegalese">Senegalese</option>
                          <option value="serbian">Serbian</option>
                          <option value="seychellois">Seychellois</option>
                          <option value="sierra leonean">Sierra Leonean</option>
                          <option value="singaporean">Singaporean</option>
                          <option value="slovakian">Slovakian</option>
                          <option value="slovenian">Slovenian</option>
                          <option value="solomon islander">Solomon Islander</option>
                          <option value="somali">Somali</option>
                          <option value="south african">South African</option>
                          <option value="south korean">South Korean</option>
                          <option value="spanish">Spanish</option>
                          <option value="sri lankan">Sri Lankan</option>
                          <option value="sudanese">Sudanese</option>
                          <option value="surinamer">Surinamer</option>
                          <option value="swazi">Swazi</option>
                          <option value="swedish">Swedish</option>
                          <option value="swiss">Swiss</option>
                          <option value="syrian">Syrian</option>
                          <option value="taiwanese">Taiwanese</option>
                          <option value="tajik">Tajik</option>
                          <option value="tanzanian">Tanzanian</option>
                          <option value="thai">Thai</option>
                          <option value="togolese">Togolese</option>
                          <option value="tongan">Tongan</option>
                          <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                          <option value="tunisian">Tunisian</option>
                          <option value="turkish">Turkish</option>
                          <option value="tuvaluan">Tuvaluan</option>
                          <option value="ugandan">Ugandan</option>
                          <option value="ukrainian">Ukrainian</option>
                          <option value="uruguayan">Uruguayan</option>
                          <option value="uzbekistani">Uzbekistani</option>
                          <option value="venezuelan">Venezuelan</option>
                          <option value="vietnamese">Vietnamese</option>
                          <option value="welsh">Welsh</option>
                          <option value="yemenite">Yemenite</option>
                          <option value="zambian">Zambian</option>
                          <option value="zimbabwean">Zimbabwean</option>
                        </select>
                      </div>     
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="job">Job</label>
                        <input type="text" name="job" id="job" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_job?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="rec_address">Address</label>
                        <input type="text" name="rec_address" id="rec_address" name="rec_address" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_address?> <?php endif; ?>">
                      </div>                      
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="fax_number">Fax Number </label>
                        <input type="text" name="fax_number" id="fax_number" name="fax_number" class="form-control" value="<?php if($application != null): ?> <?=$application[0]->recommended_by_fax_number?> <?php endif; ?>">
                      </div>                      
                    </div>
                    <input type="hidden" name="id" value="<?=$application[0]->application_id?>">
                    <input type="hidden" name="status" value="<?=$application[0]->status?>">
                      <div class="col-xs-12">
                        <input type="submit" name="submit" value="Save and Next" class="btn btn-primary">
                      </div>

                    </div>     
              </form>
            </div>
          </div>
  




          </div>
        </div>
      </div>

      </section>
      <div class="footer-login center">
        <p>&copy; 2017 Study in China. All right reserved.</p>
      </div>
    </div>
    <script type="text/javascript">
           function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/userStatus.js"></script>
</body>
</html>
