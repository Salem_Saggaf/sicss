
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Application</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="<?=base_url()?>assets/sicss/shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
	'use strict';
      	jQuery('body').backstretch([
          "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
	    ], {duration: 5000, fade: 500});
    });
    </script>
	<style media="screen" type="text/css">
	  body{
	    overflow: visible;
	  }
	</style>
</head>

<body>
	<div id="preloader"></div>
    <div id="content-wrapper">
		<div id="header" ></div>
		<section id="body" class="white">
			<div class="index-title" >
				<div class="container" >
		            <?php include('init/nav.php'); ?>
		        	<div class="panel panel-default">
		        		<div class="panel-body">
				        	<div class="row" >
				        		<div class="col-xs-12">
						        	<form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep3">
						        		<div class="form-group">
						        			<label for="type"> Please Choose Your Type :</label>
						        			<select class="form-control" id="type" name="type" required>
                            <option value="">Select Program</option>
						        				<option value="Undergraduate Program">Undergraduate Program</option>
						        				<option value="Chinese Language Student">Chinese Language Program</option>
						        			</select>
						        		</div>

					        			<input type="submit" name="submit"	value="&ensp; Next &ensp;" class="btn btn-success">

					        			<a  href="<?=base_url()?>ApplicationProcess/applicationStep1" class="btn btn-primary" style="color: white;">Prior Back</a>

						        	</form>
				        		</div>
				        	</div>
		        		</div>
		        	</div>





			</div>
			<div class="container" style="background-color: grey;">

			</div>
		</section>
		<div class="footer-login center">
	       <p>&copy; 2017 Study in China. All right reserved.</p>
	    </div>
	</div>



    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
</body>
</html>
