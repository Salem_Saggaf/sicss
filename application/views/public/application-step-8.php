<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Index</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
      jQuery(document).ready(function($){
        'use strict';
          jQuery('body').backstretch([
            "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
        ], {duration: 5000, fade: 500});
      });
    </script>

    </script>
    <script type="text/javascript">
      $( document ).ready(function() {
        
        start(<?=$application[0]->status?>, <?=$application[0]->application_id?>);
      });
    </script>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
          <?php include('init/nav.php');?>


              
            <?php include('init/sideMenuBar.php'); ?>

            <div class="col-xs-8">
              <div class="col-xs-4">
                <img src="<?=base_url()?>assets/sicss/images/<?=$application[0]->profile_picutre?>" class="img-responsive" alt="profile_img">
              </div>
              <div class="col-xs-8">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td class="infoTitle">Family Name (as on passport)</td>
                        <td class="information"><?=$application[0]->family_name?></td>
                        <td class="infoTitle">Given Name(as on passport)</td>
                        <td class="information"><?=$application[0]->given_name?></td>
                      </tr>
                      <tr>
                        <td class="infoTitle">Chinese Name (if available)</td>
                        <td class="information"><?=$application[0]->chinese_name?></td>
                        <td class="infoTitle">Gender</td>
                        <td class="information"><?=$application[0]->gender?></td>
                      </tr>
                      <tr>
                        <td class="infoTitle">Marital Status</td>
                        <td class="information"><?=$application[0]->martial_status?></td>
                        <td class="infoTitle">Nationality</td>
                        <td class="information"><?=$application[0]->nationality?></td>
                      </tr>
                      <tr>
                        <td class="infoTitle">Birth Date</td>
                        <td class="information"><?=$application[0]->birth_date?></td>
                        <td class="infoTitle">Country of Birth</td>
                        <td class="information"><?=$application[0]->place_of_birth?></td>
                      </tr>
                      <tr>
                        <td class="infoTitle">Highest Level of Education</td>
                        <td class="information"><?=$application[0]->highest_level_of_education?></td>
                        <td class="infoTitle">Religion</td>
                        <td class="information"><?=$application[0]->religion?></td>
                      </tr>
                      <tr>
                        <td class="infoTitle">Employer or Institution Affiliated</td>
                        <td class="information"><?=$application[0]->employer_or_institution_affiliated?></td>
                        <td class="infoTitle">Occupation</td>
                        <td class="information"><?=$application[0]->place_of_birth?></td>
                      </tr>
                      <tr>
                        <td class="infoTitle">Health Status</td>
                        <td class="information"><?=$application[0]->place_of_birth?></td>
                        <td class="infoTitle">Emigrant from mainland China, Hong Kong, Macau, and Taiwan?</td>
                        <td class="information"><?=$application[0]->emigrant_from?></td>
                        
                      </tr>
                      <tr>
                        <td class="infoTitle" colspan="2">Hobby</td>
                        <td class="information" colspan="2"><?=$application[0]->hobby?></td>
                      </tr>
                    </tbody>
                  </table>             
              </div>
              <div class="col-xs-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    Passport And Visa
                  </div>
                  <div class="panel-body">
                    <table class="table table-user-information">
                      <tr>
                        <td class="infoTitle">Passport No.</td>
                        <td class="information"><?=$application[0]->passport_no?></td>
                        <td class="infoTitle">Passport Expiration Date</td>
                        <td class="information"><?=$application[0]->passport_expiry?></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Educational Background
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tr>
                          <th class="col">Year Attended (From)</th>
                          <th class="col">Year Attended (To)</th>
                          <th class="col">School Name</th>
                          <th class="col">Field of Study &amp; Diploma received</th>
                        </tr>
                        <?php foreach($educational_background as $new_educational_background): ?>
                          <tr>
                            <th scope="row"><?=$new_educational_background->year_attended_from?></th>
                            <th><?=$new_educational_background->year_attended_to?></th>
                            <th><?=$new_educational_background->school_name?></th>
                            <th><?=$new_educational_background->field_of_study?></th>
                          </tr>
                        <?php endforeach; ?>
                      </table>
                    </div>
                  </div>
              </div>


              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Upload Documents
                    </div>
                    <div class="panel-body">
                      <table class="table-user-information">
                        <tr>
                          <th class="col-xs-5">Docments List</th>
                          <th class="col">Document Type</th>
                        </tr>
                        <tr>
                          <th scope="row"><img src="<?=base_url()?>assets/sicss/images/<?=$application[0]->photocopy_of_passport?>" class="img-responsive" ></th>
                          <th>The Photocopy of Passport (Front page and blank visa page)</th>
                        </tr>
                        <tr>
                          <th  scope="row"><img src="<?=base_url()?>assets/sicss/images/<?=$application[0]->high_school_certificate?>" class="img-responsive"></th>
                          <th>The Certified Copy of High School Transcripts</th>
                        </tr>
                        <tr>
                          <th><img src="<?=base_url()?>assets/sicss/images/<?=$application[0]->test_language_report?>" class="img-responsive"></th>
                          <th>The Valid Report of Chinese or English Language Proficiency Tests</th>
                        </tr>
                        <tr>
                          <th><img src="<?=base_url()?>assets/sicss/images/<?=$application[0]->test_language_report?>" class="img-responsive"></th>
                          <th>The Physical Examination Report</th>
                        </tr>
                      </table>
                    </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Language Proficiency
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle" align="center" colspan="4">English Proficiency</td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Language Proficiency</td>
                            <td class="information"><?=$application[0]->language_proficiency?></td>
                            <td class="infoTitle">TOEFL</td>
                            <td class="information"><?=$application[0]->toefl?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">GRE</td>
                            <td class="information"><?=$application[0]->gre?></td>
                            <td class="infoTitle">GMAT</td>
                            <td class="information"><?=$application[0]->gmat?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">IELTS</td>
                            <td class="information"><?=$application[0]->ielts?></td>
                            <td class="infoTitle">Other Language Proficiency</td>
                            <td class="information"><?=$application[0]->other_language_proficiency?></td>
                          </tr>
                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Study Plan - Undergraduate Program
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle">Program</td>
                            <td class="information"><?=$application[0]->type?></td>
                            <td class="infoTitle">Department</td>
                            <td class="information"><?=$application[0]->department?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Major</td>
                            <td class="information"><?=$application[0]->major?></td>
                            <td class="infoTitle">Teaching Language</td>
                            <td class="information"><?=$application[0]->teaching_language?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Study Years</td>
                            <td class="information"><?=$application[0]->years?></td>
                            <td class="infoTitle">Study Duration</td>
                            <td class="information"><?=$application[0]->duration_from?>--<?=$application[0]->duration_to?></td>
                          </tr>
                          <tr>
                            <td class="information" colspan="4" align="center"><?=$application[0]->major_adjacment?></td>
                          </tr>
                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>


              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Recommended by (1)
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle">Source</td>
                            <td class="information"><?=$application[0]->recommended_by_name?></td>
                            <td class="infoTitle">Name</td>
                            <td class="information"><?=$application[0]->recommended_by_name?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Relationship with the applicant</td>
                            <td class="information"><?=$application[0]->recommended_by_relationship?></td>
                            <td class="infoTitle">Organization</td>
                            <td class="information"><?=$application[0]->recommended_by_orgnaization?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Mobile</td>
                            <td class="information"><?=$application[0]->recommended_by_mobile?></td>
                            <td class="infoTitle">Phone Number</td>
                            <td class="information"><?=$application[0]->recommended_by_phone_number?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Email</td>
                            <td class="information"><?=$application[0]->recommended_by_email?>@gmail.com</td>
                            <td class="infoTitle">Nationality</td>
                            <td class="information"><?=$application[0]->recommended_by_nationality?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Job</td>
                            <td class="information"><?=$application[0]->recommended_by_job?></td>
                            <td class="infoTitle">Address</td>
                            <td class="information"><?=$application[0]->recommended_by_address?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle" colspan="2">Fax Number</td>
                            <td class="information" colspan="2"><?=$application[0]->recommended_by_fax_number?></td>
                          </tr>
                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Family Status
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tr>
                          <th class="col">Family Members</th>
                          <th class="col">Name</th>
                          <th class="col">Phone Number</th>
                          <th class="col">Email</th>
                          <th class="col">Position</th>
                          <th class="col">Work Place</th>
                        </tr>
                        <?php foreach($family_member as $new_family_member): ?>
                          <tr>
                            <th scope="row"><?=$new_family_member->family_member?></th>
                            <th><?=$new_family_member->name?></th>
                            <th><?=$new_family_member->phone_number?></th>
                            <th><?=$new_family_member->email?></th>
                            <th><?=$new_family_member->position?></th>
                            <th><?=$new_family_member->work_place?></th>
                          </tr>
                        <?php endforeach; ?>
                      </table>
                    </div>
                  </div>
              </div>

               <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Financial Supporter
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle">Guarantor name</td>
                            <td class="information"><?=$application[0]->guarantor_name?></td>
                            <td class="infoTitle">The guarantor Address</td>
                            <td class="information"><?=$application[0]->guarantor_address?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">The guarantor Tel</td>
                            <td class="information"><?=$application[0]->guarantor_tel?></td>
                            <td class="infoTitle">Relationship with applicant</td>
                            <td class="information"><?=$application[0]->guarantor_orgnaization?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Organization</td>
                            <td class="information"><?=$application[0]->guarantor_orgnaization?></td>
                            <td class="infoTitle">Email</td>
                            <td class="information"><?=$application[0]->guarantor_email?></td>
                          </tr>

                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Emergency Contact
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle">Name</td>
                            <td class="information"><?=$application[0]->emergency_contact_name?></td>
                            <td class="infoTitle">Mobile</td>
                            <td class="information"><?=$application[0]->emergency_contact_mobile?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Phone Number</td>
                            <td class="information"><?=$application[0]->emergency_contact_phone_number?></td>
                            <td class="infoTitle">Email</td>
                            <td class="information"><?=$application[0]->emergency_contact_email?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Organization</td>
                            <td class="information"><?=$application[0]->emergency_contact_orgnaization?></td>
                            <td class="infoTitle">Address</td>
                            <td class="information"><?=$application[0]->emergency_contact_address?></td>
                          </tr>

                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Home Country Address
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle">Street Address</td>
                            <td class="information"><?=$application[0]->home_address_street?></td>
                            <td class="infoTitle">Phone Number</td>
                            <td class="information"><?=$application[0]->home_address_phone_number?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">City/Province</td>
                            <td class="information"><?=$application[0]->home_address_city?></td>
                            <td class="infoTitle">Mobile</td>
                            <td class="information"><?=$application[0]->home_address_mobile?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Country</td>
                            <td class="information"><?=$application[0]->home_address_country?></td>
                            <td class="infoTitle">Zipcode</td>
                            <td class="information"><?=$application[0]->home_address_zip_code?></td>
                          </tr>

                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Home Country Address
                    </div>
                    <div class="panel-body">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td class="infoTitle">Personal Email</td>
                            <td class="information"><?=$application[0]->current_address_email?></td>
                            <td class="infoTitle">Mobile/Phone Number</td>
                            <td class="information"><?=$application[0]->current_address_mobile?></td>
                          </tr>
                          <tr>
                            <td class="infoTitle">Address</td>
                            <td class="information"><?=$application[0]->current_address?></td>
                            <td class="infoTitle">Zipcode</td>
                            <td class="information"><?=$application[0]->current_address_zip_code?></td>
                          </tr>
                        </tbody>
                      </table>             
                    </div>
                  </div>
              </div>

               <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      How to Collect the Admission Notice
                    </div>
                    <div class="panel-body">
                      <small><?=$application[0]->radio_admission_notice?></small>            
                    </div>
                  </div>
              </div>

            </div>
          </div>
  




          </div>
        </div>
      </div>

      </section>
      <div class="footer-login center">
        <p>&copy; 2017 Study in China. All right reserved.</p>
      </div>
    </div>
    <script type="text/javascript">
           function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/userStatus.js"></script>
</body>
</html>
