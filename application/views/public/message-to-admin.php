<!DOCTYPE html>
<html lang="en">
<head>

    <title>SICSS | Index</title>
    <?php include('init/header.php');?>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
            <?php include('init/nav.php');?>
            <div class="row">
              <div class="col-xs-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4>Send the message to the teacher</h4>  
                  </div>
                  <div class="panel-body">
                    <form method="post" action="<?=base_url()?>Home/insert_lead">
                      <div class="form-group">
                        <label for="title">Title :</label>
                        <input type="text" name="title" class="form-control" placeholder="Title">
                      </div>
                      <div class="form-group">
                        <label for="content">Content :</label>
                        <textarea name="content" placeholder="Message" class="form-control" rows="6"></textarea>
                      </div>
                      <input type="hidden" name="name" value="<?=$user[0]['username']?>">
                      <input type="hidden" name="email" value="<?=$user[0]['email']?>">
                      <input type="submit" name="submit" class="form-control">
                    </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
          

      </section>
      
    <?php include('init/footer.php'); ?>
</body>
</html>
