<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('init/header.php'); ?>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-xs-12">
                <img src="<?=base_url()?>assets/sicss/images/logo/logo.png" class="img-responsive" alt="">
              </div>
              <div class="col-sm-6 col-xs-12">
                <p style="color: white; text-transform:capitalize"><?=$user[0]['username']?>&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>home/logout">| &nbsp; Logout</a> </p>
              </div>
              <br>
              <br>


            <div class="row" >
              <div class="col-xm-12" style="color: white; text-align: center;">


              </div>
            </div>

            <div class="row" >

              <div class="col-xs-12" >
                <img src="<?=base_url()?>assets/sicss/images/default.png" class="img-responsive center-block"   alt="profile" style="border:0px solid black; width: 205px; border-radius: 50%;">
              </div>
            </div>
            <br>


          <div class="panel panel-default">
                  <div class="panel-body">
                      <div class="row " style="margin-top: 1em;">
                        <div class="col-md-4">
                          <button class="btn btn-primary btn-block" style="text-transform:capitalize;">profile picture</button>
                        </div>

                        <div class="col-md-4">
                          <button class="btn btn-success btn-lg btn-block" style="text-transform:capitalize">Edit profile</button>
                        </div>

                        <div class="col-sm-4">
                          <button class="btn btn-danger btn-lg btn-block" style="text-transform:capitalize">Change Password</button>
                        </div>

                        <!-- <div class="col-sm-4">
                          <button class="btn btn-primary btn-lg btn-block" style="text-transform:capitalize">Bind Study Information</button>
                        </div> -->




                      </div>
                      <div class="row">
                      <div class="icon-btn">

                        <div class="col-md-2 col-sm-4 col-xs-6">
                          <a href="<?=base_url()?>ApplicationProcess"><i class="fa fa-user" aria-hidden="true"></i></a>
                          <h5 style="color: black;"><br>Application Online</h5>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                          <i class="fa fa-home" aria-hidden="true"></i>
                          <h5 style="color: black;"><br>Dormitory Reservation</h5>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                          <i class="fa fa-plane" aria-hidden="true"></i>
                          <h5 style="color: black;"><br>Airport Pickup</h5>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                          <i class="fa fa-search" aria-hidden="true"></i>
                          <h5 style="color: black;"><br>Query Result</h5>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                          <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                          <h5 style="color: black;"><br>Application Notes</h5>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                          <i class="fa fa-map-marker" aria-hidden="true"></i>
                          <h5 style="color: black;"><br>School Map</h5>
                        </div>
                     </div>
                  </div>


            </div>
            </div>
          </div>
        </div>
      </div>

      </section>
      <?php include('init/footer.php'); ?>
</body>
</html>
