<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Index</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
	'use strict';
      	jQuery('body').backstretch([
          "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
	    ], {duration: 5000, fade: 500});
    });
    </script>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">

              <?php include('init/nav.php');?>
              <div class="row">
                <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 style="color:#7E7C7B">Application List</h3>
                    </div>
                    <div class="panel-body" align="center">
                      <table class="table">
                        <thead>
                          <tr>
                            <th class="col">Application No.</th>
                            <th class="col">English Name</th>
                            <th class="col">Institution Name</th>
                            <th class="col">Program</th>
                            <th class="col">Application Status</th>
                            <th class="col">Time of Creation</th>
                            <th class="col">Time of Modification</th>
                            <th class="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($application as $newApplication) : ?>
                            <tr>
                                <th scope="row"></th>
                                <th class="col-xs-2"><?=$newApplication->family_name?></th>
                                <th class="col-xs-2"><?=$newApplication->major?></th>
                                <th><?=$newApplication->type?></th>
                                <th>filled in</th>
                                <th>2018-01-02 10:32:41</th>
                                <th>2018-01-02 10:32:41</th>
                                <th>
                                  <div class="btn-group">
                                   <form method="post" action="<?=base_url()?>ApplicationProcess/editApplication">
                                     <input type="hidden" name="id" value="<?=$newApplication->application_id?>">
                                     <input type="submit" class="btn btn-primary btn-xs" value="Edit"/>
                                   </form>

                                   <form method="post" action="<?=base_url()?>ApplicationProcess/viewApplicationById">
                                      <input type="hidden" name="id" value="<?=$newApplication->application_id?>">
                                      <input type="submit" class="btn btn-success btn-xs" value="View"/>
                                   </form>

                                    <form method="post" action="<?=base_url()?>ApplicationProcess/deleteApplication">
                                      <input type="hidden" name="id" value="<?=$newApplication->application_id?>">
                                      <input type="submit" class="btn btn-danger btn-xs" value="Delete"/>
                                    </form>
                                    <span class="label label-success"> &ensp;  <span class="glyphicon glyphicon-eye-open"></span> View &ensp;</span> <br>
                                    <span class="label label-info"> &ensp; <span class="glyphicon glyphicon-pencil"></span> Edit &ensp;</span> <br>
                                    <span class="label label-danger"> <span class="glyphicon glyphicon-trash"></span> Delete</span

                                  </div>
                                </th>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                      <nav aria-label="application navigation">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">

                </div>
              </div>
          </div>
        </div>


      </section>
    </div>
      <div class="footer-login center">
        <p>&copy; 2017 Study in China. All right reserved.</p>
      </div>

    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
</body>
</html>
