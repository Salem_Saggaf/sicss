<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Application</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
	'use strict';
      	jQuery('body').backstretch([
          "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
	    ], {duration: 5000, fade: 500});
    });
    </script>
	<style media="screen" type="text/css">
	  body{
	    overflow: visible;
	  }
	</style>
</head>

<body>
	<div id="preloader"></div>
    <div id="content-wrapper">
		<div id="header" ></div>
		<section id="body" class="white">
			<div class="index-title" >
				<div class="container">


					<?php include('init/nav.php'); ?>
		        	<div class="row">

		        		<div class="col-xs-12">
			        		<?php if(isset($message)): ?>
								<div class="alert alert-danger alert-dismissable" >
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<?=$message?>
								</div>
							<?php endif; ?>
		        			<div class="panel panel-default">
		        				<div class="panel-heading">
		        					Query Option
		        				</div>
		        				<div class="panel-body">
		        					<form method="post" action="<?=base_url()?>ApplicationProcess/searchApplication">
		        						<div class="form-group">
		        							<label for="department">Department</label>
		        							<select id="department" name="department" class="form-control" placeholder = "Please choose" required>
		        								<option value="">Select Department</option>
		        								<?php foreach($study_list as $newStudyList): ?>
		        									<option value="<?=$newStudyList->department?>"><?=$newStudyList->department?></option>
		        								<?php endforeach; ?>
		        							</select>
		        						</div>
		        						<div class="form-group">
		        							<label for="major">Major</label>

		        							<select id="major" name="major" class="form-control" placeholder="Please choose" required>
		        								<option value="">Select Major</option>
		        								<?php foreach($study_list as $newStudyList): ?>
		        									<option value="<?=$newStudyList->major?>"><?=$newStudyList->major?></option>
		        								<?php endforeach; ?>
		        							</select>
		        						</div>
		        						<div class="form-group">
		        							<label for="university">University Name</label>
		        							<input type="text" name="university" id="university" class="form-control" required>
		        						</div>
		        						<div class="form-group">
		        							<label for="language">Teaching Language</label>
		        							<select id="language" name="language" class="form-control" required>
		        								<option value="">Select Lanuguage</option>
		        								<option value="English">English</option>
		        								<option value="Chinese">Chinese</option>
		        							</select>
		        						</div>
		        						<input type="submit" name="submit" class="btn btn-primary" value=" &ensp;Find&ensp; ">
                        <a  href="<?=base_url()?>ApplicationProcess/applicationStep1" class="btn btn-danger" style="color: white;">Return </a>
		        					</form>
		        				</div>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-xs-12" >
		        			<div class="panel panel-default" >
		        				<div class="panel-heading">
		        					Study Plan List
		        				</div>
			        			<div class="panel-body" style="background-color: white;">
			        				<table class="table" >
			        					<thead>
			        						<tr>
			        							<th class="col">Study Plan Name</th>
			        							<th class="col">University Name</th>
			        							<th class="col">Department</th>
			        							<th class="col">Major</th>
			        							<th class="col">Duration</th>
			        							<th class="col">Years</th>
			        							<th class="col">Teaching Language</th>
			        							<th class="col">Apply Deadling</th>
			        							<!-- <th class="col">Notes</th> -->
			        							<th class="col">Operation</th>
			        						</tr>
			        					</thead>
			        					<tbody>

			        						<?php foreach($type as $newType): ?>
				        						<tr>
				        							<form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep4">
				        								<th scope="row"><?=$newType->study_plan_name?></th>
				        								<th><?=$newType->university_name?></th>
					        							<th><?=$newType->department?></th>
					        							<th><?=$newType->major?></th>
					        							<th><?=$newType->duration_from?>-<?=$newType->duration_to?></th>
					        							<th><?=$newType->years?></th>
					        							<th><?=$newType->teaching_language?></th>
					        							<th><?=$newType->apply_deadline?></th>
					        							<!-- <th><?=$newType->notes?></th> -->
					        							<input type="hidden" name="study_plan_name" id="study_plan_name" value="<?=$newType->study_plan_name?>"/>
					        							<input type="hidden" name="university_name" id="university_name" value="<?=$newType->university_name?>"/>
					        							<input type="hidden" name="department" id="department" value="<?=$newType->department?>"/>
					        							<input type="hidden" name="major" id="major" value="<?=$newType->major?>"/>
					        							<input type="hidden" name="duration_from" id="duration_from" value="<?=$newType->duration_from?>"/>
					        							<input type="hidden" name="duration_to" id="duration_to" value="<?=$newType->duration_to?>"/>
					        							<input type="hidden" name="years" id="years" value="<?=$newType->years?>"/>
					        							<input type="hidden" name="teaching_language" id="teaching_language" value="<?=$newType->teaching_language?>"/>
					        							<input type="hidden" name="apply_deadline" id="apply_deadline" value="<?=$newType->apply_deadline?>"/>
					        							<input type="hidden" name="notes" id="notes" value="<?=$newType->notes?>"/>
					        							<th><input type="submit" class="btn btn-default" value="Apply"></th>

				        							</form>

				        						</tr>
			        						<?php endforeach; ?>

			        					</tbody>
			        				</table>
			        			</div>
		        			</div>

		        		</div>
		        	</div>





		</section>
		<div class="footer-login center">
       		<p>&copy; 2017 Study in China. All right reserved.</p>
   		</div>
	</div>



    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
</body>
</html>
