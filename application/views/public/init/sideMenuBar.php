          <div class="row application-menu" style="background-color: white; border:1px solid black; border-radius: 5px; padding: 10px;">

            <div class="col-xs-3" class="sidebar-menu" style="background-color: #2a3f54; color: white; text-decoration: none; height:100em;">
              <div class="row" >
               <div class="col-xs-12" >
                 <br><br>
                 <img src="<?=base_url()?>assets/sicss/images/default.png" class="img-responsive center-block"   alt="profile" style="border:0px solid black; width: 205px; border-radius: 50%;">
               </div>
             </div>
              <div class="row" style="background-color: #172D440;">
                <div class="col-xs-12"  id="basicInfoCol" style="margin: 0px; border: 4px groove #122335;">
                  <form method="post" id="basicInfo" action="<?=base_url()?>" >
                    <h5 class="button-text">
                      <input type="hidden" name="id" value="<?php if($application != null):?><?=$application[0]->application_id?><?php endif; ?>">
                      <button type="submit" name="submit" id="submitStep1" style="background: none; border: none;"> 1. <span class="glyphicon glyphicon-home" ></span> &ensp; Basic Info </button>
                    </h5>
                  </form>
                </div>
                <div class="col-xs-12" id="studyPlanCol" style="margin: 0px; border: 4px groove #122335;">
                  <form method="post"  id="studyPlan" action="<?=base_url()?>" >
                    <h5 class="button-text">
                      <input type="hidden" name="id" value="<?php if($application != null): ?><?=$application[0]->application_id?><?php endif; ?>">
                      <button type="submit" name="submit" id="submitStep2" style="background: none; border: none;"> 2. <span class="glyphicon glyphicon-list-alt" ></span> &ensp; Study Plan </button>
                    </h5>
                  </form>
                </div>
                <div class="col-xs-12" id="educationCol" style="margin: 0px; border: 4px groove #122335;">
                  <form method="post"  id="education" action="<?=base_url()?>">
                    <h5 class="button-text">
                      <input type="hidden" name="id" value="<?php if($application != null): ?><?=$application[0]->application_id?><?php endif; ?>">
                      <button type="submit" name="submit" id="submitStep3"  style="background: none; border: none;"> 3. <span class="glyphicon glyphicon-credit-card" ></span> &ensp; Education </button>
                    </h5>
                  </form>
                </div>
                <div class="col-xs-12" id="additionalInfoCol" style="margin: 0px; border: 4px groove #122335;">
                  <form method="post"  id="additionalInfo" action="<?=base_url()?>">
                    <h5 class="button-text">
                      <input type="hidden" name="id" value="<?php if($application != null): ?><?=$application[0]->application_id?><?php endif; ?>">
                      <button name="submit" id="submitStep4" style="background: none; border: none;"> 4. <span class="glyphicon glyphicon-tasks" ></span> &ensp; Additional Info </button>
                    </h5>
                  </form>
                </div>
                <div class="col-xs-12"  id="contactInfoCol" style="margin: 0px; border: 4px groove #122335;">
                  <form method="post"  id="contactInfo" action="<?=base_url()?>">
                    <h5 class="button-text">
                      <input type="hidden" name="id" value="<?php if($application != null): ?><?=$application[0]->application_id?><?php endif; ?>">
                      <button name="submit" id="submitStep5" style="background: none; border: none;"> 5. <span class="glyphicon glyphicon-asterisk" ></span> &ensp; Contact Info </button>
                    </h5>
                  </form>
                </div>
                <div class="col-xs-12" id="previewCol" style="margin: 0px; border: 4px groove #122335;">
                  <form method="post"  id="preview" action="<?=base_url()?>" >
                    <h5 class="button-text">
                      <input type="hidden" name="id" value="<?php if($application != null): ?><?=$application[0]->application_id?><?php endif; ?>">
                      <button type="submit" name="submit" id="submitStep6" style="background: none; border: none;"> 6. <span class="glyphicon glyphicon-eye-open" ></span> &ensp; Form Preview </button>
                    </h5>
                  </form>
                </div>
              </div>
            </div>
