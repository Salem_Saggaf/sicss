<?php $user = $this->session->userdata('user'); ?>
<div class="row">
  <div class="col-sm-6 col-xs-12">
    <img src="<?=base_url()?>assets/sicss/images/logo/logo.png" class="img-responsive" alt="">
  </div>
  <div class="col-sm-6 col-xs-12">
    <p><p style="color: white; text-transform:capitalize"><?=$user[0]['username']?>&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>home/logout">| &nbsp; Logout</a> </p></p>
  </div>
</div>
<div  style="margin-top: 208px;">
	<nav class="navbar navbar-default" style="background: #d9534f; border-color:#d9534f; color:white">
	  <div class="container-fluid">
	    <ul class="nav navbar-nav">
	      <li ><a href="<?=base_url()?>Home">Home</a></li>
	      <li ><a href="<?=base_url()?>ApplicationProcess/applicationStep1">Application</a></li>
	      <li><a href="<?=base_url()?>ApplicationProcess">Application Query</a></li>

        <li role="presentation" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Message <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url()?>Home/inbox">Inbox</a></li>
    	      <li><a href="<?=base_url()?>Home/outbox">Outbox</a></li>
    	      <li><a href="<?=base_url()?>Home/message">Message To  Admin</a></li>
          </ul>
        </li>
	    </ul>
	  </div>
	</nav>

</div>
