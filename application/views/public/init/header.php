    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>

    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/ico/fav.png">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <script type="text/javascript">
    jQuery(document).ready(function($){
	'use strict';
      	jQuery('body').backstretch([
          "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
	    ], {duration: 5000, fade: 500});
    });
    </script>


