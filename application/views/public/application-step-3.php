<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Index</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
     'use strict';
          jQuery('body').backstretch([
            "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
            "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
        ], {duration: 5000, fade: 500});
      });
    </script>

    <script type="text/javascript">

      $( document ).ready(function() {

        start(<?=$application[0]->status?>, <?=$application[0]->application_id?>);
        for(var i =0; i<document.getElementById("nationality").options.length;i++)
        {
          if(document.getElementById("nationality").options[i].value == "<?=$application[0]->nationality?>")
          {
           document.getElementById("nationality").selectedIndex = i;
          }
        }

        for(var i =0; i<document.getElementById("native_language").options.length;i++)
        {
          if(document.getElementById("native_language").options[i].value == "<?=$application[0]->native_language?>")
          {
           document.getElementById("native_language").selectedIndex = i;
          }
        }

        for(var i =0; i<document.getElementById("education_level").options.length;i++)
        {
          if(document.getElementById("education_level").options[i].value == "<?=$application[0]->highest_level_of_education?>")
          {
           document.getElementById("education_level").selectedIndex = i;
          }
        }

        for(var i =0; i<document.getElementById("religion").options.length;i++)
        {
          if(document.getElementById("religion").options[i].value == "<?=$application[0]->religion?>")
          {
           document.getElementById("religion").selectedIndex = i;
          }
        }

        for(var i =0; i<document.getElementById("occupation").options.length;i++)
        {
          if(document.getElementById("occupation").options[i].value == "<?=$application[0]->occupation?>")
          {
           document.getElementById("occupation").selectedIndex = i;
          }
        }
        isFileRequired("<?=$application[0]->profile_picutre?>", "image");


      });


    </script>
</head><!--/head-->
<style media="screen" type="text/css">
  body{
    overflow: visible;
  }
</style>
<body>
<div id="preloader"></div>


    <div id="content-wrapper">
  		<div id="header"></div>

      <section id="body" class="white">
        <div class="index-title">
          <div class="container">
          <?php include('init/nav.php'); ?>


            <?php include('init/sideMenuBar.php'); ?>
            <div class="col-xs-8">

              <form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep5" enctype="multipart/form-data">
                    <div class="form-group">
                      <?php if ($application[0]->profile_picutre == NULL): ?>
                        <img id="img" src="<?php if($application != null): ?><?=base_url()?>assets/sicss/images/default.png<?php endif; ?>" width="200px" height="auto" class="img-responsive" >
                        <?php else: ?>
                          <img id="img" src="<?php if($application != null): ?><?=base_url()?>assets/sicss/images/<?=$application[0]->profile_picutre?><?php endif; ?>" width="200px" height="auto" class="img-responsive" >
                      <?php endif; ?>
                      <br>
                      <label class="custom-file">
                        <span class="custom-file-control"></span>
                      </label>
                      <input class="btn btn-primary" type="file" id="image" name="image" class="custom-file-input"  onchange="readURL(this);")>

                    </div>

                    <hr>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="family_name">Family Name(as on passport)</label>
                        <input type="text"  id="family_name" name="family_name" class="form-control" value="<?php if($application != null): ?><?=$application[0]->family_name?><?php endif; ?>" required>
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="given_name">Given Name(as on passport)</label>
                        <input type="text"  id="given_name" name="given_name" class="form-control" value="<?php if($application != null): ?><?=$application[0]->given_name?><?php endif; ?>">
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="chinese_name">Chinese Name(if available)</label>
                        <input type="text"  id="chinese_name" name="chinese_name" class="form-control" value="<?php if($application != null): ?><?=$application[0]->chinese_name?><?php endif; ?>">
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group">
                        <label for="nationality">*Nationality</label>
                        <select name="nationality" id="nationality" class="form-control"  required>
                          <option value="">-- select one --</option>
                          <option value="afghan">Afghan</option>
                          <option value="albanian">Albanian</option>
                          <option value="algerian">Algerian</option>
                          <option value="american">American</option>
                          <option value="andorran">Andorran</option>
                          <option value="angolan">Angolan</option>
                          <option value="antiguans">Antiguans</option>
                          <option value="argentinean">Argentinean</option>
                          <option value="armenian">Armenian</option>
                          <option value="australian">Australian</option>
                          <option value="austrian">Austrian</option>
                          <option value="azerbaijani">Azerbaijani</option>
                          <option value="bahamian">Bahamian</option>
                          <option value="bahraini">Bahraini</option>
                          <option value="bangladeshi">Bangladeshi</option>
                          <option value="barbadian">Barbadian</option>
                          <option value="barbudans">Barbudans</option>
                          <option value="batswana">Batswana</option>
                          <option value="belarusian">Belarusian</option>
                          <option value="belgian">Belgian</option>
                          <option value="belizean">Belizean</option>
                          <option value="beninese">Beninese</option>
                          <option value="bhutanese">Bhutanese</option>
                          <option value="bolivian">Bolivian</option>
                          <option value="bosnian">Bosnian</option>
                          <option value="brazilian">Brazilian</option>
                          <option value="british">British</option>
                          <option value="bruneian">Bruneian</option>
                          <option value="bulgarian">Bulgarian</option>
                          <option value="burkinabe">Burkinabe</option>
                          <option value="burmese">Burmese</option>
                          <option value="burundian">Burundian</option>
                          <option value="cambodian">Cambodian</option>
                          <option value="cameroonian">Cameroonian</option>
                          <option value="canadian">Canadian</option>
                          <option value="cape verdean">Cape Verdean</option>
                          <option value="central african">Central African</option>
                          <option value="chadian">Chadian</option>
                          <option value="chilean">Chilean</option>
                          <option value="chinese">Chinese</option>
                          <option value="colombian">Colombian</option>
                          <option value="comoran">Comoran</option>
                          <option value="congolese">Congolese</option>
                          <option value="costa rican">Costa Rican</option>
                          <option value="croatian">Croatian</option>
                          <option value="cuban">Cuban</option>
                          <option value="cypriot">Cypriot</option>
                          <option value="czech">Czech</option>
                          <option value="danish">Danish</option>
                          <option value="djibouti">Djibouti</option>
                          <option value="dominican">Dominican</option>
                          <option value="dutch">Dutch</option>
                          <option value="east timorese">East Timorese</option>
                          <option value="ecuadorean">Ecuadorean</option>
                          <option value="egyptian">Egyptian</option>
                          <option value="emirian">Emirian</option>
                          <option value="equatorial guinean">Equatorial Guinean</option>
                          <option value="eritrean">Eritrean</option>
                          <option value="estonian">Estonian</option>
                          <option value="ethiopian">Ethiopian</option>
                          <option value="fijian">Fijian</option>
                          <option value="filipino">Filipino</option>
                          <option value="finnish">Finnish</option>
                          <option value="french">French</option>
                          <option value="gabonese">Gabonese</option>
                          <option value="gambian">Gambian</option>
                          <option value="georgian">Georgian</option>
                          <option value="german">German</option>
                          <option value="ghanaian">Ghanaian</option>
                          <option value="greek">Greek</option>
                          <option value="grenadian">Grenadian</option>
                          <option value="guatemalan">Guatemalan</option>
                          <option value="guinea-bissauan">Guinea-Bissauan</option>
                          <option value="guinean">Guinean</option>
                          <option value="guyanese">Guyanese</option>
                          <option value="haitian">Haitian</option>
                          <option value="herzegovinian">Herzegovinian</option>
                          <option value="honduran">Honduran</option>
                          <option value="hungarian">Hungarian</option>
                          <option value="icelander">Icelander</option>
                          <option value="indian">Indian</option>
                          <option value="indonesian">Indonesian</option>
                          <option value="iranian">Iranian</option>
                          <option value="iraqi">Iraqi</option>
                          <option value="irish">Irish</option>
                          <option value="israeli">Israeli</option>
                          <option value="italian">Italian</option>
                          <option value="ivorian">Ivorian</option>
                          <option value="jamaican">Jamaican</option>
                          <option value="japanese">Japanese</option>
                          <option value="jordanian">Jordanian</option>
                          <option value="kazakhstani">Kazakhstani</option>
                          <option value="kenyan">Kenyan</option>
                          <option value="kittian and nevisian">Kittian and Nevisian</option>
                          <option value="kuwaiti">Kuwaiti</option>
                          <option value="kyrgyz">Kyrgyz</option>
                          <option value="laotian">Laotian</option>
                          <option value="latvian">Latvian</option>
                          <option value="lebanese">Lebanese</option>
                          <option value="liberian">Liberian</option>
                          <option value="libyan">Libyan</option>
                          <option value="liechtensteiner">Liechtensteiner</option>
                          <option value="lithuanian">Lithuanian</option>
                          <option value="luxembourger">Luxembourger</option>
                          <option value="macedonian">Macedonian</option>
                          <option value="malagasy">Malagasy</option>
                          <option value="malawian">Malawian</option>
                          <option value="malaysian">Malaysian</option>
                          <option value="maldivan">Maldivan</option>
                          <option value="malian">Malian</option>
                          <option value="maltese">Maltese</option>
                          <option value="marshallese">Marshallese</option>
                          <option value="mauritanian">Mauritanian</option>
                          <option value="mauritian">Mauritian</option>
                          <option value="mexican">Mexican</option>
                          <option value="micronesian">Micronesian</option>
                          <option value="moldovan">Moldovan</option>
                          <option value="monacan">Monacan</option>
                          <option value="mongolian">Mongolian</option>
                          <option value="moroccan">Moroccan</option>
                          <option value="mosotho">Mosotho</option>
                          <option value="motswana">Motswana</option>
                          <option value="mozambican">Mozambican</option>
                          <option value="namibian">Namibian</option>
                          <option value="nauruan">Nauruan</option>
                          <option value="nepalese">Nepalese</option>
                          <option value="new zealander">New Zealander</option>
                          <option value="ni-vanuatu">Ni-Vanuatu</option>
                          <option value="nicaraguan">Nicaraguan</option>
                          <option value="nigerien">Nigerien</option>
                          <option value="north korean">North Korean</option>
                          <option value="northern irish">Northern Irish</option>
                          <option value="norwegian">Norwegian</option>
                          <option value="omani">Omani</option>
                          <option value="pakistani">Pakistani</option>
                          <option value="palauan">Palauan</option>
                          <option value="panamanian">Panamanian</option>
                          <option value="papua new guinean">Papua New Guinean</option>
                          <option value="paraguayan">Paraguayan</option>
                          <option value="peruvian">Peruvian</option>
                          <option value="polish">Polish</option>
                          <option value="portuguese">Portuguese</option>
                          <option value="qatari">Qatari</option>
                          <option value="romanian">Romanian</option>
                          <option value="russian">Russian</option>
                          <option value="rwandan">Rwandan</option>
                          <option value="saint lucian">Saint Lucian</option>
                          <option value="salvadoran">Salvadoran</option>
                          <option value="samoan">Samoan</option>
                          <option value="san marinese">San Marinese</option>
                          <option value="sao tomean">Sao Tomean</option>
                          <option value="saudi">Saudi</option>
                          <option value="scottish">Scottish</option>
                          <option value="senegalese">Senegalese</option>
                          <option value="serbian">Serbian</option>
                          <option value="seychellois">Seychellois</option>
                          <option value="sierra leonean">Sierra Leonean</option>
                          <option value="singaporean">Singaporean</option>
                          <option value="slovakian">Slovakian</option>
                          <option value="slovenian">Slovenian</option>
                          <option value="solomon islander">Solomon Islander</option>
                          <option value="somali">Somali</option>
                          <option value="south african">South African</option>
                          <option value="south korean">South Korean</option>
                          <option value="spanish">Spanish</option>
                          <option value="sri lankan">Sri Lankan</option>
                          <option value="sudanese">Sudanese</option>
                          <option value="surinamer">Surinamer</option>
                          <option value="swazi">Swazi</option>
                          <option value="swedish">Swedish</option>
                          <option value="swiss">Swiss</option>
                          <option value="syrian">Syrian</option>
                          <option value="taiwanese">Taiwanese</option>
                          <option value="tajik">Tajik</option>
                          <option value="tanzanian">Tanzanian</option>
                          <option value="thai">Thai</option>
                          <option value="togolese">Togolese</option>
                          <option value="tongan">Tongan</option>
                          <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                          <option value="tunisian">Tunisian</option>
                          <option value="turkish">Turkish</option>
                          <option value="tuvaluan">Tuvaluan</option>
                          <option value="ugandan">Ugandan</option>
                          <option value="ukrainian">Ukrainian</option>
                          <option value="uruguayan">Uruguayan</option>
                          <option value="uzbekistani">Uzbekistani</option>
                          <option value="venezuelan">Venezuelan</option>
                          <option value="vietnamese">Vietnamese</option>
                          <option value="welsh">Welsh</option>
                          <option value="yemenite">Yemenite</option>
                          <option value="zambian">Zambian</option>
                          <option value="zimbabwean">Zimbabwean</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <br>
                      <div class="form-group">
                        <label for="marital_status">Marital Status :</label>
                        <label class="radio-inline"><input type="radio" name="marital_status">Married</label>
                        <label class="radio-inline"><input type="radio" name="marital_status" >Unmarried</label>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <br>
                      <div class="form-group">
                        <label for="gender">Gender :</label>
                        <label class="radio-inline"><input type="radio" name="gender" >Male</label>
                        <label class="radio-inline"><input type="radio" name="gender">Female</label>
                      </div>
                    </div>
                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="birth_date">Birth Date :</label>
                        <input type="date" name="birth_date" id="birth_date" class="form-control" value="<?php if($application != null): ?><?=$application[0]->birth_date?><?php endif; ?>" >
                      </div>
                    </div>

                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="birth_place">*Place of Birth(City,Province)</label>
                        <input type="text" name="birth_place" id="birth_place" class="form-control" value="<?php if($application != null): ?><?=$application[0]->place_of_birth?><?php endif; ?>"  required>
                      </div>
                    </div>
                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="native_language">Native language</label>
                        <select data-placeholder="Choose a Language..." name="native_language" id="native_language"  class="form-control" >
                          <option value="AF">Afrikanns</option>
                          <option value="SQ">Albanian</option>
                          <option value="AR">Arabic</option>
                          <option value="HY">Armenian</option>
                          <option value="EU">Basque</option>
                          <option value="BN">Bengali</option>
                          <option value="BG">Bulgarian</option>
                          <option value="CA">Catalan</option>
                          <option value="KM">Cambodian</option>
                          <option value="ZH">Chinese (Mandarin)</option>
                          <option value="HR">Croation</option>
                          <option value="CS">Czech</option>
                          <option value="DA">Danish</option>
                          <option value="NL">Dutch</option>
                          <option value="EN">English</option>
                          <option value="ET">Estonian</option>
                          <option value="FJ">Fiji</option>
                          <option value="FI">Finnish</option>
                          <option value="FR">French</option>
                          <option value="KA">Georgian</option>
                          <option value="DE">German</option>
                          <option value="EL">Greek</option>
                          <option value="GU">Gujarati</option>
                          <option value="HE">Hebrew</option>
                          <option value="HI">Hindi</option>
                          <option value="HU">Hungarian</option>
                          <option value="IS">Icelandic</option>
                          <option value="ID">Indonesian</option>
                          <option value="GA">Irish</option>
                          <option value="IT">Italian</option>
                          <option value="JA">Japanese</option>
                          <option value="JW">Javanese</option>
                          <option value="KO">Korean</option>
                          <option value="LA">Latin</option>
                          <option value="LV">Latvian</option>
                          <option value="LT">Lithuanian</option>
                          <option value="MK">Macedonian</option>
                          <option value="MS">Malay</option>
                          <option value="ML">Malayalam</option>
                          <option value="MT">Maltese</option>
                          <option value="MI">Maori</option>
                          <option value="MR">Marathi</option>
                          <option value="MN">Mongolian</option>
                          <option value="NE">Nepali</option>
                          <option value="NO">Norwegian</option>
                          <option value="FA">Persian</option>
                          <option value="PL">Polish</option>
                          <option value="PT">Portuguese</option>
                          <option value="PA">Punjabi</option>
                          <option value="QU">Quechua</option>
                          <option value="RO">Romanian</option>
                          <option value="RU">Russian</option>
                          <option value="SM">Samoan</option>
                          <option value="SR">Serbian</option>
                          <option value="SK">Slovak</option>
                          <option value="SL">Slovenian</option>
                          <option value="ES">Spanish</option>
                          <option value="SW">Swahili</option>
                          <option value="SV">Swedish </option>
                          <option value="TA">Tamil</option>
                          <option value="TT">Tatar</option>
                          <option value="TE">Telugu</option>
                          <option value="TH">Thai</option>
                          <option value="BO">Tibetan</option>
                          <option value="TO">Tonga</option>
                          <option value="TR">Turkish</option>
                          <option value="UK">Ukranian</option>
                          <option value="UR">Urdu</option>
                          <option value="UZ">Uzbek</option>
                          <option value="VI">Vietnamese</option>
                          <option value="CY">Welsh</option>
                          <option value="XH">Xhosa</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="education_level">*Highest Level of Education</label>
                        <select type="date" name="education_level" id="education_level" class="form-control"  required>
                          <option value="">-choose-</option>
                          <option value="6" name="Junior">Junior high</option>
                          <option value="1" name="senior">Senior high</option>
                          <option value="2" name="vocational">Vocational College</option>
                          <option value="3" name="bachelor">Bachelor</option>
                          <option value="4" name="master">Master</option>
                          <option value="5" name="phd">PhD.</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="religion">*Religion</label>
                        <select type="date" name="religion" id="religion" class="form-control"  required>
                          <option value="">-choose-</option>
                          <option value="ff8080813f09a886013f0d0fe8920d11" name="Anglican">Anglican</option>
                          <option value="ff808081311878ac01311c1b00560014" name="Atheism">Atheism</option>
                          <option value="ff808081311878ac01311c1e0ca20015" name="Mormon">Mormon</option>
                          <option value="1" name="Christianity">Christianity</option>
                          <option value="2" name="Judaism">Judaism</option>
                          <option value="3" name="Catholicism">Catholicism</option>
                          <option value="4" name="Eastern_Orthodoxy">Eastern Orthodoxy</option>
                          <option value="6" name="Hinduism">Hinduism</option>
                          <option value="7" name="Islam">Islam</option>
                          <option value="8" name="Buddhism">Buddhism</option>
                          <option value="13" name="None">None</option>
                          <option value="12" name="Others">Others</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="institution_affilliated">*Employer or Institution Affiliated</label>
                        <input type="text" name="institution_affilliated" id="institution_affilliated" class="form-control" value="<?php if($application != null): ?><?=$application[0]->employer_or_institution_affiliated?><?php endif; ?>" required>
                      </div>
                    </div>

                    <div class="col-xs-6">

                      <div class="form-group">
                        <label for="occupation">*Occupation</label>
                        <select type="date" name="occupation" id="occupation" class="form-control"  required>
                          <option value="">-choose-</option>
                          <option value="ff80808131d70e210131dadb4ef6000a" name="职员">Employee</option>
                          <option value="3" name="学生">Student</option>
                          <option value="2" name="教师">Teacher</option>
                          <option value="ff8080812ef58be3012ef5d7efb80004" name="医生">Doctor</option>
                          <option value="4" name="工人">Worker</option>
                          <option value="ff808081368270a901368a978ec20380" name="军人">Army service</option>
                          <option value="6" name="工程师">Engineers</option>
                          <option value="7" name="学者">Scholars</option>
                          <option value="9" name="家庭主妇">Housewife</option>
                          <option value="8" name="退休">Retired</option>
                          <option value="ff8080812ef58be3012ef5d7aaf20003" name="经理">Manager</option>
                          <option value="ff80808131d70e210131dadd484a000b" name="政府公务人员">Officer</option>
                          <option value="5" name="农民">Farmer</option>
                          <option value="1" name="其他">Others</option>
                        </select>
                      </div>
                    </div>
                      <div class="col-xs-6">

                        <div class="form-group">
                          <label for="health_status">Health Status</label>
                          <input type="text" name="health_status" id="health_status"  class="form-control" value="<?php if($application != null): ?><?=$application[0]->health_status?><?php endif; ?>">
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <br>
                        <div class="form-group">
                          <label for="emigrant">Emigrant from mainland China, Hong Kong, Macau, and Taiwan?</label>
                          <label class="radio-inline"><input type="radio" name="emigrant" >Yes</label>
                          <label class="radio-inline"><input type="radio" name="emigrant" >No</label>
                        </div>
                      </div>
                      <div class="col-xs-6">

                        <div class="form-group">
                          <label for="hobby">Hobby</label>
                          <input type="text" name="hobby" id="hobby" class="form-control" value="<?php if($application != null): ?><?=$application[0]->hobby?><?php endif; ?>">
                        </div>
                      </div>
                      <div class="col-xs-12">
                                              <hr>
                      <h3>Passport And Visa</h3>
                      <hr>
                      </div>

                      <div class="col-xs-6">
                        <div class="form-group">
                          <label for="passport_no">*Passport No.</label>
                          <input type="text" name="passport_no" id="passport_no" class="form-control" value="<?php if($application != null): ?><?=$application[0]->passport_no?><?php endif; ?>"  required>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label for="passport_expiry_date">*Passport Expiration Date</label>
                          <input type="date" name="passport_expiry_date" id="passport_expiry_date"  class="form-control" value="<?php if($application != null): ?><?=$application[0]->passport_expiry?><?php endif; ?>" required>
                        </div>
                      </div>
                      <input type="hidden" name="id" value="<?php if($application != null):?><?=$application[0]->application_id?><?php endif; ?>">
                      <input type="hidden" name="status" value="<?php if($application != null):?><?=$application[0]->status?><?php endif; ?>">
                      <div class="col-xs-12">
                        <input type="submit" name="submit" value="Save and Next" class="btn btn-primary">
                      </div>
                    </div>
              </form>
            </div>
          </div>





          </div>
        </div>
      </div>

      </section>
      <div class="footer-login center">
        <p>&copy; 2017 Study in China. All right reserved.</p>
      </div>
    </div>
    <script type="text/javascript">
           function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img')
                        .attr('src', e.target.result)
                        .width(160)
                        .height(auto);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/userStatus.js"></script>
</body>
</html>
