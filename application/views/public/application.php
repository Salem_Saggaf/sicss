<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SICSS | Application</title>
    <link href="<?=base_url()?>assets/sicss/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/pe-icons.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/animate.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/sicss/css/custom.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/sicss/js/jquery.js"></script>
    <link rel="shortcut icon" href="<?=base_url()?>assets/sicss/images/ico/fav.png">

    <script type="text/javascript">
    jQuery(document).ready(function($){
	'use strict';
      	jQuery('body').backstretch([
          "<?=base_url()?>assets/sicss/images/banner/banner4.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner5.jpg",
	        "<?=base_url()?>assets/sicss/images/banner/banner6.jpg"
	    ], {duration: 5000, fade: 500});
    });
    </script>
	<style media="screen" type="text/css">
	  body{
	    overflow: visible;
	  }
	</style>
</head>

<body>
	<div id="preloader"></div>
    <div id="content-wrapper">
		<div id="header" ></div>
		<section id="body" class="white">
			<div class="index-title" >
				<div class="container" >
					<?php include('init/nav.php'); ?>
		        	<div class="panel panel-default">
		        		<div class="panel-body">
				        	<div class="row" style="background-color: white; ">
				        		<form method="post" action="<?=base_url()?>ApplicationProcess/applicationStep2">
				        			<div class="col-xs-3"></div>
					        		<div class="col-xs-9">
						        		<div class="form-group">
						        			<label for="program"> Please Choose Your Program :</label>
			                   				<label class="radio-inline" ><input required type="radio" name="program" id="program" checked>Self-Sponsered</label>
						        		</div>
					        		</div>
					        		<div class="col-xs-3"></div>
					        		<div class="col-xs-6">
				        				<div class="form-group">
						        			<input type="submit" name="submit"	value="Next" class="btn btn-primary btn-lg btn-block" style="border-radius: 15px; ">
						        		</div>
					        		</div>
				        		</form>
				        	</div>

				        	<div class="row" style="background-color: white;">
				        		<div class="col-xs-12">
				        			<div class="well well-lg" style="background-color: #36c154; color: white;">
										申请人保证/I hereby affirm that:<br>
										1)上述各项中填写的信息和提供的材料真实无误。如因个人信息错误、失真造成不良后果，责任由本人承担。
										All information and materials provided are factually true and correct. I understand that I may be subject to a range of possible disciplinary actions, including admission revocation or expulsion, should the information I’ve certified be false. <br>
										2)在华期间，遵守中国的法律和法规，不从事任何危害中国社会秩序的，与本人来华学习身份不相符合的活动；
										During my stay in China, I shall abide by the laws and decrees of the Chinese government, and will not participate in any activities which are deemed to be adverse to the social order in China and are inappropriate to the capacity as a student; <br>
										3)在学期间，遵守学校的校纪校规，尊重学校的教学安排。
										 During my study in China, I shall observe the rules and regulations of the university, and will concentrate on my studies and researches, and will follow the teaching programs provided by the university.
				        			</div>
				        		</div>
				        	</div>
		        		</div>
		        	</div>



			</div>
			<div class="container" style="background-color: grey;">

			</div>
		</section>
		<div class="footer-login center">
	       <p>&copy; 2017 Study in China. All right reserved.</p>
	    </div>
	</div>



    <script src="<?=base_url()?>assets/sicss/js/plugins.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>assets/sicss/js/init.js"></script>
</body>
</html>
